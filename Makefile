PRJ_NAME = box0-v5
PRJ_DIR = .
SRC_DIR = $(PRJ_DIR)/source
BUILD_DIR = $(PRJ_DIR)/build
LIB_DIR = $(PRJ_DIR)/libraries
EXTRA_DIR = $(PRJ_DIR)/extra

VPATH = $(PRJ_DIR)/source

# Linker script
LDSCRIPT = $(EXTRA_DIR)/STM32F072R8T6.ld

CFLAGS += -std=gnu99
CFLAGS += -mtune=cortex-m0 -mcpu=cortex-m0 -mfloat-abi=soft
CFLAGS += -mlittle-endian -mthumb -march=armv6-m
CFLAGS += -I$(LIB_DIR)/unicore-mx/include -DSTM32F0
CFLAGS += -Wall -Werror -Wno-main
#~ CFLAGS += -Ofast
CFLAGS += -g

LFLAGS += -nostartfiles -T $(LDSCRIPT) -nostdlib -Wl,--print-gc-sections
LFLAGS += -L$(LIB_DIR)/unicore-mx/lib

LIBS += -lucmx_stm32f0 -lc -lgcc -lnosys

OOCD = openocd

# Executables
ARCH = arm-none-eabi-
CC = $(ARCH)gcc
LD = $(ARCH)ld -v
AS = $(ARCH)as
OBJCPY = $(ARCH)objcopy
OBJDMP = $(ARCH)objdump
GDB = $(ARCH)gdb

CPFLAGS = --output-target=binary
ODFLAGS	= -x --syms

OBJS = usb.o dio.o main.o pwm.o ain.o aout.o helper.o i2c.o spi.o
OBJS += usb-descriptor.o device.o power.o clock.o

MKDIR = mkdir

# Targets
all: bin

# create build directory
init:
	@printf "Creating build directory\n"
	@-$(MKDIR) $(BUILD_DIR)
	@printf "Building unicore-mx"
	@cd $(LIB_DIR)/unicore-mx; $(MAKE) TARGETS="stm32/f0"

clean:
	rm -rf $(BUILD_DIR)/*

ihex: elf
	$(OBJCPY) --output-target=ihex 				\
		$(BUILD_DIR)/$(PRJ_NAME).elf			\
		$(BUILD_DIR)/$(PRJ_NAME).ihex

bin: elf
	@printf "BIN $(PRJ_NAME).bin\n"
	@$(OBJCPY) $(CPFLAGS) $(BUILD_DIR)/$(PRJ_NAME).elf $(BUILD_DIR)/$(PRJ_NAME).bin

%.o: %.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

elf: $(LDSCRIPT) $(OBJS)
	@printf "ELF $(PRJ_NAME).elf \n"
	@$(CC) $(CFLAGS) $(LFLAGS) $(BUILD_DIR)/*.o  \
		--static $(LIBS) -o $(BUILD_DIR)/$(PRJ_NAME).elf

install: burn
flash: burn

burn:
	@printf "Installing on device (Openocd, STLINK v2)"
	@$(OOCD) -f board/stm32f0discovery.cfg						\
		-c "init"												\
		-c "reset halt"											\
		-c "stm32f0x mass_erase 0"								\
		-c "flash write_image $(BUILD_DIR)/$(PRJ_NAME).elf"		\
		-c "reset"												\
		-c "shutdown"

dfu:
	dfu-util -a 0 -d 0483:df11 -s 0x08000000:leave -D $(BUILD_DIR)/box0-v5.bin
