/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/syscfg.h>
#include <unicore-mx/stm32/spi.h>
#include <unicore-mx/cm3/assert.h>

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "usb.h"
#include "standard.h"
#include "spi.h"
#include "common/handy.h"
#include "common/helper.h"
#include "config.h"
#include "power.h"
#include "common/debug.h"

/* temp fix for libopencm3 */
#define SPI2_I2S_BASE SPI2_BASE

int spi_validate_transaction(uint8_t *buf, uint8_t len);
void spi0_slave_select(uint8_t i, bool active);

void spi0_exec_start(void);
void spi0_exec_next(void);
void spi0_exec_done(uint8_t status);
void spi0_exec_stop(void);
void spi0_status_submit(uint8_t status);
static void spi0_command_submit(void);

/* default speed is 187.5 Khz */
#define SPI0_SPEED_INDEX_DEFAULT 0

/* lookup table is better */
static const struct spi_speed {
	uint32_t br_prescalar;
	uint32_t speed;
} spi0_speeds[] = {
	{SPI_CR1_BAUDRATE_FPCLK_DIV_256, 187500},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_128, B0_SPEED_KHZ(375)},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_64, B0_SPEED_KHZ(750)},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_32, B0_SPEED_KHZ(1500)},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_16, B0_SPEED_MHZ(3)},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_8, B0_SPEED_MHZ(6) },
	{SPI_CR1_BAUDRATE_FPCLK_DIV_4, B0_SPEED_MHZ(12)},
	{SPI_CR1_BAUDRATE_FPCLK_DIV_2, B0_SPEED_MHZ(24)},
};

static const struct {
	uint32_t port;
	uint16_t pinmask;
} spi0_ss[SPI0_SS_COUNT] = {
	{GPIOB, GPIO12},
	{GPIOC, GPIO6},
	{GPIOC, GPIO7},
	{GPIOC, GPIO8}
};

static struct {
	uint8_t state;
	uint8_t active_state; /* SS active Low OR SS active High */
	uint8_t speed_index;

	struct b0_spi_command command;
	struct b0_spi_status status;

	struct {
		uint8_t len;
		uint8_t buffer[SPI0_BUFFER_SIZE] ALIGNED_2BYTE;
	} in;

	struct {
		uint8_t len, processed;
		uint8_t buffer[SPI0_BUFFER_SIZE] ALIGNED_2BYTE;
	} out;
} spi0;

int spi0_speed_search(uint32_t value)
{
	int i;

	for (i = 0; i < ARRAY_LEN(spi0_speeds); i++) {
		if (spi0_speeds[i].speed == value) {
			return i;
		}
	}

	return -1;
}

void spi0_slave_select(uint8_t i, bool active)
{
	uint8_t mask = (1 << i);
	bool val = (spi0.active_state & mask) == (active ? mask : 0);

	if (val) {
		GPIO_BSRR(spi0_ss[i].port) = spi0_ss[i].pinmask;
	} else {
		GPIO_BRR(spi0_ss[i].port) = spi0_ss[i].pinmask;
	}
}

void spi0_init(void)
{
	spi0.state = B0_STATE_STOPPED;

	/* enable power supply */
	power_state_set(POWER_DIGITAL, POWER_ON);

	/* enable clocks */
	rcc_periph_clock_enable(RCC_SPI2);

	/* ============ configure dma peripherial Channel4 -- RX ========= */
	/* - Disable
	 * - Read from peripherial
	 * - Enable memory increment mode
	 * - Disable peripherial increment mode
	 * - 8bit memory access
	 * - 8bit periphierial access
	 * - Enable transfer complete interrupt
	 * - Enable transfer error interrupt */
	DMA1_CCR4 = DMA_CCR_MINC | DMA_CCR_TCIE | DMA_CCR_TEIE;
	DMA1_CPAR4 = (uint32_t) &SPI2_DR;

	/* ==========  configure dma peripherial Channel5 -- TX ========== */
	/* - Disable
	 * - Write to peripherial
	 * - Enable memory increment mode
	 * - Disable peripherial increment mode
	 * - 8bit memory access
	 * - 8bit periphierial access
	 * - Enable transfer complete interrupt
	 * - Enable transfer error interrupt */
	DMA1_CCR5 = DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE | DMA_CCR_TEIE;
	DMA1_CPAR5 = (uint32_t) &SPI2_DR;

	DMA1_IFCR = DMA_IFCR_CGIF4 | DMA_IFCR_CGIF5; /* Clear flags */
	nvic_set_priority(NVIC_DMA1_CHANNEL4_5_IRQ, NVIC_PRIO_VERY_HIGH);
	nvic_enable_irq(NVIC_DMA1_CHANNEL4_5_IRQ);

	/* ====================== configure spi peripherial ============= */
	/* - Disable CRC
	 * - Master mode
	 * - Disable hardware slave select (SS as GPIO) */
	SPI2_CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	/* - 8bit FIFO threshold
	 * - 8bit bitsize
	 * - SS output disabled
	 * - Error interrupt enable */
	SPI2_CR2 = SPI_CR2_FRXTH | SPI_CR2_DS_8BIT | SPI_CR2_ERRIE;

	/* disable I2S */
	SPI2_I2SCFGR = 0;

	nvic_set_priority(NVIC_SPI2_IRQ, NVIC_PRIO_VERY_HIGH);
	nvic_enable_irq(NVIC_SPI2_IRQ);

	/* ======================= configure gpio======================== */
	/* TODO: GPIO_PUPD_PULLDOWN should be done according to pin (f.e.: SS require PULLUP)
	 * also, when device is in spi mode where clock or data line shall be HIGH
	 * in normal state PULLUP should be used */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO13|GPIO14|GPIO15);
	gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, GPIO13|GPIO14|GPIO15);
	gpio_set_af(GPIOB, GPIO_AF0, GPIO13|GPIO14|GPIO15);

	/* init SS pins */
	spi0.active_state = 0x00;
	uint8_t i;
	for (i = 0; i < SPI0_SS_COUNT; i++) {
		uint32_t port = spi0_ss[i].port;
		uint16_t pinmask = spi0_ss[i].pinmask;
		gpio_mode_setup(port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, pinmask);
		gpio_set_output_options(port, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, pinmask);
		gpio_set(port, pinmask);
	}

	/* =============== default configuration ======================== */
	spi0.speed_index = SPI0_SPEED_INDEX_DEFAULT;

	/*============== endpoint =========== */
	/* Clear data toggle */
	usbd_set_ep_dtog(dev_global, SPI0_EP_ADDR_IN, false);
	usbd_set_ep_dtog(dev_global, SPI0_EP_ADDR_OUT, false);

	/* Clear stall */
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_IN, false);
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_OUT, false);

	/* Cancel pending transfer */
	usbd_transfer_cancel_ep(dev_global, SPI0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev_global, SPI0_EP_ADDR_OUT);
}

void spi0_deinit(void)
{
	spi0.state = B0_STATE_STOPPED;

	/*  ==================== disable SPI2 clocks ===================== */
	rcc_periph_clock_disable(RCC_SPI2);
	nvic_disable_irq(NVIC_SPI2_IRQ);

	/* ============== disable DMA channels ========================== */
	DMA1_CCR4 &= ~DMA_CCR_EN;
	DMA1_CCR5 &= ~DMA_CCR_EN;

	nvic_disable_irq(NVIC_DMA1_CHANNEL4_5_IRQ);

	/* ====================== make the pins HiZ ===================== */
	pins_protect(GPIOB, GPIO13|GPIO14|GPIO15);
	uint8_t i;
	for (i = 0; i < SPI0_SS_COUNT; i++) {
		pins_protect(spi0_ss[i].port, spi0_ss[i].pinmask);
	}

	/* Cancel pending transfer */
	usbd_transfer_cancel_ep(dev_global, SPI0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev_global, SPI0_EP_ADDR_OUT);
}

static usbd_control_transfer_feedback spi0_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	uint32_t speed = CAST_TO_UINT32_PTR(arg->buffer)[0];

	int index = spi0_speed_search(speed);
	ASSERT_STALL(index >= 0);

	spi0.speed_index = index;
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback spi0_mode_set_master_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	UNUSED(arg);

	/* Stop any ongoing execution */
	if (spi0.state == B0_STATE_RUNNING) {
		spi0_exec_stop();
	}

	/* Cancel any pending transfer */
	usbd_transfer_cancel_ep(dev, SPI0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev, SPI0_EP_ADDR_OUT);

	/* Clear any previous stalled endpoint */
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_IN, false);
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_OUT, false);

	/* Set SS pins */
	spi0.active_state = 0x00;
	uint8_t i;
	for (i = 0; i < SPI0_SS_COUNT; i++) {
		GPIO_BSRR(spi0_ss[i].port) = spi0_ss[i].pinmask;
	}

	/* Accept command */
	spi0_command_submit();

	return USBD_CONTROL_TRANSFER_OK;
}

bool spi0_setup_callback(usbd_device *dev,
			const struct usb_setup_data *setup_data)
{
	uint8_t *buf = usbd_control_buffer;

	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		switch (setup_data->bRequest) {
		case B0_SPI_ACTIVE_STATE_GET: {
			uint8_t bSS = B0_SPI_PIN_FROM_WVALUE(setup_data->wValue);
			ASSERT_STALL(bSS < SPI0_SS_COUNT);
			buf[0] = (spi0.active_state & (1 << bSS)) ?
				B0_SPI_ACTIVE_STATE_HIGH : B0_SPI_ACTIVE_STATE_LOW;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		} break;
		case B0_SPEED_GET:
			usbd_ep0_transfer(dev, setup_data,
				(void *) &spi0_speeds[spi0.speed_index].speed, 4, NULL);
		break;
		case B0_MODE_GET:
			buf[0] = B0_MODE_MASTER;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &spi0.state, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_SPI_ACTIVE_STATE_SET: {
			ASSERT_STALL(spi0.state == B0_STATE_STOPPED);
			uint8_t bSS = B0_SPI_PIN_FROM_WVALUE(setup_data->wValue);
			ASSERT_STALL(bSS < SPI0_SS_COUNT);

			/* update the value */
			if (B0_SPI_VALUE_FROM_WVALUE(setup_data->wValue) & 0x01) {
				spi0.active_state |= 1 << bSS;
			} else {
				spi0.active_state &= ~(1 << bSS);
			}

			/* update the pin */
			spi0_slave_select(bSS, false);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		} break;
		case B0_MODE_SET:
			ASSERT_STALL(B0_MODE_FROM_WVALUE(setup_data->wValue) == B0_MODE_MASTER);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, spi0_mode_set_master_callback);
		break;
		case B0_SPEED_SET:
			ASSERT_STALL(setup_data->wLength == 4);
			ASSERT_STALL(spi0.state == B0_STATE_STOPPED);
			usbd_ep0_transfer(dev, setup_data, buf, 4, spi0_speed_set_callback);
		break;
		case B0_STATE_SET:
			/* Host can only issue STOP command and should be in running state */
			ASSERT_STALL(B0_STATE_FROM_WVALUE(setup_data->wValue) == B0_STATE_STOP);
			ASSERT_STALL(spi0.state == B0_STATE_RUNNING);
			spi0_exec_done(B0_SPI_STATUS_PREMAT_HALT);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

void spi0_exec_start(void)
{
	spi0.out.processed = 0;
	spi0.out.len = spi0.command.dLength;
	spi0.in.len = 0;
	spi0.state = B0_STATE_RUNNING;

#if (SPI0_TRANSACTION_VALIDATION_ENABLE == 1)
	int r = spi_validate_transaction(spi0.out.buffer, spi0.out.len);
	if (r < 0 || r > SPI0_BUFFER_SIZE) {
		/* Transaction will overflow */
		LOG_LN("SPI0 Transaction invalid");
		spi0_exec_done(B0_SPI_STATUS_ERR_INVALID);
		return;
	}
#endif

	/* start SPI */
	spi0_exec_next();
}

/* for stm32f072 this is equivalent to dma1_channel4_5_6_7_isr */
void dma1_channel4_5_isr(void)
{
	/* error occured? */
	if (DMA1_ISR & (DMA_ISR_TEIF4 | DMA_ISR_TEIF5)) {
		/* clear the flags */
		DMA1_IFCR = DMA_IFCR_CGIF4 | DMA_IFCR_CGIF5;

		spi0_deinit();
		return;
	}

	/* execute next if transfer is complete */
	if (DMA1_ISR & (DMA_ISR_TCIF4 | DMA_ISR_TCIF5)) {
		/* convert half duplex receive mode to transmit mode
		 * so that, it dont read extra data on half-duplex receive mode
		 * no side-effect on full-duplex
		 *
		 * Thanks to Krishnan Solanki @ ST Noida
		 */
		if ((SPI2_CR1 & (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE)) == SPI_CR1_BIDIMODE) {
			SPI2_CR1 |= SPI_CR1_BIDIOE;
		} else {
			/* Wait to receive last data */
			while (SPI2_SR & SPI_SR_RXNE);

			/* Wait to transmit last data */
			while (!(SPI2_SR & SPI_SR_TXE));

			/* Wait until not busy */
			while (SPI2_SR & SPI_SR_BSY);
		}

		/* disable SPI */
		SPI2_CR1 &= ~SPI_CR1_SPE;

		union b0_spi_out *out = (union b0_spi_out *) &spi0.out.buffer[spi0.out.processed];

		/* deactivate the previous slave */
		spi0_slave_select(out->bSS, false);

		/* disable DMA trigger */
		SPI2_CR2 &= ~(SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN);

		/* clear the flags */
		DMA1_IFCR = DMA_IFCR_CGIF4 | DMA_IFCR_CGIF5;

		DMA1_CCR4 &= ~DMA_CCR_EN;
		DMA1_CCR5 &= ~DMA_CCR_EN;

		spi0.out.processed += sizeof(struct b0_spi_out_header);

		uint8_t bytes_per_data = BIT2BYTE(out->bBitsize);
		unsigned bytes_transferred = out->bCount * bytes_per_data;

		if (out->bmConfig & (0x1 << 2)) { /* half duplex */
			if (out->bmConfig & (0x1 << 3) /* just finished performing a read */ ) {
				/* increment to new memory */
				/* spi0.out.processed += 0; */
				spi0.in.len += bytes_transferred;
			} else /* just finished performing a write */ {
				/* also update the number of bytes written
				 * TODO: perform test if data was successful */
				union b0_spi_in *in = (union b0_spi_in *) &spi0.in.buffer[spi0.in.len];
				in->hd.write.bWrite = out->hd.write.bWrite;

				/* increment to new memory */
				spi0.out.processed += bytes_transferred;
				spi0.in.len += 1;
			}
		} else { /* full duplex */
			spi0.out.processed += bytes_transferred;
			spi0.in.len += bytes_transferred;
		}

		if (spi0.out.processed >= spi0.out.len) {
			spi0_exec_done(B0_SPI_STATUS_SUCCESS);
		} else {
			spi0_exec_next();
		}
	} else {
		cm3_assert_failed();
	}
}

/* SS will be activated, de-activated outside this function */
void spi0_exec_next(void)
{
	union b0_spi_out *out = (union b0_spi_out *) &spi0.out.buffer[spi0.out.processed];
	union b0_spi_in *in = (union b0_spi_in *) &spi0.in.buffer[spi0.in.len];

	/* Speed switch */
	int speed_index = out->dSpeed ? spi0_speed_search(out->dSpeed) : spi0.speed_index;
	SPI2_CR1 = (SPI2_CR1 & ~(0x7 << 3)) | spi0_speeds[speed_index].br_prescalar;

	uint16_t dma_count;
	void *dma_tx, *dma_rx;

	if (out->bmConfig & (0x1 << 2)) {
		SPI2_CR1 |= SPI_CR1_BIDIMODE;

		if (out->bmConfig & (0x1 << 3)) {
			/* Perform a Half Duplex Read */
			dma_count = out->hd.read.bRead;

			/* Read data and place it in IN buffer */
			dma_rx = in->hd.read.bData;

			/* TX dma to be left un-initalized */
			dma_tx = NULL;

			/* SPI in Half Duplex Read Mode */
			SPI2_CR1 &= ~SPI_CR1_BIDIOE;
		} else {
			/* Perform a Half Duplex Write */
			dma_count = out->hd.write.bWrite;

			/* dont enable RX DMA  */
			dma_rx = NULL;

			/* data to write to slave */
			dma_tx = out->hd.write.bData;

			/* SPI in Half Duplex Write Mode */
			SPI2_CR1 |= SPI_CR1_BIDIOE;
		}
	} else { /* full duplex */
		dma_rx = in->fd.bData;
		dma_tx = out->fd.bData;
		dma_count = out->fd.bCount;

		/* full duplex */
		SPI2_CR1 &= ~SPI_CR1_BIDIMODE;
	}

	if (out->bmConfig & (0x1 << 4)) {
		SPI2_CR1 |= SPI_CR1_LSBFIRST; /* LSB first */
	} else {
		SPI2_CR1 &= ~SPI_CR1_LSBFIRST; /* MSB first */
	}

	SPI2_CR1 = (SPI2_CR1 & ~(SPI_CR1_CPOL | SPI_CR1_CPHA)) | (out->bmConfig & 0x3);
	SPI2_CR2 = (SPI2_CR2 & ~SPI_CR2_DS_MASK) | ((out->bBitsize - 1) << 8);

	dma_count *= BIT2BYTE(out->bBitsize);

	/* enable RX trigger (optional -- see above code) */
	if (dma_rx != NULL) {
		SPI2_CR2 |= SPI_CR2_RXDMAEN;

		/* Initalize DMA Rx */
		DMA1_CMAR4 = (uint32_t) dma_rx;
		DMA1_CNDTR4 = dma_count;
		DMA1_CCR4 |= DMA_CCR_EN;
	}

	if (dma_tx != NULL) {
		/* Initalize DMA Tx */
		DMA1_CMAR5 = (uint32_t) dma_tx;
		DMA1_CNDTR5 = dma_count;
		DMA1_CCR5 |= DMA_CCR_EN;
	}

	/* enable Tx trigger */
	SPI2_CR2 |= SPI_CR2_TXDMAEN;

	/* activate the slave */
	spi0_slave_select(out->bSS, true);

	/* and the war begins... */
	SPI2_CR1 |= SPI_CR1_SPE;
}

void spi0_exec_stop(void)
{
	/* disable DMA channels */
	DMA1_CCR4 &= ~DMA_CCR_EN;
	DMA1_CCR5 &= ~DMA_CCR_EN;

	/* disable DMA trigger */
	SPI2_CR2 &= ~(SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN);

	/* disable SPI */
	SPI2_CR1 &= ~SPI_CR1_SPE;

	/* force SPI to stop */
	spi0.state = B0_STATE_STOPPED;
}

void spi0_exec_done(uint8_t status)
{
	spi0_exec_stop();
	spi0_status_submit(status);
}

void spi2_isr(void)
{
	/* catch all error | though none of these is useful */
	if (SPI2_SR & (SPI_SR_MODF | SPI_SR_OVR | SPI_SR_UDR)) {
		spi0_deinit();
	} else {
		cm3_assert_failed();
	}
}

#if (SPI0_TRANSACTION_VALIDATION_ENABLE == 1)
int spi_validate_transaction(uint8_t *buf, uint8_t len)
{
	unsigned out_processed = 0;
	unsigned in_consumed = 0;
	union b0_spi_out *out;

	while (out_processed < len) {
		out = (union b0_spi_out *) &buf[out_processed];

		if (out->dSpeed && spi0_speed_search(out->dSpeed) == -1) {
			LOG_LN("Transaction have invalid speed");
			return -1;
		}

		if (out->bBitsize < 4 || out->bBitsize > 16) {
			LOG_LN("Transaction has invalid bitsize");
			return -1;
		}

		uint8_t byte_size = BIT2BYTE(out->bBitsize);
		size_t payload_len = byte_size * out->bCount;

		out_processed += sizeof(struct b0_spi_out_header);

		if (!(out->bmConfig & B0_SPI_CONFIG_DUPLEX_HD_MASK)) {
			/* Full duplex */
			out_processed += payload_len;
			in_consumed += payload_len;
		} else {
			if (out->bmConfig & B0_SPI_CONFIG_DUPLEX_HD_READ_MASK) {
				/* Half duplex read */
				in_consumed += payload_len;
			} else {
				/* Half duplex write */
				out_processed += payload_len;
				in_consumed += 1;
			}
		}
	}

	if (out_processed > len) {
		/* corrupt transaction */
		return -1;
	} else {
		return in_consumed;
	}
}
#endif

static void spi0_problem_detected(void)
{
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_IN, true);
	usbd_set_ep_stall(dev_global, SPI0_EP_ADDR_OUT, true);
}

static void spi0_in_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = SPI0_EP_ADDR_IN,
		.ep_size = SPI0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = spi0.in.buffer,
		.length = spi0.in.len,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = NULL
	};

	usbd_transfer_submit(dev_global, &transfer);
}

static void spi0_command_submit(void);

static void spi0_status_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	UNUSED(urb_id);

	if (status != USBD_SUCCESS) {
		return;
	}

	if (spi0.in.len) {
		spi0_in_submit();
	}

	spi0_command_submit();
}

void spi0_status_submit(uint8_t status)
{
	spi0.status.dSignature = B0_SPI_STATUS_SIGNATURE;
	spi0.status.dTag = spi0.command.dTag;
	spi0.status.dLength = spi0.in.len;
	spi0.status.bStatus = status;

	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = SPI0_EP_ADDR_IN,
		.ep_size = SPI0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = &spi0.status,
		.length = sizeof(spi0.status),
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = spi0_status_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}

static void spi0_out_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	if (status != USBD_SUCCESS) {
		LOG_LN("SPI0 OUT callback reported failure");
		return;
	}

	spi0_exec_start();
}

static void spi0_out_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = SPI0_EP_ADDR_OUT,
		.ep_size = SPI0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = spi0.out.buffer,
		.length = spi0.command.dLength,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = spi0_out_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}

static void spi0_command_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	usbd_transfer_cancel_ep(dev, SPI0_EP_ADDR_IN);

	if (status != USBD_SUCCESS) {
		/* Problem receving command packet */
		return;
	}

	/* Verify the signature */
	if (spi0.command.dSignature != B0_SPI_COMMAND_SIGNATURE) {
		LOG_LN("SPI0 COMMAND signature match failed");
		goto problem;
	}

	/* Verify the reserved field */
	if (spi0.command.bCommand != B0_SPI_COMMAND_TRANSACTION) {
		LOG_LN("SPI0 COMMAND reserved field not zero");
		goto problem;
	}

	/* Verify the OUT length */
	if (!spi0.command.dLength || spi0.command.dLength > SPI0_BUFFER_SIZE) {
		LOG_LN("SPI0 COMMAND received dLength will cause overflow");
		goto problem;
	}

	spi0_out_submit();
	return;

	problem:
	spi0_problem_detected();
}

static void spi0_command_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = SPI0_EP_ADDR_OUT,
		.ep_size = SPI0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = &spi0.command,
		.length = sizeof(spi0.command),
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = spi0_command_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}
