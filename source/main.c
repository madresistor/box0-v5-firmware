/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/crs.h>
#include <unicore-mx/stm32/flash.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/cm3/systick.h>

#include "common/helper.h"
#include "standard.h"
#include "ain.h"
#include "aout.h"
#include "dio.h"
#include "config.h"
#include "usb.h"
#include "device.h"
#include "power.h"
#include "i2c.h"
#include "clock.h"

#if (USB_INTERRUPT_DESIGN == 1)
void tim7_isr(void)
{
	ain0_dma_poll();
	aout0_dma_poll();
	TIM7_SR = 0; /* Clear interrrupt flag. */
}

/**
 * TIM7 is unused.
 * Use it for polling (at 1Khz)
 */
static void poll_setup(void)
{
	rcc_periph_clock_enable(RCC_TIM7);

	nvic_set_priority(NVIC_TIM7_IRQ, NVIC_PRIO_MEDIUM);
	nvic_enable_irq(NVIC_TIM7_IRQ);

	TIM7_PSC = 48 - 1; /* 1Mhz CK_PSC */
	TIM7_ARR = USBD_INTERRUPT_POLL_DELAY_US - 1;
	TIM7_DIER |= TIM_DIER_UIE; /* Update interrupt */
	TIM7_CR1 |= TIM_CR1_CEN;
}
#endif

void main(void)
{
	/* common periph clocking */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);
	rcc_periph_clock_enable(RCC_GPIOD);
	rcc_periph_clock_enable(RCC_DMA);
	rcc_periph_clock_enable(RCC_SYSCFG_COMP);

	/* board specific init */
	led_act_init();
	power_init(POWER_ANALOG);
	power_init(POWER_DIGITAL);

	clock_init();

	adc_calibrate();

	usb_init();

#if (USB_INTERRUPT_DESIGN == 1)
	poll_setup();
#endif

	for (;;) {
#if (USB_INTERRUPT_DESIGN == 1)
		__asm__("wfi");
#else
		usb_isr();
		ain0_dma_poll();
		aout0_dma_poll();
#endif
	}
}
