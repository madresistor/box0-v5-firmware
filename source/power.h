/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_POWER_H
#define BOX0V5_POWER_H

#include <stdbool.h>

typedef struct rt9728 rt9728;
extern const struct rt9728 power_analog, power_digital;

#define POWER_ANALOG (&power_analog)
#define POWER_DIGITAL (&power_digital)

void power_init(const struct rt9728 *ic);

enum power_state {
	POWER_OFF,
	POWER_ON,
	POWER_OC
};

enum power_state power_state_get(const struct rt9728 *ic);
void power_state_set(const struct rt9728 *ic, enum power_state state);

#endif
