/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_PWM_H
#define BOX0V5_PWM_H

#include "standard.h"
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>

__BEGIN_DECLS

/* properties */
#define B0_PWM_INITAL 101
#define B0_PWM_SYMMETRIC 102

/*
 * get pwm width register
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_PWM_WIDTH_GET
 * wValue: HIGH8(0) LOW8(<width-register-index>)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: BITSIZE_TO_BYTES(<current_bitsize>)
 *
 * data: <width register value>
 *
 * STALL if the width register dont exists
 */
#define B0_PWM_WIDTH_GET 101

/*
 * set pwm width register value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_PWM_WIDTH_SET
 * wValue: HIGH8(0) LOW8(<width-register-index>)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: BITSIZE_TO_BYTES(<current_bitsize>)
 *
 * data: <width register value>
 *
 * stall if width register dont exists
 * stall if size greater than maximum register capacity
 */
#define B0_PWM_WIDTH_SET 102

/*
 * get pwm period register value
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_PWM_PERIOD_GET
 * wValue: 0
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: BITSIZE_TO_BYTES(<current_bitsize>)
 *
 * data: <period register value>
 *
 * STALL if the width register dont exists
 */
#define B0_PWM_PERIOD_GET 103

/*
 * set pwm period register
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_PWM_PERIOD_SET
 * wValue: 0
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: BITSIZE_TO_BYTES(<current_bitsize>)
 *
 * data: <period register value>
 *
 * stall if size greater than maximum register capacity
 */
#define B0_PWM_PERIOD_SET 104

/*
 * state of output (LOW or HIGH) when (Width < Period)
 * if not provided assumed LOW only
 */
#define DECLARE_B0_PWM_INITAL(len_of_array)		\
struct b0_pwm_inital ## len_of_array {			\
	struct b0_property header;					\
	uint8_t bValues[ len_of_array ];			\
} PACKED

#define FILL_B0_PWM_INITAL(type_size, ...) {					\
		.header = FILL_B0_PROPERTY(type_size, B0_PWM_INITAL),	\
		.bValues = { __VA_ARGS__ }								\
	}

#define B0_PWM_INITAL_LOW 0x00
#define B0_PWM_INITAL_HIGH 0x01
DECLARE_B0_PWM_INITAL(2);

/* symetricity in output
 * if not provided, symmetric = B0_PWM_SYMMETRIC_NO is assumed
 */
#define DECLARE_B0_PWM_SYMMETRIC(len_of_array)		\
struct b0_pwm_symmetric ## len_of_array {			\
	struct b0_property header;						\
	uint8_t bValues[ len_of_array ];				\
} PACKED

#define FILL_B0_PWM_SYMMETRIC(type_size, ...) {						\
		.header = FILL_B0_PROPERTY(type_size, B0_PWM_SYMMETRIC),	\
		.bValues = { __VA_ARGS__ }									\
	}

#define B0_PWM_SYMMETRIC_NO 0x00
#define B0_PWM_SYMMETRIC_YES 0x01
DECLARE_B0_PWM_SYMMETRIC(2);

struct b0_pwm0 {
	struct b0_module header;
	struct b0_label1 label1;
	struct b0_bitsize1 bitsize1;
	struct b0_capab capab;
	struct b0_speed_div speeddiv;
	struct b0_ref ref;
	struct b0_pwm_inital2 inital2;
	struct b0_pwm_symmetric2 symmetric2;
	struct b0_count count;
} PACKED;

struct b0_pwm1 {
	struct b0_module header;
	struct b0_label1 label1;
	struct b0_bitsize1 bitsize1;
	struct b0_capab capab;
	struct b0_speed_div speeddiv;
	struct b0_ref ref;
	struct b0_pwm_inital2 inital2;
	struct b0_pwm_symmetric2 symmetric2;
	/* TODO: support oneshot for PWM */
	struct b0_count count;
} PACKED;

#define B0_PWM_PIN_FROM_WVALUE(wValue) (wValue & 0xFF)

void pwm0_deinit(void);
void pwm0_init(void);
bool pwm0_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data);

void pwm1_deinit(void);
void pwm1_init(void);
bool pwm1_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data);

__END_DECLS

#endif
