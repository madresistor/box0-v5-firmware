/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/syscfg.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>

#include "standard.h"
#include "dio.h"
#include "usb.h"
#include "common/helper.h"
#include "common/handy.h"
#include "config.h"

struct {
	uint8_t dir; /* 1 = OUTPUT, 0 = INPUT */
	uint8_t hiz; /* 1 = ENABLE, 0 = DISABLE */
	uint8_t value; /* 1 = HIGH, 0 = LOW */

	uint8_t state;
} dio0;

static void dio0_reset(void);

static void dio0_reset(void)
{
	dio0.dir = 0x00;
	dio0.hiz = 0xFF;
	dio0.value = 0x00;
	dio0.state = B0_STATE_STOPPED;

	pins_protect(GPIOB, dio0.hiz);
}

void dio0_deinit(void)
{
	dio0_reset();
}

void dio0_init(void)
{
	dio0_reset();
}

/**
 * Set the pins in @a pinmask to @a high
 * @param pinmask Pins mask
 * @param high High (if true) else Low
 */
static inline void value_set(uint16_t pinmask, bool high)
{
	if (high) {
		dio0.value |= pinmask;
	} else {
		dio0.value &= ~pinmask;
	}

	if (dio0.state == B0_STATE_RUNNING) {
		if (high) {
			GPIOB_BSRR = pinmask;
		} else {
			GPIOB_BRR = pinmask;
		}
	}
}

static inline void value_toggle(uint16_t pinmask)
{
	dio0.value ^= pinmask;

	if (dio0.state == B0_STATE_RUNNING) {
		if (dio0.value & pinmask) {
			GPIOB_BSRR = pinmask;
		} else {
			GPIOB_BRR = pinmask;
		}
	}
}

static inline void dir_set(uint16_t pinmask, bool output)
{
	/* on Direction set, value is reset to low */
	dio0.value &= ~pinmask;

	if (output) {
		dio0.dir |= pinmask;
	} else {
		dio0.dir &= ~pinmask;
	}

	if (dio0.state == B0_STATE_RUNNING) {
		uint8_t not_in_hiz = ~dio0.hiz;

		if (output) {
			/* update values of pins which are not in hiz */
			uint8_t reset = (~dio0.value) & not_in_hiz;
			uint8_t set = dio0.value & not_in_hiz;
			GPIOB_BSRR = (reset << 16) | set;

			/* mask only those pin output that are not in hiz */
			gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
									pinmask & not_in_hiz);
			gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_LOW,
									pinmask & not_in_hiz);
		} else {
			/* only make pins input which are not in hiz */
			gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE,
									pinmask & not_in_hiz);
		}
	}
}

static inline void hiz_set(uint16_t pinmask, bool enable)
{
	if (dio0.state == B0_STATE_RUNNING) {
		if (enable) {
			pins_protect(GPIOB, pinmask);
		} else {
			uint8_t change = dio0.hiz & pinmask;
			uint8_t input = change & (~dio0.dir);
			uint8_t output = change & dio0.dir;

			/* setup input ones */
			gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, input);

			/* setup output ones */
			gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, output);
			gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_LOW, output);

			/* update values of changed output pins */
			uint8_t reset = (~dio0.value) & output;
			uint8_t set = dio0.value & output;
			GPIOB_BSRR = (reset << 16) | set;
		}
	}

	if (enable) {
		dio0.hiz |= pinmask;
	} else {
		dio0.hiz &= ~pinmask;
	}
}

static bool single_command(usbd_device *dev,
				const struct usb_setup_data *setup_data)
{
	uint8_t pin = B0_DIO_PIN_FROM_WVALUE(setup_data->wValue);
	ASSERT_STALL(pin < DIO0_PIN_COUNT);

	uint8_t pinmask = 1 << pin;

	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		uint8_t *buf = usbd_control_buffer;

		switch (setup_data->bRequest) {
		case B0_DIO_VALUE_GET: {
			uint8_t tmp = (dio0.dir & pinmask) ? dio0.value : GPIOB_IDR;
			buf[0] = (tmp & pinmask) ? B0_DIO_HIGH : B0_DIO_LOW;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		} break;
		case B0_DIO_DIR_GET:
			buf[0] = (dio0.dir & pinmask) ? B0_DIO_ENABLE : B0_DIO_DISABLE;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_DIO_HIZ_GET:
			buf[0] = (dio0.hiz & pinmask) ? B0_DIO_ENABLE : B0_DIO_DISABLE;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_DIO_VALUE_SET:
			/* Make sure pin in output mode */
			ASSERT_STALL((dio0.dir & pinmask) == pinmask);

			value_set(pinmask, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_VALUE_TOGGLE:
			/* Make sure pin in output mode */
			ASSERT_STALL((dio0.dir & pinmask) == pinmask);

			value_toggle(pinmask);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_DIR_SET:
			dir_set(pinmask, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_HIZ_SET:
			hiz_set(pinmask, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

/**
 * Called after SETUP_OUT/STATUS data of multiple commands
 */
static usbd_control_transfer_feedback multiple_command_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	UNUSED(arg);
	UNUSED(dev);

	uint8_t bRequest = usbd_control_buffer[0];
	uint8_t wValue_low = usbd_control_buffer[1];
	uint8_t startpin = usbd_control_buffer[2];
	uint8_t pinmask = usbd_control_buffer[3] << startpin;

	switch (bRequest) {
	case B0_DIO_MULTIPLE_VALUE_SET:
		/* All pinmask pins should be in Output mode */
		ASSERT_STALL((dio0.dir & pinmask) == pinmask);

		value_set(pinmask, !!(wValue_low & 0x1));
	break;
	case B0_DIO_MULTIPLE_VALUE_TOGGLE:
		/* All pinmask pins should be in Output mode */
		ASSERT_STALL((dio0.dir & pinmask) == pinmask);

		value_toggle(pinmask);
	break;
	case B0_DIO_MULTIPLE_DIR_SET:
		dir_set(pinmask, !!(wValue_low & 0x1));
	break;
	case B0_DIO_MULTIPLE_HIZ_SET:
		hiz_set(pinmask, !!(wValue_low & 0x1));
	break;
	}

	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static bool multiple_command(usbd_device *dev,
				const struct usb_setup_data *setup_data)
{
	uint8_t startpin = B0_DIO_PIN_FROM_WVALUE(setup_data->wValue);
	ASSERT_STALL(startpin < DIO0_PIN_COUNT);
	ASSERT_STALL(setup_data->wLength == 1);

	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		uint8_t *buf = usbd_control_buffer;

		switch (setup_data->bRequest) {
		case B0_DIO_MULTIPLE_VALUE_GET: {
			/* hiz pins value read from storage */
			uint8_t tmp = (GPIOB_IDR & (~dio0.hiz)) | (dio0.value & dio0.hiz);
			buf[0] = tmp >> startpin;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		} break;
		case B0_DIO_MULTIPLE_DIR_GET:
			buf[0] = dio0.dir >> startpin;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_DIO_MULTIPLE_HIZ_GET:
			buf[0] = dio0.hiz >> startpin;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		uint8_t *buf = usbd_control_buffer;
		buf[0] = setup_data->bRequest; buf++;
		buf[1] = setup_data->wValue; buf++;
		buf[2] = setup_data->wValue >> 8; buf++;

		switch (setup_data->bRequest) {
		case B0_DIO_MULTIPLE_VALUE_SET:
		case B0_DIO_MULTIPLE_VALUE_TOGGLE:
		case B0_DIO_MULTIPLE_DIR_SET:
		case B0_DIO_MULTIPLE_HIZ_SET:
			usbd_ep0_transfer(dev, setup_data, buf, 1,
							multiple_command_callback);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

static bool all_command(usbd_device *dev,
				const struct usb_setup_data *setup_data)
{
	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		ASSERT_STALL(setup_data->wLength == 1);
		uint8_t *buf = usbd_control_buffer;

		switch (setup_data->bRequest) {
		case B0_DIO_ALL_VALUE_GET:
			buf[0] = (GPIOB_IDR & (~dio0.hiz)) | (dio0.value & dio0.hiz);
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_DIO_ALL_DIR_GET:
			buf[0] = dio0.dir;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_DIO_ALL_HIZ_GET:
			buf[0] = dio0.hiz;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_DIO_ALL_VALUE_SET:
			/* Set value of all OUTPUT pins */
			value_set(dio0.dir, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_ALL_VALUE_TOGGLE:
			/* Toggle value of all OUTPUT pins */
			value_toggle(dio0.dir);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_ALL_DIR_SET:
			dir_set(0xFF, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_DIO_ALL_HIZ_SET:
			hiz_set(0xFF, !!(setup_data->wValue & 0x1));
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

/**
 * @brief DIO control request
 * @details parse the control request send from the device
 */
bool dio0_setup_callback(usbd_device *dev,
				const struct usb_setup_data *setup_data)
{
	if (setup_data->bRequest >= 101 && setup_data->bRequest <= 161) {
		if (setup_data->bRequest >= 101 && setup_data->bRequest <= 120) {
			return single_command(dev, setup_data);
		} else if (setup_data->bRequest >= 121 && setup_data->bRequest <= 140) {
			return multiple_command(dev, setup_data);
		} else if (setup_data->bRequest >= 141 && setup_data->bRequest <= 160) {
			return all_command(dev, setup_data);
		}

		return true;
	}

	uint8_t *buf = usbd_control_buffer;

	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		switch (setup_data->bRequest) {
		case B0_MODE_GET:
			buf[0] = B0_MODE_BASIC;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &dio0.state, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_MODE_SET:
			ASSERT_STALL(B0_MODE_FROM_WVALUE(setup_data->wValue)
							== B0_MODE_BASIC);
			usbd_ep0_transfer(dev, setup_data, NULL, 0,
				(usbd_control_transfer_callback) dio0_reset);
		break;
		case B0_STATE_SET:
			switch(B0_STATE_FROM_WVALUE(setup_data->wValue)) {
			case B0_STATE_START: {
				/* Cannot be already in running state */
				ASSERT_STALL(dio0.state == B0_STATE_STOPPED);
				dio0.state = B0_STATE_RUNNING;

				/* Make pins to their configured state */
				uint8_t no_in_hiz = ~dio0.hiz;
				uint8_t input = (~dio0.dir) & no_in_hiz;
				uint8_t output = dio0.dir & no_in_hiz;

				/* setup input ones */
				gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, input);

				/* setup output ones */
				gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, output);
				gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_LOW, output);

				/* update values of changed output pins */
				uint8_t reset = (~dio0.value) & output;
				uint8_t set = dio0.value & output;
				GPIOB_BSRR = (reset << 16) | set;
			} break;
			case B0_STATE_STOP:
				/* already stopped? */
				ASSERT_STALL(dio0.state == B0_STATE_RUNNING);
				dio0.state = B0_STATE_STOPPED;

				/* Make all pin to HiZ */
				pins_protect(GPIOB, 0xFF);
			break;
			default:
			/* unknown state */
			goto stall;
			}

			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}
