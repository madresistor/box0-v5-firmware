/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/gpio.h>
#include "common/helper.h"

void pins_protect(uint32_t port, uint16_t pinmask)
{
	//gpio_mode_setup(port, GPIO_MODE_INPUT, GPIO_PUPD_NONE, pinmask);
	gpio_mode_setup(port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, pinmask);
	gpio_set_output_options(port, GPIO_OTYPE_OD, GPIO_OSPEED_HIGH, pinmask);
	gpio_set(port, pinmask);
}
