/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/usb/class/cdc.h>
#include <unicore-mx/stm32/crs.h>
#include <unicore-mx/stm32/flash.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/st_usbfs.h>
#include <unicore-mx/stm32/memorymap.h>
#include <unicore-mx/usbd/usbd.h>

#include <stdint.h>
#include "standard.h"
#include "dio.h"
#include "pwm.h"
#include "ain.h"
#include "aout.h"
#include "i2c.h"
#include "spi.h"
#include "common/handy.h"
#include "common/helper.h"
#include "config.h"

void prepare_uid(void);
void uint32_to_hex(uint32_t val, uint8_t buf []);

#define IFACENUM_DIO0 0
#define IFACENUM_PWM0 1
#define IFACENUM_PWM1 2
#define IFACENUM_AIN0 3
#define IFACENUM_AOUT0 4
#define IFACENUM_I2C0 5
#define IFACENUM_SPI0 6

#define STRING_CONF_1 4
#define STRING_DIO0 5
#define STRING_DIO0_PINx(x) (STRING_DIO0 + 1 + (x))

#define STRING_PWM0 14
#define STRING_PWM0_CHx(x) (STRING_PWM0 + 1 + (x))

#define STRING_PWM1 16
#define STRING_PWM1_CHx(x) (STRING_PWM1 + 1 + (x))

#define STRING_AIN0 18
#define STRING_AIN0_CHx(x) (STRING_AIN0 + 1 + (x))

#define STRING_AOUT0 23
#define STRING_AOUT0_CHx(x) (STRING_AOUT0 + 1 + (x))

#define STRING_I2C0 26
#define STRING_I2C0_SCL (STRING_I2C0 + 1)
#define STRING_I2C0_SDA (STRING_I2C0 + 2)

#define STRING_SPI0 29
#define STRING_SPI0_MISO (STRING_SPI0 + 1)
#define STRING_SPI0_MOSI (STRING_SPI0_MISO + 1)
#define STRING_SPI0_CLK (STRING_SPI0_MOSI + 1)
#define STRING_SPI0_SSx(x) (STRING_SPI0_CLK + (x) + 1)

#if (VERBOSE_STRING_DESC == 1)
# define DEV_STR(name, dev_ref) (const uint8_t *) ( name " (" dev_ref ")" )
#else
# define DEV_STR(name, dev_ref) (const uint8_t *)( name )
#endif

#define DEVICE_STRINGS_COUNT 36

uint8_t device_uid[25];
const uint8_t *device_strings_english[DEVICE_STRINGS_COUNT] = {
	(const uint8_t *) "Mad Resistor",
	(const uint8_t *) "Box0",
	device_uid,
	(const uint8_t *) "Box0",

	DEV_STR("DIO0", "GPIOB"),
		DEV_STR("0", "PB0"),
		DEV_STR("1", "PB1"),
		DEV_STR("2", "PB2"),
		DEV_STR("3", "PB3"),
		DEV_STR("4", "PB4"),
		DEV_STR("5", "PB5"),
		DEV_STR("6", "PB6"),
		DEV_STR("7", "PB7"),

	DEV_STR("PWM0", "TIM2"),
		DEV_STR("OUT0", "PB11"),

	DEV_STR("PWM1", "TIM1"),
		DEV_STR("OUT0", "PA8"),

	DEV_STR("AIN0", "ADC0, TIM15, DMA_CH1"),
		DEV_STR("CH0", "PA3"),
		DEV_STR("CH1", "PA2"),
		DEV_STR("CH2", "PA1"),
		DEV_STR("CH3", "PA0"),

	DEV_STR("AOUT0", "DAC, TIM6, DMA_CH3"),
		DEV_STR("CH0", "PA4"),
		DEV_STR("CH1", "PA5"),

	DEV_STR("I2C0", "I2C1, DMA_CH6, DMA_CH7"),
		DEV_STR("SCL", "PB8"),
		DEV_STR("SDA", "PB9"),

	DEV_STR("SPI0", "SPI2, DMA_CH4, DMA_CH5"),
		DEV_STR("MISO", "PB14"),
		DEV_STR("MOSI", "PB15"),
		DEV_STR("SCLK", "PB13"),
		DEV_STR("SS0", "PB12"),
		DEV_STR("SS1", "PC6"),
		DEV_STR("SS2", "PC7"),
		DEV_STR("SS3", "PC8"),
};

const struct usb_string_utf8_data device_strings[] = {{
	.data = device_strings_english,
	.count = DEVICE_STRINGS_COUNT,
	.lang_id = USB_LANGID_ENGLISH_UNITED_STATES
}, {
	.data = NULL
}};

/* =========================== DIO0 ================================ */

static const struct b0_dio0 box0_dio0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_dio0),
			B0_DIO, 0, IFACENUM_DIO0, STRING_DIO0),
	.label8 = FILL_B0_LABEL(sizeof(struct b0_label8),
		STRING_DIO0_PINx(0), STRING_DIO0_PINx(1),
		STRING_DIO0_PINx(2), STRING_DIO0_PINx(3), STRING_DIO0_PINx(4),
		STRING_DIO0_PINx(5), STRING_DIO0_PINx(6), STRING_DIO0_PINx(7)),
	.capab = FILL_B0_CAPAB(
		B0_DIO_CAPAB_OUTPUT | B0_DIO_CAPAB_INPUT | B0_DIO_CAPAB_HIZ),
	.count = FILL_B0_COUNT(DIO0_PIN_COUNT),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, 0, DEVICE_VOLTAGE),
};

const struct usb_interface_descriptor dio0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_DIO0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_DIO,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_DIO0
}};

/* ============================ PWM0 ================================ */

static const struct b0_pwm0 box0_pwm0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_pwm0),
					B0_PWM, 0, IFACENUM_PWM0, STRING_PWM0),
	.label1 = FILL_B0_LABEL(sizeof(struct b0_label1), STRING_PWM0_CHx(0)),
	.count = FILL_B0_COUNT(1),
	.bitsize1 = FILL_B0_BITSIZE(sizeof(struct b0_bitsize1), 32),
	.capab = FILL_B0_CAPAB(0),
	.speeddiv = FILL_B0_SPEED_DIV(65536, B0_SPEED_MHZ(48)),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, 0, DEVICE_VOLTAGE),
	.inital2 = FILL_B0_PWM_INITAL(sizeof(struct b0_pwm_inital2),
		B0_PWM_INITAL_LOW, B0_PWM_INITAL_HIGH),
	.symmetric2 = FILL_B0_PWM_SYMMETRIC(sizeof(struct b0_pwm_symmetric2),
		B0_PWM_SYMMETRIC_NO, B0_PWM_SYMMETRIC_YES),
};

static const struct usb_interface_descriptor pwm0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_PWM0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_PWM,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_PWM0,
}};


/* ============================= PWM1 =============================== */

static const struct b0_pwm1 box0_pwm1 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_pwm1),
				B0_PWM, 1, IFACENUM_PWM1, STRING_PWM1),
	.label1 = FILL_B0_LABEL(sizeof(struct b0_label1), STRING_PWM1_CHx(0)),
	.count = FILL_B0_COUNT(1),
	.bitsize1 = FILL_B0_BITSIZE(sizeof(struct b0_bitsize1), 16),
	.capab = FILL_B0_CAPAB(0),
	.speeddiv = FILL_B0_SPEED_DIV(65536, B0_SPEED_MHZ(48)),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, 0, DEVICE_VOLTAGE),
	.inital2 = FILL_B0_PWM_INITAL(sizeof(struct b0_pwm_inital2),
		B0_PWM_INITAL_LOW, B0_PWM_INITAL_HIGH),
	.symmetric2 = FILL_B0_PWM_SYMMETRIC(sizeof(struct b0_pwm_symmetric2),
		B0_PWM_SYMMETRIC_NO, B0_PWM_SYMMETRIC_YES),
};

static const struct usb_interface_descriptor pwm1_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_PWM1,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_PWM,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_PWM1,
}};

/* =============================== AIN0 ============================= */

/*
 * Designer note::
 * dont support same channel multiple times in channel sequence
 */
static const struct b0_ain0 box0_ain0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_ain0),
				B0_AIN, 0, IFACENUM_AIN0, STRING_AIN0),
	.label4 = FILL_B0_LABEL(sizeof(struct b0_label4),
			STRING_AIN0_CHx(0), STRING_AIN0_CHx(1),
			STRING_AIN0_CHx(2), STRING_AIN0_CHx(3),),
	.capab = FILL_B0_CAPAB(
		B0_AIN_CAPAB_FORMAT_BINARY | B0_AIN_CAPAB_ALIGN_LSB),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, AIN_REF_LOW, AIN_REF_HIGH),
	.count = FILL_B0_COUNT(AIN0_CHAN_COUNT),
	.buffer = FILL_B0_BUFFER(AIN0_BUFFER_SIZE),

#if (AIN0_1MS_HACK == 1)
	/* notify host that we have a special 1MS */
	.snapshot_bitsize_speed_raw1 = FILL_B0_BITSIZE_SPEED_RAW(
		sizeof(struct snapshot_bitsize_speed_raw1),
		B0_BITSIZE_SPEED_SNAPSHOT_USE, 12, 8, 0, B0_SPEED_MHZ(1)),
#endif

	.snapshot_bitsize_speed_dec = FILL_B0_BITSIZE_SPEED_DEC(
		B0_BITSIZE_SPEED_SNAPSHOT_USE, 12, 8, 0, AIN0_SNAPSHOT_MAX_SPEED,
		AIN0_SNAPSHOT_MAX_SPEED, 1),

	.stream_bitsize_speed_raw6 = FILL_B0_BITSIZE_SPEED_RAW(
		sizeof(struct b0_bitsize_speed_raw6),
		B0_BITSIZE_SPEED_STREAM_USE, 12, 8, 0, B0_SPEED_KHZ(100),
		B0_SPEED_KHZ(50), B0_SPEED_KHZ(10), B0_SPEED_KHZ(5),
		B0_SPEED_KHZ(1), 100),

	.stream_bitsize_speed_raw1 = FILL_B0_BITSIZE_SPEED_RAW(
		sizeof(struct b0_bitsize_speed_raw1),
		B0_BITSIZE_SPEED_STREAM_USE, 8, 0, 0, B0_SPEED_KHZ(200))
};

static const struct usb_endpoint_descriptor ain0_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = AIN0_EP_ADDR,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = AIN0_BULK_EP_SIZE,
	.bInterval = 0,
}};

const struct usb_interface_descriptor ain0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_AIN0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_AIN,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_AIN0,

	.endpoint = ain0_endp
}};

/* ============================= AOUT0 ============================== */

static const struct b0_aout0 box0_aout0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_aout0),
				B0_AOUT, 0, IFACENUM_AOUT0, STRING_AOUT0),
	.label2 = FILL_B0_LABEL(sizeof(struct b0_label2),
					STRING_AOUT0_CHx(0), STRING_AOUT0_CHx(1)),
	.buffer = FILL_B0_BUFFER(AOUT0_BUFFER_SIZE),
	.capab = FILL_B0_CAPAB(B0_AOUT_CAPAB_FORMAT_BINARY |
				B0_AOUT_CAPAB_ALIGN_LSB | B0_AOUT_CAPAB_REPEAT),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, AOUT_REF_LOW, AOUT_REF_HIGH),
	.count = FILL_B0_COUNT(AOUT0_CHAN_COUNT),

	.snapshot_bitsize_speed_dec = FILL_B0_BITSIZE_SPEED_DEC(
		B0_BITSIZE_SPEED_SNAPSHOT_USE, 12, 8, 0, B0_SPEED_MHZ(1),
		B0_SPEED_MHZ(1), 1),

	.stream_bitsize_speed_raw6 = FILL_B0_BITSIZE_SPEED_RAW(
		sizeof(struct b0_bitsize_speed_raw6),
		B0_BITSIZE_SPEED_STREAM_USE, 12, 8, 0, B0_SPEED_KHZ(100),
		B0_SPEED_KHZ(50), B0_SPEED_KHZ(10), B0_SPEED_KHZ(5),
		B0_SPEED_KHZ(1), 100),

	.stream_bitsize_speed_raw1 = FILL_B0_BITSIZE_SPEED_RAW(
		sizeof(struct b0_bitsize_speed_raw1),
		B0_BITSIZE_SPEED_STREAM_USE, 8, 0, 0, B0_SPEED_KHZ(200))
};

static const struct usb_endpoint_descriptor aout0_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = AOUT0_EP_ADDR,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = AOUT0_BULK_EP_SIZE,
	.bInterval = 0,
}};

const struct usb_interface_descriptor aout0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_AOUT0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_AOUT,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_AOUT0,

	.endpoint = aout0_endp
}};

/* ========================= I2C0 =================================== */

static const struct b0_i2c0 box0_i2c0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_i2c0),
					B0_I2C, 0, IFACENUM_I2C0, STRING_I2C0),
	.label2 = FILL_B0_LABEL(sizeof(struct b0_label2),
		STRING_I2C0_SCL, STRING_I2C0_SDA),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, 0, DEVICE_VOLTAGE),
	.version3 = FILL_B0_I2C_VERSION(sizeof(struct b0_i2c_version3),
		B0_I2C_VERSION_SM, B0_I2C_VERSION_FM, B0_I2C_VERSION_FMPLUS),
	.buffer = FILL_B0_BUFFER(I2C0_BUFFER_SIZE)
};

static const struct usb_endpoint_descriptor i2c0_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = I2C0_EP_ADDR_IN,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = I2C0_BULK_EP_SIZE,
	.bInterval = 0,
}, {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = I2C0_EP_ADDR_OUT,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = I2C0_BULK_EP_SIZE,
	.bInterval = 0,
}};

static const struct usb_interface_descriptor i2c0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_I2C0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_I2C,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_I2C0,

	.endpoint = i2c0_endp,
}};

/* ============================ SPI0 ================================ */

static const struct b0_spi0 box0_spi0 ALIGNED_2BYTE = {
	.header = FILL_B0_MODULE(sizeof(struct b0_spi0),
		B0_SPI, 0, IFACENUM_SPI0, STRING_SPI0),
	.ref = FILL_B0_REF(B0_REF_TYPE_VOLTAGE, 0, DEVICE_VOLTAGE),
	.buffer = FILL_B0_BUFFER(SPI0_BUFFER_SIZE),
	.count = FILL_B0_COUNT(SPI0_SS_COUNT),
	.label7 = FILL_B0_LABEL(sizeof(struct b0_label7),
		STRING_SPI0_CLK, STRING_SPI0_MOSI, STRING_SPI0_MISO,
		STRING_SPI0_SSx(0), STRING_SPI0_SSx(1),
		STRING_SPI0_SSx(2), STRING_SPI0_SSx(3)),
	.speed_raw8 = FILL_B0_SPEED_RAW(sizeof(struct b0_speed_raw8),
		187500, B0_SPEED_KHZ(375), B0_SPEED_KHZ(750),
		B0_SPEED_KHZ(1500), B0_SPEED_MHZ(3), B0_SPEED_MHZ(12),
		B0_SPEED_MHZ(6), B0_SPEED_MHZ(24),),
	.bitsize13 = FILL_B0_BITSIZE(sizeof(struct b0_bitsize13),
			4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
};

static const struct usb_endpoint_descriptor spi0_endp[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = SPI0_EP_ADDR_IN,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = SPI0_BULK_EP_SIZE,
	.bInterval = 0,
}, {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = SPI0_EP_ADDR_OUT,
	.bmAttributes = USB_ENDPOINT_ATTR_BULK,
	.wMaxPacketSize = SPI0_BULK_EP_SIZE,
	.bInterval = 0,
}};

static const struct usb_interface_descriptor spi0_iface[] = {{
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = IFACENUM_SPI0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_VENDOR,
	.bInterfaceSubClass = B0_SPI,
	.bInterfaceProtocol = 0,
	.iInterface = STRING_SPI0,

	.endpoint = spi0_endp,
}};

/* ================================================================== */

static const struct usb_interface ifaces[] = {{
	.num_altsetting = 1,
	.altsetting = dio0_iface,
}, {
	.num_altsetting = 1,
	.altsetting = pwm0_iface,
}, {
	.num_altsetting = 1,
	.altsetting = pwm1_iface,
}, {
	.num_altsetting = 1,
	.altsetting = ain0_iface,
}, {
	.num_altsetting = 1,
	.altsetting = aout0_iface,
}, {
	.num_altsetting = 1,
	.altsetting = i2c0_iface,
}, {
	.num_altsetting = 1,
	.altsetting = spi0_iface,
}};

const struct usb_config_descriptor device_configurations[] = {{
	.bLength = USB_DT_CONFIGURATION_SIZE,
	.bDescriptorType = USB_DT_CONFIGURATION,
	.wTotalLength = 0,
	.bNumInterfaces = ARRAY_LEN(ifaces),
	.bConfigurationValue = 1,
	.iConfiguration = STRING_CONF_1,
	.bmAttributes = 0x80,
	.bMaxPower = 250, //2.5W

	.interface = ifaces,
	.string = device_strings
}};

const struct usb_device_descriptor device_descriptor ALIGNED_2BYTE = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x0200,
	.bDeviceClass = USB_CLASS_VENDOR,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = USB_EP0_SIZE,
	.idVendor = 0x1d50,
	.idProduct = 0x8085,
	.bcdDevice = 0x0501,
	.iManufacturer = 1, /* Mad Resistor */
	.iProduct = 2, /* Box0 */
	.iSerialNumber = 3, /* <device_uid> */
	.bNumConfigurations = 1,

	.config = device_configurations,
	.string = device_strings
};

/**
 * Table of Box0 modules available in the device.
 */
const struct b0_module *box0_modules[] = {
	(const struct b0_module *) &box0_dio0,
	(const struct b0_module *) &box0_pwm0,
	(const struct b0_module *) &box0_pwm1,
	(const struct b0_module *) &box0_ain0,
	(const struct b0_module *) &box0_aout0,
	(const struct b0_module *) &box0_i2c0,
	(const struct b0_module *) &box0_spi0,
};

/**
 * Box0 device descriptor
 */
const struct b0_device box0_device ALIGNED_2BYTE = {
	.bLength = B0_DT_DEVICE_SIZE,
	.bDescriptorType = B0_DESC_DEVICE,
	.bcdBOX0 = 0x0020, /* 00.20 */
	.bNumModules = 7,
	.modules = box0_modules
};

/**
 * @brief Generate device ID
 * @details Generate device ID by using the unique ID inside the microcontroller
 */
void prepare_uid(void)
{
	uint32_to_hex(DESIG_UNIQUE_ID0, device_uid);
	uint32_to_hex(DESIG_UNIQUE_ID1, device_uid + 8);
	uint32_to_hex(DESIG_UNIQUE_ID2, device_uid + 16);
	device_uid[24] = '\0';
}

/**
 * convert the uint32_t value to its ascii equivalent (hex)
 * @param[in] val Value to hexify
 * @param[out] buffer Buffer to write ascii character
 * @note @a need to be atleast able to hold 8 char
 */
void uint32_to_hex(uint32_t val, uint8_t buf [])
{
	unsigned i;

	for (i = 0; i < 8; i++) {
		buf[i] = "0123456789ABCDEF"[val & 0xF];
		val >>= 4;
	}
}
