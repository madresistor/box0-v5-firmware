/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_DEVICE_H
#define BOX0V5_DEVICE_H

#include "standard.h"
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>

#define BOX0V5_PS_EN_GET 201
#define BOX0V5_PS_EN_SET 202
#define BOX0V5_PS_OC_GET 203
#define BOX0V5_PS_OC_ACK 204

/*
 * Note: Analog and digital are to independent power supply units.
 *
 * Power supply enable get
 * -----------------------
 * bmRequestType: device->host, device, vendor
 * bRequest: BOX0V5_PS_EN_GET
 * wValue: 0x0000
 * wIndex: 0x0000
 * wLength: 1
 * Data: BIT0 = Analog, BIT1 = Digital
 *
 *
 * Power supply enable set
 * -----------------------
 * bmRequestType: host->device, device, vendor
 * bRequest: BOX0V5_PS_EN_SET
 * wValue: HIGH8(mask) LOW8(value)
 * wIndex: 0x0000
 * wLength: 0
 * Data: None
 * @note: only those bits (in @a value) will be
 *   considered valid whose mask bit (in @a mask) is high.
 * @note: overcurrent flag is cleared for the specific unit that are in mask (if any)
 *
 * Power supply over-current get
 * -----------------------------
 * bmRequestType: device->host, device, vendor
 * bRequest: BOX0V5_PS_OC_GET
 * wValue: 0x0000
 * wIndex: 0x0000
 * wLength: 1
 * Data: BIT0 = Analog, BIT1 = Digital
 *
 * Power supply over-current acknowledge
 * -----------------------------
 * bmRequestType: host->device, device, vendor
 * bRequest: BOX0V5_PS_OC_ACK
 * wValue: HIGH8(0) LOW8(mask)
 * wIndex: 0x0000
 * wLength: 0
 * Data: None
 * @note Acknowledge overcurrent for the specific unit.
 *  if the mask contain bit0 high, analog over-current flag is cleared.
 *  if the mask contain bit1 high, digital over-current flag is cleared.
 * @note if there is no over-current condition in any of the requested unit (in mask),
 *  then the command will STALL
 */

/* Non volatile memory read/write */
#define BOX0V5_NVM_GET 205
#define BOX0V5_NVM_SET 206

/*
 * Non volatile memory is the content that can be written by host.
 * Usecase: calibration values
 * 2KB of NVM is available
 *
 * Non volatile memory read
 * ------------------------
 * bmRequestType: device->host, device, vendor
 * bRequest: BOX0V5_NVM_GET
 * wValue: 0x0000
 * wIndex: <memory offset>
 * wLength: <Number of bytes to read>
 * Data: <data>
 * stall: Out of range memory
 * note: wIndex should be multiple of 2 (or else hard-fault)
 *
 * Non volatile memory write
 * -------------------------
 * bmRequestType: host->device, device, vendor
 * bRequest: BOX0V5_NVM_SET
 * wValue: 0x0000
 * wIndex: <memory offset>
 * wLength: <Number of bytes to write>
 * Data: <data>
 * stall: Out of range memory
 * note: The non written memory range will have undefined content.
 *   So, it is recommended to always write the full 2KB NVM.
 * note: wIndex should be multiple of 2 (or else hard-fault)
 */

bool device_setup_callback(usbd_device *dev,
				const struct usb_setup_data *setup_data);

void led_act_init(void);
void led_act_en(bool on);
void led_act_toggle(void);

#endif
