/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_AIN_H
#define BOX0V5_AIN_H

#include <stdint.h>
#include "standard.h"
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>
#include "config.h"

__BEGIN_DECLS

/* BIT0 */
#define B0_AIN_CAPAB_FORMAT_BINARY 0x00
#define B0_AIN_CAPAB_FORMAT_2COMPL 0x01
/* BIT1 */
#define B0_AIN_CAPAB_ALIGN_LSB 0x00
#define B0_AIN_CAPAB_ALIGN_MSB 0x02
/* BIT2 */
#define B0_AIN_CAPAB_ENDIAN_LITTLE 0x00
#define B0_AIN_CAPAB_ENDIAN_BIG 0x04

struct b0_ain0 {
	struct b0_module header;
	struct b0_count count;
	struct b0_buffer buffer;
	struct b0_label4 label4;
	struct b0_capab capab;
	struct b0_ref ref;

#if (AIN0_1MS_HACK == 1)
	/* send a special 1MS speed */
	struct b0_bitsize_speed_raw1 snapshot_bitsize_speed_raw1;
#endif

	struct b0_bitsize_speed_dec snapshot_bitsize_speed_dec;

	struct b0_bitsize_speed_raw6 stream_bitsize_speed_raw6;
	struct b0_bitsize_speed_raw1 stream_bitsize_speed_raw1;
} PACKED;

void ain0_init(void);
void ain0_deinit(void);
bool ain0_setup_callback(usbd_device *dev,
			const struct usb_setup_data *setup_data);
void adc_calibrate(void);
void ain0_dma_poll(void);

__END_DECLS

#endif
