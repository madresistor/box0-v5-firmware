/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/dac.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/cm3/assert.h>

#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include "standard.h"
#include "aout.h"
#include "common/handy.h"
#include "common/helper.h"
#include "config.h"
#include "usb.h"
#include "power.h"

void aout0_stop(void);
void aout0_snapshot_start(uint32_t sample_count);
void aout0_stream_start(void);
void aout0_speed_bitsize_chan_seq_refresh(void);

static struct {
	uint8_t mode;
	uint8_t state;

	struct {
		uint8_t count;
		uint8_t values[AOUT0_CHAN_COUNT];
	} chan_seq;

	uint8_t bitsize;
	uint32_t speed;

	bool transfer_pending; /* stream only */

	uint32_t repeat; /* snapshot only */
	volatile uint32_t repeat_i; /* snapshot only */

	union {
		uint16_t last_dma_chan_cnt; /* stream only */
		uint16_t count; /* snapshot only */
	};

	/* 32bit aligned buffer so that DMA can copy to
	 *  dual dac channel register (and dac channel bitsize=12) */
	uint8_t buffer[AOUT0_BUFFER_SIZE] ALIGNED_4BYTE;
} aout0;

static inline void output_voltage_null(void)
{
	/* Initalize DAC (Timer trigger DMA, DMA copy to DAC register)
	 *  - Disable buffering
	 *  - Enable output */
	DAC_CR =  DAC_CR_EN1 | DAC_CR_BOFF1 | DAC_CR_EN2 | DAC_CR_BOFF2;

	/* Force the DAC to output at VDDA/2,
	 *  so that the OpAmp can output 0V */
	DAC_DHR12RD = 0x07FF07FF;
}

void aout0_init(void)
{
	/* enable power supply */
	power_state_set(POWER_ANALOG, POWER_ON);

	/* enable all peripherials clock */
	rcc_periph_clock_enable(RCC_DAC);
	rcc_periph_clock_enable(RCC_TIM6);

	/* move AOUT to STOPPED state on initalization */
	aout0.state = B0_STATE_STOPPED;

	/* Disable counter */
	TIM6_CR1 &= ~TIM_CR1_CEN;

	/* set timer in master mode to generate trigger on update event */
	TIM6_CR2 = (TIM6_CR2 & (~TIM_CR2_MMS_MASK)) | TIM_CR2_MMS_UPDATE;

	/* timer dma request enable */
	TIM6_DIER = 0x0100;

	output_voltage_null();

	nvic_set_priority(NVIC_TIM6_DAC_IRQ, NVIC_PRIO_LOW);
	nvic_enable_irq(NVIC_TIM6_DAC_IRQ);

	DMA1_CMAR3 = (uint32_t) aout0.buffer;

	nvic_set_priority(NVIC_DMA1_CHANNEL2_3_IRQ, NVIC_PRIO_VERY_HIGH);
	nvic_enable_irq(NVIC_DMA1_CHANNEL2_3_IRQ);

	/* default: bitsize = 12, speed = 1MSPS */
	aout0.speed = B0_SPEED_MHZ(1);
	aout0.bitsize = 12;
	/* default: channel-sequence = {0} */
	aout0.chan_seq.count = 1;
	aout0.chan_seq.values[0] = 0;
	aout0_speed_bitsize_chan_seq_refresh();

	aout0.repeat = 0;

	usbd_transfer_cancel_ep(dev_global, AOUT0_EP_ADDR);

	/* reset DTOG bit */
	usbd_set_ep_dtog(dev_global, AOUT0_EP_ADDR, false);
}

/* it should feel like that (to host),
 * AOUT is running at a speed, and
 * is resources are destributed among channels
 * example: if speed = 10Khz, each channel is sampled at 10Khz / <number-of-channel>.
 */
void aout0_speed_bitsize_chan_seq_refresh(void)
{
	/* apply the destination register of DMA1 Channel3
	 * this is dependent on bitsize and channel list
	 * so when ever bitsize change or channel list, this need to be called
	 */
	static const struct {
		uint32_t dma_psize;
		uint32_t dma_msize;
		volatile uint32_t *dac_reg;
	} cfg_12bit_both = { /* Increment 4 */
		.dma_psize = DMA_CCR_PSIZE_32BIT,
		.dma_msize = DMA_CCR_MSIZE_32BIT,
		.dac_reg = &DAC_DHR12RD
	}, cfg_8bit_both = { /* Increment 2 */
		.dma_psize = DMA_CCR_PSIZE_16BIT,
		.dma_msize = DMA_CCR_MSIZE_16BIT,
		.dac_reg = &DAC_DHR8RD
	}, cfg_12bit_ch0 = { /* Increment 2 */
		.dma_psize = DMA_CCR_PSIZE_16BIT,
		.dma_msize = DMA_CCR_MSIZE_16BIT,
		.dac_reg = &DAC_DHR12R1
	}, cfg_12bit_ch1 = { /* Increment 2 */
		.dma_psize = DMA_CCR_PSIZE_16BIT,
		.dma_msize = DMA_CCR_MSIZE_16BIT,
		.dac_reg = &DAC_DHR12R2
	}, cfg_8bit_ch0 = { /* Increment 1 */
		.dma_psize = DMA_CCR_PSIZE_8BIT,
		.dma_msize = DMA_CCR_MSIZE_8BIT,
		.dac_reg = &DAC_DHR8R1
	}, cfg_8bit_ch1 = { /* Increment 1 */
		.dma_psize = DMA_CCR_PSIZE_8BIT,
		.dma_msize = DMA_CCR_MSIZE_8BIT,
		.dac_reg = &DAC_DHR8R2
	}, *cfg;

	if (aout0.chan_seq.count == 2) {
		cfg = (aout0.bitsize == 12) ? &cfg_12bit_both : &cfg_8bit_both;
	} else {
		if (aout0.bitsize == 12) {
			cfg = aout0.chan_seq.values[0] ? &cfg_12bit_ch1 : &cfg_12bit_ch0;
		} else {
			cfg = aout0.chan_seq.values[0] ? &cfg_8bit_ch1 : &cfg_8bit_ch0;
		}
	}

	DMA1_CCR3 = DMA_CCR_PL_VERY_HIGH | DMA_CCR_MINC | DMA_CCR_DIR |
				cfg->dma_msize | cfg->dma_psize | DMA_CCR_CIRC;
	DMA1_CPAR3 = (uint32_t) cfg->dac_reg;

	/* ============= channel list ============= */
	/* CH0 -> dac-ch1(PA4) | CH1 -> dac-ch2(PA5) */
	if (aout0.chan_seq.count == 2) {
		/* enable dual channels */
		gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO4 | GPIO5);
	} else {
		/* enable the pin and HiZ the other one */
		uint16_t pin = aout0.chan_seq.values[0] ? GPIO5 : GPIO4;
		gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, pin);
		pins_protect(GPIOA, pin ^ (GPIO5 | GPIO4));
		/* FIXME: null voltage output on other pin */
	}

	/* =============== speed ================ */
	/*
	 * example sample_rate = 96000:
	 *
	 * DAC bitsize = 12 ie bytes_per_sample = 2
	 * generate trigger at 96Khz
	 * timer input clock: 48Mhz
	 * prescaler(9) divide it by 0, PS_CLK = 48Mhz
	 * period(499) register of 500, so effectively generating 96Khz TRGO
	 */

	/* easy configuration */
	uint16_t div;
	if (aout0.speed >= B0_SPEED_KHZ(1)) {
		div = 1;
	} else if (aout0.speed >= 100) {
		div = 8;
	} else if (aout0.speed >= 10) {
		div = 80;
	} else {
		div = 800;
	}

	TIM6_PSC = div - 1;
	TIM6_ARR = (B0_SPEED_MHZ(48) / (aout0.speed * div)) - 1;
}


static usbd_control_transfer_feedback aout0_bitsize_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *_arg)
{
	struct b0_bitsize_speed_arg *arg = _arg->buffer;

	/* Check for valid bitsize */
	ASSERT_STALL(arg->bBitsize == 12 || arg->bBitsize == 8);

	/* Check for valid speed */
	ASSERT_STALL(arg->dSpeed > 0 && arg->dSpeed <= B0_SPEED_MHZ(1));

	aout0.bitsize = arg->bBitsize;
	aout0.speed = arg->dSpeed;
	aout0_speed_bitsize_chan_seq_refresh();
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback aout0_state_set_snapshot_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	uint32_t count = CAST_TO_UINT32_PTR(arg->buffer)[0];

	unsigned bytes_per_sample = BIT2BYTE(aout0.bitsize);
	unsigned max_count = AOUT0_BUFFER_SIZE / bytes_per_sample;

	ASSERT_STALL(count > 0 && count <= max_count);

	aout0_snapshot_start(count);
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback aout0_chan_seq_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	uint8_t *buf = arg->buffer;

	if (arg->length == 2) {
		ASSERT_STALL(buf[0] == 0 && buf[1] == 1);
	} else {
		ASSERT_STALL(buf[0] < 2);
	}

	/* apply the list */
	memcpy(aout0.chan_seq.values, arg->buffer, arg->length);
	aout0.chan_seq.count = arg->length;
	aout0_speed_bitsize_chan_seq_refresh();
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback aout0_repeat_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	aout0.repeat = CAST_TO_UINT32_PTR(arg->buffer)[0];
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;
}

static usbd_control_transfer_feedback aout0_mode_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	/* STOP any ongoing process if we are in running state */
	if (aout0.state == B0_STATE_RUNNING) {
		aout0_stop();
	}

	usbd_transfer_cancel_ep(dev, AOUT0_EP_ADDR);

	aout0.mode = usbd_control_buffer[0];
	aout0.bitsize = 12;
	aout0.chan_seq.count = 1;
	aout0.chan_seq.values[0] = 0;

	switch (aout0.mode) {
	case B0_MODE_SNAPSHOT:
		aout0.speed = B0_SPEED_MHZ(1);
	break;
	case B0_MODE_STREAM:
		aout0.speed = B0_SPEED_KHZ(100);
	break;
	}

	aout0_speed_bitsize_chan_seq_refresh();

	return USBD_CONTROL_TRANSFER_OK;
}

bool aout0_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data)
{
	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		switch (setup_data->bRequest) {
		case B0_BITSIZE_SPEED_GET: {
			struct b0_bitsize_speed_arg *arg = (void *) usbd_control_buffer;
			arg->bBitsize = aout0.bitsize;
			arg->dSpeed = aout0.speed;
			usbd_ep0_transfer(dev, setup_data, arg, sizeof(*arg), NULL);
		} break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &aout0.state, 1, NULL);
		break;
		case B0_CHAN_SEQ_GET:
			usbd_ep0_transfer(dev, setup_data, aout0.chan_seq.values,
								aout0.chan_seq.count, NULL);
		break;
		case B0_REPEAT_GET:
			usbd_ep0_transfer(dev, setup_data, &aout0.repeat, 4, NULL);
		break;
		case B0_MODE_GET:
			usbd_ep0_transfer(dev, setup_data, &aout0.mode, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		uint8_t *buf = usbd_control_buffer;

		switch (setup_data->bRequest) {
		case B0_BITSIZE_SPEED_SET:
			ASSERT_STALL(aout0.state == B0_STATE_STOPPED);
			ASSERT_STALL(setup_data->wLength
						== sizeof(struct b0_bitsize_speed_arg));
			usbd_ep0_transfer(dev, setup_data, buf,
				sizeof(struct b0_bitsize_speed_arg),
				aout0_bitsize_speed_set_callback);
		break;
		case B0_STATE_SET:
			switch (B0_STATE_FROM_WVALUE(setup_data->wValue)) {
			case B0_STATE_START:
				ASSERT_STALL(aout0.state == B0_STATE_STOPPED);

				switch (aout0.mode) {
				case B0_MODE_SNAPSHOT:
					ASSERT_STALL(setup_data->wLength == 4);
					usbd_ep0_transfer(dev, setup_data, buf, 4,
							aout0_state_set_snapshot_callback);
				break;
				case B0_MODE_STREAM:
					ASSERT_STALL(setup_data->wLength == 0);
					usbd_ep0_transfer(dev, setup_data, NULL, 0,
						(usbd_control_transfer_callback) aout0_stream_start);
				break;
				default:
				goto stall;
				}
			break;
			case B0_STATE_STOP:
				ASSERT_STALL(aout0.state == B0_STATE_RUNNING);
				usbd_ep0_transfer(dev, setup_data, NULL, 0,
					(usbd_control_transfer_callback) aout0_stop);
			break;
			default:
			goto stall;
			}
		break;
		case B0_MODE_SET: {
			buf[0] = B0_MODE_FROM_WVALUE(setup_data->wValue);
			ASSERT_STALL(buf[0] == B0_MODE_SNAPSHOT || buf[0] == B0_MODE_STREAM);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, aout0_mode_set_callback);
		} break;
		case B0_CHAN_SEQ_SET:
			ASSERT_STALL(aout0.state == B0_STATE_STOPPED);
			ASSERT_STALL(setup_data->wLength > 0);
			ASSERT_STALL(setup_data->wLength <= AOUT0_CHAN_COUNT);
			usbd_ep0_transfer(dev, setup_data, buf, setup_data->wLength,
					aout0_chan_seq_set_callback);
		break;
		case B0_REPEAT_SET:
			ASSERT_STALL(aout0.state == B0_STATE_STOPPED);
			ASSERT_STALL(aout0.mode == B0_MODE_SNAPSHOT);
			ASSERT_STALL(setup_data->wLength == 4);
			usbd_ep0_transfer(dev, setup_data, buf, 4, aout0_repeat_set_callback);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

static void aout0_stream_callback(usbd_device *dev,
	const usbd_transfer *transfer, usbd_transfer_status status,
	usbd_urb_id urb_id)
{
	if (status == USBD_ONE_PACKET_DATA) {
		/* A packet has been received, we can start output */
		TIM6_CR1 |= TIM_CR1_CEN;
		return;
	}

	aout0.transfer_pending = false;

	if (status == USBD_SUCCESS) {
		return;
	}

	if (status != USBD_ERR_CANCEL) {
		aout0_stop();
	}
}

void aout0_stream_start(void)
{
	aout0.transfer_pending = false;
	aout0.state = B0_STATE_RUNNING;

	unsigned bytes_per_sample = BIT2BYTE(aout0.bitsize);
	unsigned bytes_per_trigger = bytes_per_sample  * aout0.chan_seq.count;

	aout0.last_dma_chan_cnt = AOUT0_BUFFER_SIZE / bytes_per_sample;
	DMA1_CNDTR3 = AOUT0_BUFFER_SIZE / bytes_per_trigger;
	DMA1_IFCR = DMA_IFCR_CGIF3;
	DMA1_CCR3 = (DMA1_CCR3 & ~(DMA_CCR_TCIE)) | DMA_CCR_EN;

	/* Place transfer request and enable timer in transfer callback.
	 * The buffer is written completely. */
	const usbd_transfer first_full_transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = AOUT0_EP_ADDR,
		.ep_size = AOUT0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = aout0.buffer,
		.length = AOUT0_BUFFER_SIZE,
		.flags = USBD_FLAG_PER_PACKET_CALLBACK,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = aout0_stream_callback
	};

	if (usbd_transfer_submit(dev_global, &first_full_transfer) == USBD_INVALID_URB_ID) {
		/* Could not submit transfer */
		aout0_stop();
		return;
	}

	aout0.transfer_pending = true;
}

void aout_snapshot_data_callback(usbd_device *dev,
	const usbd_transfer *transfer, usbd_transfer_status status,
	usbd_urb_id urb_id)
{
	/* Problem in receiving data */
	if (status != USBD_SUCCESS) {
		aout0.state = B0_STATE_STOPPED;
		return;
	}

	/* the number of times to repeat */
	aout0.repeat_i = aout0.repeat;

	/* only output the no of sample the host requested */
	DMA1_CNDTR3 = aout0.count / aout0.chan_seq.count;

	/* Clear previous flags */
	DMA1_IFCR = DMA_IFCR_CGIF3;

	/* Enable DMA channel */
	uint32_t reg32 = DMA1_CCR3 | DMA_CCR_EN;

	if (aout0.repeat) {
		reg32 |= DMA_CCR_TCIE; /* MUTLIPLE */
	} else {
		reg32 &= ~DMA_CCR_TCIE; /* INFINITE */
	}

	DMA1_CCR3 = reg32;

	/* Start timer */
	TIM6_CR1 |= TIM_CR1_CEN;
}

void aout0_snapshot_start(uint32_t count)
{
	aout0.state = B0_STATE_RUNNING;
	aout0.count = count;
	unsigned bytes_per_sample = BIT2BYTE(aout0.bitsize);

	usbd_transfer_cancel_ep(dev_global, AOUT0_EP_ADDR);

	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = AOUT0_EP_ADDR,
		.ep_size = AOUT0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = aout0.buffer,
		.length = count * bytes_per_sample,
		.flags = USBD_FLAG_SHORT_PACKET,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = aout_snapshot_data_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}

void aout0_dma_poll(void)
{
	if (aout0.state == B0_STATE_STOPPED) {
		/* Not running, ignore */
		return;
	}

	if (aout0.mode != B0_MODE_STREAM) {
		/* Not in streaming */
		return;
	}

	if (aout0.transfer_pending) {
		/* We cannot do anything because a transfer is already in progress */
		return;
	}

	unsigned byte_per_sample = BIT2BYTE(aout0.bitsize);
	/* unsigned byte_per_trigger = byte_per_sample * aout0.chan_seq.count; */

	/* We have some data to transfer? */
	uint16_t current_cnt = DMA1_CNDTR3 * aout0.chan_seq.count;
	if (current_cnt == aout0.last_dma_chan_cnt) {
		/* Nop, no data! */
		return;
	}

	unsigned consumed;
	if (current_cnt < aout0.last_dma_chan_cnt) {
		consumed = aout0.last_dma_chan_cnt - current_cnt;
	} else {
		consumed = aout0.last_dma_chan_cnt;
	}

	/* Assure that atleast one packet number of samples are available */
	unsigned samples_per_packet = AOUT0_BULK_EP_SIZE / byte_per_sample;
	if (consumed < samples_per_packet) {
		/* Nop, not enough! */
		return;
	}

	unsigned offset = AOUT0_BUFFER_SIZE - (aout0.last_dma_chan_cnt * byte_per_sample);
	void *data = aout0.buffer + offset;

	unsigned recv_samples = (consumed / samples_per_packet) * samples_per_packet;

	aout0.last_dma_chan_cnt -= recv_samples;
	if (!aout0.last_dma_chan_cnt) {
		/* Note: memory can only be used till end. (fake circular queue) */
		aout0.last_dma_chan_cnt = AOUT0_BUFFER_SIZE / byte_per_sample;
	}

	/* Get new data */
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = AOUT0_EP_ADDR,
		.ep_size = AOUT0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = data,
		.length = recv_samples * byte_per_sample,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = aout0_stream_callback
	};

	if (usbd_transfer_submit(dev_global, &transfer) == USBD_INVALID_URB_ID) {
		/* Could not submit transfer */
		aout0_stop();
		return;
	}

	aout0.transfer_pending = true;
}

void aout0_stop(void)
{
	/* disable dma */
	DMA1_CCR3 &= ~DMA_CCR_EN;

	/* disable timer */
	TIM6_CR1 &= ~TIM_CR1_CEN;

	output_voltage_null();

	aout0.state = B0_STATE_STOPPED;
	aout0.transfer_pending = false;
	usbd_transfer_cancel_ep(dev_global, AOUT0_EP_ADDR);
}

void tim6_dac_isr(void)
{
	/* DAC Overrun.
	 * Reason: DMA and timer are writing to fast
	 */
	aout0_deinit();
}

void dma1_channel2_3_isr(void)
{
	/* DMA Channel3: Transfer Complete
	 * If Repeat > 0, it will decrement the value of reapeat on every transfer complete.
	 *  After repeat_i goes to 0, it stop the waveform generation
	 */
	if (DMA1_ISR & DMA_ISR_TCIF3) {
		aout0.repeat_i--;
		if (!aout0.repeat_i) {
			aout0_stop();
		}
	}

	/* DMA Channel3: Transfer Error */
	if (DMA1_ISR & DMA_ISR_TEIF3) {
		aout0_deinit();
	}

	DMA1_IFCR = DMA_IFCR_CGIF3;
}

void aout0_deinit(void)
{
	/* deinit pins */
	pins_protect(GPIOA, GPIO4 | GPIO5);

	/* deinit timer */
	rcc_periph_clock_disable(RCC_TIM6);

	/* deinit DAC */
	rcc_periph_clock_enable(RCC_DAC);
	nvic_disable_irq(NVIC_TIM6_DAC_IRQ);
	output_voltage_null();

	/* deinit DMA */
	DMA1_CCR3 &= ~DMA_CCR_EN;
	nvic_disable_irq(NVIC_DMA1_CHANNEL2_3_IRQ);
}
