/*
 * This file is part of box0-v5.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_COMMON_DEBUG_H
#define BOX0_COMMON_DEBUG_H

#include "handy.h"

__BEGIN_DECLS

#define LOG(str) /* TODO */
#define LOGF(arg, ...) /* TODO */

#define _NEW_LINE "\n"

#define LOGF_LN(str,...)			\
			LOG(str, __VA_ARGS__);	\
			LOG(_NEW_LINE)

#define LOG_LN(str)					\
		LOG(str);					\
		LOG(_NEW_LINE)

#define LOGF_FUNC 					\
		LOGF_LN("Inside ");			\
		LOGF_LN(__FUNCTION__)

__END_DECLS

#endif
