/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_COMMON_HANDY_H
#define BOX0_COMMON_HANDY_H

/* __BEGIN_DECLS should be used at the beginning of your declarations,
   so that C++ compilers don't mangle their names.  Use __END_DECLS at
   the end of C declarations. */
#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

#define ARRAY_LEN(arr) (sizeof(arr)/sizeof(arr[0]))

#define CAST_TO_UINT32_PTR(v)	((uint32_t *)(v))
#define UNUSED(v) 				((void)(v))

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) > (b)) ? (b) : (a))

#define __nop() __asm__("nop")

#define ALIGNED_4BYTE __attribute__ ((aligned(4)))
#define ALIGNED_2BYTE __attribute__ ((aligned(2)))

#define PACKED __attribute__((packed))

#define NVIC_PRIO_VERY_HIGH 0x00
#define NVIC_PRIO_HIGH 0x40
#define NVIC_PRIO_MEDIUM 0x80
#define NVIC_PRIO_LOW 0xC0

#define BIT2BYTE(bits) ((bits + 7) >> 3)

#define POW2(val) (1UL << (val))

#define INLINE_FUNC inline __attribute__((always_inline))

__END_DECLS

#endif
