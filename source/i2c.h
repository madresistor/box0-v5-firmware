/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_I2C_H
#define BOX0V5_I2C_H

#include "standard.h"
#include <stdint.h>
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>

__BEGIN_DECLS

#define B0_I2C_VERSION 101

#define B0_I2C_VERSION_GET 101
#define B0_I2C_VERSION_SET 102

/**
 * as per wikipedia
 * B0_I2C_VERSION_SM: standard mode (100 kbit/s)
 * B0_I2C_VERSION_FM: fast mode (400 kbit/s)
 * B0_I2C_VERSION_HS high speed mode (3.4 Mbit/s)
 * B0_I2C_VERSION_FMPLUS : fast mode plus (1 Mbit/s)
 * B0_I2C_VERSION_UFM: ultra fast mode (5 Mbit/s) - unidirectional
 */
#define B0_I2C_VERSION_SM 0
#define B0_I2C_VERSION_FM 1
#define B0_I2C_VERSION_HS 2
//#define B0_I2C_VERSION_HS_CLEANUP1 3
#define B0_I2C_VERSION_FMPLUS 4
#define B0_I2C_VERSION_UFM 5
//#define B0_I2C_VERSION_VER5 6
//#define B0_I2C_VERSION_VER6 7

#define DECLARE_B0_I2C_VERSION(length_of_array)						\
struct b0_i2c_version ##length_of_array {								\
	struct b0_property header;											\
	uint8_t bValues[length_of_array];									\
} PACKED

DECLARE_B0_I2C_VERSION(3);

#define FILL_B0_I2C_VERSION(type_size, ...) {							\
		.header = FILL_B0_PROPERTY(type_size, B0_I2C_VERSION),			\
		.bValues = {__VA_ARGS__},										\
	}

struct b0_i2c0 {
	struct b0_module header;
	/* iLabel[0] = SCL | iLabel[1] = SDA */
	struct b0_label2 label2;
	struct b0_ref ref;
	struct b0_i2c_version3 version3;
	struct b0_buffer buffer; /* only for transaction buffer value */
} PACKED;

#define B0_I2C_COMMAND_SIGNATURE 0xA4C60277
#define B0_I2C_STATUS_SIGNATURE 0x836B2A7C

/* bCommand */
#define B0_I2C_COMMAND_TRANSACTION 0

struct b0_i2c_command {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bCommand;
} PACKED;

/* bStatus */
#define B0_I2C_STATUS_SUCCESS 0 /* Success */
#define B0_I2C_STATUS_ERR_INVALID 1 /* Invalid Transaction */
#define B0_I2C_STATUS_ERR_UNDEFINED 2 /* Unknown error */
#define B0_I2C_STATUS_ERR_PREMATURE 3 /* Premature transfer */
#define B0_I2C_STATUS_ERR_ARBITRATION 4 /* Arbitration lost */
#define B0_I2C_STATUS_ERR_ELECTRICAL 5 /* Bus/Electrical problem */
#define B0_I2C_STATUS_PREMAT_HALT 6 /* Premature halt - STOP issue by Host */

struct b0_i2c_status {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bStatus;
} PACKED;

/* Transaction query wrapper */
struct b0_i2c_out {
	uint8_t bAddress; /* BIT0: 0 = Write, 1 = Read */
	uint8_t bVersion;
	uint8_t bReserved; /* Should be 0 */
	uint8_t bCount;
	uint8_t bData[0]; /* Unused for Write */
} PACKED;

struct b0_i2c_out_header {
	uint8_t bAddress;
	uint8_t bVersion;
	uint8_t bReserved;
	uint8_t bCount;
} PACKED;

/* Transaction result wrapper */
struct b0_i2c_in {
	uint8_t bACK;
	uint8_t bData[0]; /* Unused for Read */
} PACKED;

struct b0_i2c_in_header {
	uint8_t bACK;
} PACKED;

void i2c0_deinit(void);
void i2c0_init(void);
bool i2c0_setup_callback(usbd_device *dev,
			const struct usb_setup_data *setup_data);

__END_DECLS

#endif
