/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/stm32/timer.h>

#include <string.h>
#include "standard.h"
#include "common/handy.h"
#include "pwm.h"
#include "common/helper.h"
#include "config.h"
#include "usb.h"

struct b0_pwmX_data {
	uint32_t speed;
	uint32_t width;
	uint32_t period;
	uint8_t state;
};

struct b0_pwmX {
	uint32_t port;
	uint16_t pin;
	uint8_t pin_alt_fn;

	uint32_t tim;
	enum rcc_periph_clken tim_clock;
	enum tim_oc_id tim_oc;
	uint8_t tim_bitsize;
	uint8_t tim_bytesize;
	uint32_t tim_period_max;

	usbd_control_transfer_callback speed_set_callback;
	usbd_control_transfer_callback period_set_callback;
	usbd_control_transfer_callback width_set_callback;

	struct b0_pwmX_data *data;
};

static void pwmX_deinit(const struct b0_pwmX *pwmX);
static void pwmX_init(const struct b0_pwmX *pwmX);
bool pwm_setup_callback(const struct b0_pwmX *pwmX,
	usbd_device *dev, const struct usb_setup_data *setup_data);

static void pwmX_speed_set(const struct b0_pwmX *pwmX, uint32_t speed);
static void pwmX_period_set(const struct b0_pwmX *pwmX, uint32_t period);
static void pwmX_width_set(const struct b0_pwmX *pwmX, uint32_t width);
static void pwmX_stop(const struct b0_pwmX *pwmX);
static void pwmX_start(const struct b0_pwmX *pwmX);

static struct b0_pwmX_data pwmX_list_data[2];

static usbd_control_transfer_feedback pwm0_width_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);
static usbd_control_transfer_feedback pwm1_width_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);
static usbd_control_transfer_feedback pwm0_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);
static usbd_control_transfer_feedback pwm1_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);
static usbd_control_transfer_feedback pwm0_period_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);
static usbd_control_transfer_feedback pwm1_period_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg);

static const struct b0_pwmX pwmX_list[] = {{
	.port = GPIOB,
	.pin = GPIO11,
	.pin_alt_fn = GPIO_AF2,
	.tim = TIM2,
	.tim_oc = TIM_OC4,
	.tim_clock = RCC_TIM2,
	.tim_bitsize = 32,
	.tim_bytesize = 4,
	.tim_period_max = 0xFFFFFFFF,
	.data = &pwmX_list_data[0],
	.speed_set_callback = pwm0_speed_set_callback,
	.period_set_callback = pwm0_period_set_callback,
	.width_set_callback = pwm0_width_set_callback
}, {
	.port = GPIOA,
	.pin = GPIO8,
	.pin_alt_fn = GPIO_AF2,
	.tim = TIM1,
	.tim_oc = TIM_OC1,
	.tim_clock = RCC_TIM1,
	.tim_bitsize = 16,
	.tim_bytesize = 2,
	.tim_period_max = 0xFFFF,
	.data = &pwmX_list_data[1],
	.speed_set_callback = pwm1_speed_set_callback,
	.period_set_callback = pwm1_period_set_callback,
	.width_set_callback = pwm1_width_set_callback
}};

#define FEEDED_CLOCK B0_SPEED_MHZ(48)

void pwm0_deinit(void) { pwmX_deinit(&pwmX_list[0]); }
void pwm0_init(void) { pwmX_init(&pwmX_list[0]); }
bool pwm0_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data)
{ return pwm_setup_callback(&pwmX_list[0], dev, setup_data); }

void pwm1_deinit(void) { pwmX_deinit(&pwmX_list[1]); }
void pwm1_init(void) { pwmX_init(&pwmX_list[1]); }
bool pwm1_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data)
{ return pwm_setup_callback(&pwmX_list[1], dev, setup_data); }

static void pwmX_init(const struct b0_pwmX *pwmX)
{
	/* enable required peripherials clock */
	rcc_periph_clock_enable(pwmX->tim_clock);

	pwmX->data->state = B0_STATE_STOPPED;

	/* ==================== configure timer ================= */
	timer_disable_counter(pwmX->tim);
	timer_continuous_mode(pwmX->tim);
	timer_direction_up(pwmX->tim);
	timer_enable_break_main_output(pwmX->tim);

	timer_disable_oc_output(pwmX->tim, pwmX->tim_oc);
	timer_set_oc_mode(pwmX->tim, pwmX->tim_oc, TIM_OCM_PWM1);

	/* initalize prescaler to divide 48Mhz by 1 and preiod to maximum/2 */
	pwmX_speed_set(pwmX, B0_SPEED_MHZ(48));
	pwmX_period_set(pwmX, 0);
	pwmX_width_set(pwmX, 0);

	/* ================= configure pins =============== */
	gpio_mode_setup(pwmX->port, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, pwmX->pin);
	gpio_set_output_options(pwmX->port, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, pwmX->pin);
	gpio_set_af(pwmX->port, pwmX->pin_alt_fn, pwmX->pin);
}

static void pwmX_deinit(const struct b0_pwmX *pwmX)
{
	/* ============ deconfigure timer ================ */
	rcc_periph_clock_disable(pwmX->tim_clock);

	/* ============ deconfigure pins ================ */
	pins_protect(pwmX->port, pwmX->pin);
}

static usbd_control_transfer_feedback pwmX_width_set_callback(
		const struct b0_pwmX *pwmX, usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{
	uint32_t param = 0;
	memcpy(&param, arg->buffer, arg->length);

	/* NOTE: setting width 0 means disable the output */
	pwmX_width_set(pwmX, param);
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;
}

static usbd_control_transfer_feedback pwm0_width_set_callback(usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{ return pwmX_width_set_callback(&pwmX_list[0], dev, arg); }

static usbd_control_transfer_feedback pwm1_width_set_callback(usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{ return pwmX_width_set_callback(&pwmX_list[1], dev, arg); }

static usbd_control_transfer_feedback pwmX_period_set_callback(
		const struct b0_pwmX *pwmX, usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{
	uint32_t param = 0;
	memcpy(&param, arg->buffer, arg->length);

	/* NOTE: setting period 0 means disable the output */
	pwmX_period_set(pwmX, param);
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;
}

static usbd_control_transfer_feedback pwm0_period_set_callback(usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{ return pwmX_period_set_callback(&pwmX_list[0], dev, arg); }

static usbd_control_transfer_feedback pwm1_period_set_callback(usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{ return pwmX_period_set_callback(&pwmX_list[1], dev, arg); }

static usbd_control_transfer_feedback pwmX_speed_set_callback(
		const struct b0_pwmX *pwmX, usbd_device *dev,
		const usbd_control_transfer_callback_arg *arg)
{
	uint32_t param = CAST_TO_UINT32_PTR(arg->buffer)[0];

	ASSERT_STALL(param > 0 && param <= B0_SPEED_MHZ(48));

	/* NOTE: setting speed 0 means disable the output */
	pwmX_speed_set(pwmX, param);
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback pwm0_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{ return pwmX_speed_set_callback(&pwmX_list[0], dev, arg); }

static usbd_control_transfer_feedback pwm1_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{ return pwmX_speed_set_callback(&pwmX_list[1], dev, arg); }

bool pwm_setup_callback(const struct b0_pwmX *pwmX,
	usbd_device *dev, const struct usb_setup_data *setup_data)
{
	uint8_t *buf = usbd_control_buffer;

	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		switch (setup_data->bRequest) {
		case B0_MODE_GET:
			buf[0] = B0_MODE_OUTPUT;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &pwmX->data->state, 1, NULL);
		break;
		case B0_PWM_WIDTH_GET:
			ASSERT_STALL(B0_PWM_PIN_FROM_WVALUE(setup_data->wValue) == 0);
			usbd_ep0_transfer(dev, setup_data, &pwmX->data->width,
						pwmX->tim_bytesize, NULL);
		break;
		case B0_PWM_PERIOD_GET:
			usbd_ep0_transfer(dev, setup_data, &pwmX->data->period,
						pwmX->tim_bytesize, NULL);
		break;
		case B0_SPEED_GET:
			usbd_ep0_transfer(dev, setup_data, &pwmX->data->speed, 4, NULL);
		break;
		case B0_BITSIZE_GET:
			usbd_ep0_transfer(dev, setup_data, (void *) &pwmX->tim_bitsize,
					1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_STATE_SET:
			switch (B0_STATE_FROM_WVALUE(setup_data->wValue)) {
			case B0_STATE_START:
				ASSERT_STALL(pwmX->data->state == B0_STATE_STOPPED);
				pwmX_start(pwmX);
			break;
			case B0_STATE_STOP:
				ASSERT_STALL(pwmX->data->state == B0_STATE_RUNNING);
				pwmX_stop(pwmX);
			break;
			default:
			/* unknown state */
			goto stall;
			}

			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_MODE_SET:
			ASSERT_STALL(B0_MODE_FROM_WVALUE(setup_data->wValue) == B0_MODE_OUTPUT);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_BITSIZE_SET:
			ASSERT_STALL(B0_BITSIZE_FROM_WVALUE(setup_data->wValue)
								== pwmX->tim_bitsize);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case B0_PWM_WIDTH_SET:
			ASSERT_STALL(B0_PWM_PIN_FROM_WVALUE(setup_data->wValue) == 0);
			ASSERT_STALL(setup_data->wLength == pwmX->tim_bytesize);
			usbd_ep0_transfer(dev, setup_data, buf, pwmX->tim_bytesize,
							pwmX->width_set_callback);
		break;
		case B0_PWM_PERIOD_SET:
			ASSERT_STALL(setup_data->wLength == pwmX->tim_bytesize);
			usbd_ep0_transfer(dev, setup_data, buf, pwmX->tim_bytesize,
							pwmX->period_set_callback);
		break;
		case B0_SPEED_SET:
			ASSERT_STALL(setup_data->wLength == 4);
			ASSERT_STALL(pwmX->data->state == B0_STATE_STOPPED);
			usbd_ep0_transfer(dev, setup_data, buf, 4,
							pwmX->speed_set_callback);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

static void pwmX_start(const struct b0_pwmX *pwmX)
{
	pwmX->data->state = B0_STATE_RUNNING;

	//FIX: sometimes the pin remain high after halt is issued
	timer_enable_oc_output(pwmX->tim, pwmX->tim_oc);

	TIM_CNT(pwmX->tim) = 0;
	TIM_CR1(pwmX->tim) |= TIM_CR1_CEN;
}

static void pwmX_stop(const struct b0_pwmX *pwmX)
{
	pwmX->data->state = B0_STATE_STOPPED;

	//FIX: sometimes the pin remain high after halt is issued
	timer_disable_oc_output(pwmX->tim, pwmX->tim_oc);

	TIM_CR1(pwmX->tim) &= ~TIM_CR1_CEN;
}

static void pwmX_width_set(const struct b0_pwmX *pwmX, uint32_t width)
{
	pwmX->data->width = width;

	switch (pwmX->tim_oc) {
	case TIM_OC1:
		TIM_CCR1(pwmX->tim) = width;
	break;
	case TIM_OC2:
		TIM_CCR2(pwmX->tim) = width;
	break;
	case TIM_OC3:
		TIM_CCR3(pwmX->tim) = width;
	break;
	case TIM_OC4:
		TIM_CCR4(pwmX->tim) = width;
	break;
	default:
	break;
	}

	TIM_CNT(pwmX->tim) = 0;
}

static void pwmX_period_set(const struct b0_pwmX *pwmX, uint32_t period)
{
	TIM_ARR(pwmX->tim) = period;
	TIM_CNT(pwmX->tim) = 0;
	pwmX->data->period = period;
}

static void pwmX_speed_set(const struct b0_pwmX *pwmX, uint32_t speed)
{
	pwmX->data->speed = speed;
	TIM_PSC(pwmX->tim) = (FEEDED_CLOCK / speed) - 1;
	TIM_CNT(pwmX->tim) = 0;
}
