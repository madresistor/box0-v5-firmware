/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/adc.h>
#include <unicore-mx/cm3/systick.h>
#include <unicore-mx/cm3/assert.h>

#include <stdint.h>
#include <string.h>
#include "standard.h"
#include "ain.h"
#include "common/handy.h"
#include "common/helper.h"
#include "common/debug.h"
#include "config.h"
#include "usb.h"
#include "power.h"

void ain0_snapshot_stop(void);
void ain0_stream_stop(void);
void ain0_snapshot_start(uint32_t count);
void ain0_stream_start(void);
void ain0_chan_seq_refresh(void);
void ain0_speed_bitsize_refresh(void);

static struct {
	uint8_t state;
	uint8_t mode;

	struct {
		uint8_t count;
		uint8_t values[AIN0_CHAN_COUNT];
	} chan_seq;

	uint32_t speed;
	uint8_t bitsize;

	bool transfer_pending; /* stream only */

	union {
		uint16_t last_dma_chan_cnt; /* stream only */
		uint16_t count; /* snapshot only */
	};

	/* 16bit aligned buffer so that in case adc runned in 12bit resolution */
	uint8_t buffer[AIN0_BUFFER_SIZE] ALIGNED_2BYTE;
} ain0;

/**
 * Calibrate ADC
 * @brief
 *  Called on POR, this will calibrate the ADC
 */
void adc_calibrate(void)
{
	/* start HSI14, used by ADC */
	rcc_osc_on(RCC_HSI14);
	rcc_wait_for_osc_ready(RCC_HSI14);

	/* enable ADC clock */
	rcc_periph_clock_enable(RCC_ADC);

	/* clock using 12Mhz = (48Mhz / 4) */
	adc_set_clk_source(ADC1, ADC_CLKSOURCE_PCLK_DIV4);

	/* start the calibration procedure */
	adc_calibrate_start(ADC);
	adc_calibrate_wait_finish(ADC);

	/* power on the adc */
	adc_power_on(ADC);
}

static usbd_control_transfer_feedback ain0_chan_seq_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	uint8_t *buf = arg->buffer;
	unsigned i;

	for (i = 0; i < arg->length; i++) {
		/* Invalid channel value? */
		ASSERT_STALL(buf[i] < AIN0_CHAN_COUNT);

		/* test that the channel dont occur twice and
		 *  the sequence should be in increment order */
		if (i) {
			ASSERT_STALL(buf[i - 1] < buf[i]);
		}
	}

	memcpy(&ain0.chan_seq.values, arg->buffer, arg->length);
	ain0.chan_seq.count = arg->length;
	ain0_chan_seq_refresh();
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback ain0_state_set_snapshot_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	uint32_t count = CAST_TO_UINT32_PTR(arg->buffer)[0];
	unsigned bytes_per_sample = BIT2BYTE(ain0.bitsize);
	unsigned max_count = AIN0_BUFFER_SIZE / bytes_per_sample;

	ASSERT_STALL(count > 0 && count <= max_count);

	ain0_snapshot_start(count);
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback ain0_bitsize_speed_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *_arg)
{
	struct b0_bitsize_speed_arg *arg = _arg->buffer;

	/* Check for valid bitsize */
	ASSERT_STALL(arg->bBitsize == 12 || arg->bBitsize == 8);

	/* Check for valid speed */
	ASSERT_STALL(arg->dSpeed > 0);

	if (arg->dSpeed > AIN0_SNAPSHOT_MAX_SPEED) {
#if (AIN0_1MS_HACK == 1)
		ASSERT_STALL(arg->dSpeed == B0_SPEED_MHZ(1));
#else
		goto stall;
#endif
	}

	ain0.speed = arg->dSpeed;
	ain0.bitsize = arg->bBitsize;
	ain0_speed_bitsize_refresh();
	return USBD_CONTROL_TRANSFER_NO_STATUS_CALLBACK;

	stall:
	return USBD_CONTROL_TRANSFER_STALL;
}

static usbd_control_transfer_feedback ain0_mode_set_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	/* STOP any ongoing process if we are in running state */
	if (ain0.state == B0_STATE_RUNNING) {
		switch (ain0.mode) {
		case B0_MODE_SNAPSHOT:
			ain0_snapshot_stop();
		break;
		case B0_MODE_STREAM:
			ain0_stream_stop();
		break;
		}
	}

	/* Cancel any transfer pending */
	usbd_transfer_cancel_ep(dev_global, AIN0_EP_ADDR);

	/* Switch to the new mode */
	ain0.mode = usbd_control_buffer[0];

	switch (ain0.mode) {
	case B0_MODE_SNAPSHOT:
#if (AIN0_1MS_HACK == 1)
		ain0.speed = B0_SPEED_MHZ(1);
#else
		ain0.speed = AIN0_SNAPSHOT_MAX_SPEED;
#endif
	break;
	case B0_MODE_STREAM:
		ain0.speed = B0_SPEED_KHZ(100);
	break;
	}

	ain0.bitsize = 12;
	ain0_speed_bitsize_refresh();

	ain0.chan_seq.count = 1;
	ain0.chan_seq.values[0] = 0;
	ain0_chan_seq_refresh();

	return USBD_CONTROL_TRANSFER_OK;
}

bool ain0_setup_callback(usbd_device *dev,
			const struct usb_setup_data *setup_data)
{
	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		switch(setup_data->bRequest) {
		case B0_CHAN_SEQ_GET:
			usbd_ep0_transfer(dev, setup_data, ain0.chan_seq.values,
						ain0.chan_seq.count, NULL);
		break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &ain0.state, 1, NULL);
		break;
		case B0_MODE_GET:
			usbd_ep0_transfer(dev, setup_data, &ain0.mode, 1, NULL);
		break;
		case B0_BITSIZE_SPEED_GET: {
			struct b0_bitsize_speed_arg *arg = (void *) usbd_control_buffer;
			arg->bBitsize = ain0.bitsize;
			arg->dSpeed = ain0.speed;
			usbd_ep0_transfer(dev, setup_data, arg, sizeof(*arg), NULL);
		} break;
		default:
		goto stall;
		}
	} else {
		uint8_t *buf = usbd_control_buffer;
		switch(setup_data->bRequest) {
		case B0_CHAN_SEQ_SET:
			/* cannot set while AIN running */
			ASSERT_STALL(ain0.state == B0_STATE_STOPPED);

			/* Cannot set a empty channel sequence */
			ASSERT_STALL(setup_data->wLength > 0);

			/* Overflow */
			ASSERT_STALL(setup_data->wLength <= AIN0_CHAN_COUNT);

			usbd_ep0_transfer(dev, setup_data, buf, setup_data->wLength,
							ain0_chan_seq_set_callback);
		break;
		case B0_MODE_SET:
			buf[0] = B0_MODE_FROM_WVALUE(setup_data->wValue);
			ASSERT_STALL(buf[0] == B0_MODE_SNAPSHOT || buf[0] == B0_MODE_STREAM);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, ain0_mode_set_callback);
		break;
		case B0_STATE_SET:
			switch(B0_STATE_FROM_WVALUE(setup_data->wValue)) {
			case B0_STATE_START:
				/* Cannot be already in running state */
				ASSERT_STALL(ain0.state == B0_STATE_STOPPED);

				/* Start the appropriate mode */
				switch (ain0.mode) {
				case B0_MODE_SNAPSHOT:
					ASSERT_STALL(setup_data->wLength == 4);
					usbd_ep0_transfer(dev, setup_data, buf, 4,
									ain0_state_set_snapshot_callback);
				break;
				case B0_MODE_STREAM:
					ASSERT_STALL(setup_data->wLength == 0);
					usbd_ep0_transfer(dev, setup_data, NULL, 0,
						(usbd_control_transfer_callback) ain0_stream_start);
				break;
				default:
				goto stall;
				}
			break;
			case B0_STATE_STOP:
				/* already stopped? */
				ASSERT_STALL(ain0.state == B0_STATE_RUNNING);
				void *callback = (ain0.mode == B0_MODE_STREAM) ?
								ain0_stream_stop : ain0_snapshot_stop;
				usbd_ep0_transfer(dev, setup_data, NULL, 0, callback);
			break;
			default:
			/* unknown state */
			goto stall;
			}
		break;
		case B0_BITSIZE_SPEED_SET:
			/* cannot set while AIN running */
			ASSERT_STALL(ain0.state == B0_STATE_STOPPED);

			ASSERT_STALL(setup_data->wLength ==
						sizeof(struct b0_bitsize_speed_arg));

			usbd_ep0_transfer(dev, setup_data, buf,
					sizeof(struct b0_bitsize_speed_arg),
					ain0_bitsize_speed_set_callback);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

void ain0_init(void)
{
	/* enable power supply */
	power_state_set(POWER_ANALOG, POWER_ON);

	/* turn all required peripherials clock ON */
	rcc_periph_clock_enable(RCC_TIM15);

	/* AIN should be in STOPPED mode when SET_INTERFACE'd */
	ain0.state = B0_STATE_STOPPED;

	DMA1_CPAR1 = (uint32_t) &ADC1_DR;
	DMA1_CMAR1 = (uint32_t) ain0.buffer;

	nvic_set_priority(NVIC_DMA1_CHANNEL1_IRQ, NVIC_PRIO_LOW);
	nvic_enable_irq(NVIC_DMA1_CHANNEL1_IRQ);

	/* stop the ADC (precaution) so that configuration can be done */
	ADC1_CR |= ADC_CR_ADSTP;
	while (ADC1_CR & ADC_CR_ADSTP);

	/* For routing benifit, reverse mapping was done */
	ADC1_CFGR1 = ADC_CFGR1_SCANDIR | ADC_CFGR1_DMAEN;
	ADC1_IER = ADC_IER_OVRIE;

	/* ADC overrun interrupt */
	nvic_set_priority(NVIC_ADC_COMP_IRQ, NVIC_PRIO_MEDIUM);
	nvic_enable_irq(NVIC_ADC_COMP_IRQ);

	/* disable counter */
	TIM15_CR1 &= ~TIM_CR1_CEN;

	/* set TIM15 in master mode, TRGO is generate on UPDATE EVENT */
	TIM15_CR2 = (TIM15_CR2 & (~TIM_CR2_MMS_MASK)) | TIM_CR2_MMS_UPDATE;

	/* default: bitsize */
	ain0.bitsize = 12;

	/* Default: speed [snapshot mode only] */
#if (AIN0_1MS_HACK == 1)
	/* since 1MS is at top, it should be the default speed if HACK enabled */
	ain0.speed = B0_SPEED_MHZ(1);
#else
	ain0.speed = AIN0_SNAPSHOT_MAX_SPEED;
#endif
	ain0_speed_bitsize_refresh();

	/* default: channel-sequence: {0} */
	ain0.chan_seq.count = 1;
	ain0.chan_seq.values[0] = 0;
	ain0_chan_seq_refresh();

	/* Cancel any pending transfer */
	usbd_transfer_cancel_ep(dev_global, AIN0_EP_ADDR);

	/* Clear endpoint DTOG. */
	usbd_set_ep_dtog(dev_global, AIN0_EP_ADDR, false);
}

void ain0_speed_bitsize_refresh(void)
{
	static const struct {
		uint32_t adc_res;
		uint32_t dma_psize;
		uint32_t dma_msize;
		uint8_t bytes_per_sample;
	} cfg_12bit = {
		.adc_res = ADC_RESOLUTION_12BIT,
		.dma_psize = DMA_CCR_PSIZE_16BIT,
		.dma_msize = DMA_CCR_MSIZE_16BIT,
		.bytes_per_sample = 2
	}, cfg_8bit = {
		.adc_res = ADC_RESOLUTION_8BIT,
		.dma_psize = DMA_CCR_PSIZE_8BIT,
		.dma_msize = DMA_CCR_MSIZE_8BIT,
		.bytes_per_sample = 1
	}, *cfg;

	cfg = (ain0.bitsize > 8) ? &cfg_12bit : &cfg_8bit;

	DMA1_CCR1 = DMA_CCR_PL_VERY_HIGH | DMA_CCR_TEIE | DMA_CCR_MINC |
					cfg->dma_psize | cfg->dma_msize;

	adc_set_resolution(ADC1, cfg->adc_res);

	/* When ADC is run at its full pace (1MS).
	 *  It do not need trigger from timer.
	 *  Instead it is run in scan infinite mode.
	 */
	if (ain0.speed == B0_SPEED_MHZ(1)) {
		/* just convert with as much as throughput */
		adc_set_operation_mode(ADC1, ADC_MODE_SCAN_INFINITE);
		adc_disable_external_trigger_regular(ADC1);
	} else {
		/* Perform conversion on trigger from timer */
		adc_set_operation_mode(ADC1, ADC_MODE_SEQUENTIAL);
		adc_enable_external_trigger_regular(ADC1,
			ADC_CFGR1_EXTSEL_VAL(4), ADC_CFGR1_EXTEN_RISING_EDGE);
	}

	/*
	 * we need (sampling-time + convertion-time + latency) < (1 / requested-speed)
	 * in calculation 13 is used instead of 12.5 (and likewise 10 instead of 9.5) because
	 * so, to have nearest smaller value and to skip floating point calculation
	 * as well as latency = write-latency + trigger-latency
	 */
	uint32_t adc_clock = B0_SPEED_MHZ(12);
	uint32_t conversion_clocks = (ain0.bitsize == 12) ? 13 : 10;
	uint32_t latency = 5; /* in term of adc clock cycle (write + trigger) */
	uint32_t sampling_clocks = (adc_clock / ain0.speed) - conversion_clocks - latency;

	/* These values are the best nearest values that
	 * fit the clock requirements
	 */
	if (sampling_clocks > 239) {
		sampling_clocks = ADC_SMPTIME_239DOT5;
	} else if (sampling_clocks > 71) {
		sampling_clocks = ADC_SMPTIME_071DOT5;
	}  else if (sampling_clocks > 55) {
		sampling_clocks = ADC_SMPTIME_055DOT5;
	}  else if (sampling_clocks > 41) {
		sampling_clocks = ADC_SMPTIME_041DOT5;
	}  else if (sampling_clocks > 28) {
		sampling_clocks = ADC_SMPTIME_028DOT5;
	}  else if (sampling_clocks > 13) {
		sampling_clocks = ADC_SMPTIME_013DOT5;
	}  else if (sampling_clocks > 7) {
		sampling_clocks = ADC_SMPTIME_007DOT5;
	}  else {
		sampling_clocks = ADC_SMPTIME_001DOT5;
	}
	adc_set_sample_time_on_all_channels(ADC1, sampling_clocks);

	/* ============================== timer ========================= */
	/* easy configuration */
	uint16_t div;
	if (ain0.speed >= B0_SPEED_KHZ(1)) {
		div = 1;
	} else if (ain0.speed >= 100) {
		div = 8;
	} else if (ain0.speed >= 10) {
		div = 80;
	} else {
		div = 800;
	}

	TIM15_PSC = div - 1;
	TIM15_ARR = (B0_SPEED_MHZ(48) / (ain0.speed * div)) - 1;
}

void ain0_chan_seq_refresh(void)
{
	unsigned i;
	uint16_t mask = 0;

	for (i = 0; i < ain0.chan_seq.count; i++) {
		/* mapping: 3->CHSEL0 2->CHSEL1  1->CHSEL2  0->CHSEL3 */
		/* mapping: 3->PA0 2->PA1  1->PA2  0->PA3 */
		/* For routing benifit, the reverse mapping was done */
		mask |= 1 << (3 - ain0.chan_seq.values[i]);
	}

	/* Channel selection */
	ADC_CHSELR(ADC) = mask;

	/* make the pins in analog mode */
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, mask);

	/* make the others unused pins in HiZ mode */
	mask ^= GPIO0|GPIO1|GPIO2|GPIO3;
	pins_protect(GPIOA, mask);
}

void ain0_stream_start(void)
{
	usbd_transfer_cancel_ep(dev_global, AIN0_EP_ADDR);

	ain0.state = B0_STATE_RUNNING;
	ain0.transfer_pending = false;

	unsigned bits_per_sample = BIT2BYTE(ain0.bitsize);
	ain0.last_dma_chan_cnt = AIN0_BUFFER_SIZE / bits_per_sample;

	DMA1_CNDTR1 = ain0.last_dma_chan_cnt;
	DMA1_CCR1 = (DMA1_CCR1 & ~(DMA_CCR_TCIE)) | DMA_CCR_CIRC | DMA_CCR_EN;

	/* Circular mode */
	ADC1_CFGR1 |= ADC_CFGR1_DMACFG;

	/* start adc */
	ADC1_CR |= ADC_CR_ADSTART;
	while (!(ADC1_CR & ADC_CR_ADSTART));

	TIM15_CR1 |= TIM_CR1_CEN;
}

void ain0_stream_callback(usbd_device *dev,
	const usbd_transfer *transfer, usbd_transfer_status status,
	usbd_urb_id urb_id)
{
	ain0.transfer_pending = false;

	if (status == USBD_SUCCESS) {
		return;
	}

	if (status != USBD_ERR_CANCEL) {
		ain0_stream_stop();
	}
}

void ain0_dma_poll(void)
{
	if (ain0.state == B0_STATE_STOPPED) {
		/* Not running, ignore */
		return;
	}

	if (ain0.mode != B0_MODE_STREAM) {
		/* Not in streaming */
		return;
	}

	if (ain0.transfer_pending) {
		/* We cannot do anything because a transfer is already in progress */
		return;
	}

	/* We have some data to transfer? */
	uint16_t current_cnt = DMA1_CNDTR1;
	if (current_cnt == ain0.last_dma_chan_cnt) {
		/* Nop, no data! */
		return;
	}

	unsigned byte_per_sample = BIT2BYTE(ain0.bitsize);

	/* Calculate the number of samples capture */
	unsigned captured;
	if (current_cnt < ain0.last_dma_chan_cnt) {
		captured = ain0.last_dma_chan_cnt - current_cnt;
	} else {
		captured = ain0.last_dma_chan_cnt;
	}

	/* Assure that atleast one packet number of samples are available */
	unsigned samples_per_packet = AIN0_BULK_EP_SIZE / byte_per_sample;
	if (captured < samples_per_packet) {
		/* Nop, not enough! */
		return;
	}

	unsigned offset = AIN0_BUFFER_SIZE - (ain0.last_dma_chan_cnt * byte_per_sample);
	void *data = ain0.buffer + offset;

	unsigned send_samples = (captured / samples_per_packet) * samples_per_packet;

	ain0.last_dma_chan_cnt -= send_samples;
	if (!ain0.last_dma_chan_cnt) {
		/* Note: memory can only be used till end. (fake circular queue) */
		ain0.last_dma_chan_cnt = AIN0_BUFFER_SIZE / byte_per_sample;
	}

	/* Send the new data */
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = AIN0_EP_ADDR,
		.ep_size = AIN0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = data,
		.length = send_samples * byte_per_sample,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = ain0_stream_callback
	};

	if (usbd_transfer_submit(dev_global, &transfer) == USBD_INVALID_URB_ID) {
		/* Could not sumit transfer!!! */
		ain0_stream_stop();
		return;
	}

	ain0.transfer_pending = true;
}

/**
 * Start AIN0 in snapshot mode.
 * @param count Number of samples to capture
 */
void ain0_snapshot_start(uint32_t count)
{
	usbd_transfer_cancel_ep(dev_global, AIN0_EP_ADDR);

	ain0.state = B0_STATE_RUNNING;

	DMA1_IFCR = DMA_IFCR_CGIF1;

	DMA1_CNDTR1 = ain0.count = count;
	DMA1_CCR1 = (DMA1_CCR1 & ~(DMA_CCR_CIRC)) | DMA_CCR_TCIE | DMA_CCR_EN;

	/* Clear ADC flags */
	ADC1_ISR = ADC_ISR_AWD1 | ADC_ISR_OVR | ADC_ISR_EOSEQ | ADC_ISR_EOC |
				ADC_ISR_EOSMP | ADC_ISR_ADRDY;

	/* One shot mode */
	ADC1_CFGR1 &= ~ADC_CFGR1_DMACFG;

	/* start adc */
	ADC1_CR |= ADC_CR_ADSTART;
	while (!(ADC1_CR & ADC_CR_ADSTART));

	/* (AIN0_1MS_HACK == 1)
	 * if we are running at 1MS, dont enable timer.
	 * 1MS/S hack dont need trigger
	 */
	if (ain0.speed != B0_SPEED_MHZ(1)) {
		/* start timer */
		TIM15_CR1 |= TIM_CR1_CEN;
	}
}

void ain0_stream_stop(void)
{
	/* disable timer */
	TIM15_CR1 &= ~TIM_CR1_CEN;

	/* stop adc */
	ADC1_CR |= ADC_CR_ADSTP;
	while (ADC1_CR & ADC_CR_ADSTP);

	/* disable dma channel */
	DMA1_CCR1 &= ~DMA_CCR_EN;

	usbd_transfer_cancel_ep(dev_global, AIN0_EP_ADDR);

	/* mark as stopped */
	ain0.state = B0_STATE_STOPPED;
}

void ain0_snapshot_stop(void)
{
	/* disable timer */
	TIM15_CR1 &= ~TIM_CR1_CEN;

	/* stop adc */
	ADC1_CR |= ADC_CR_ADSTP;
	while (ADC1_CR & ADC_CR_ADSTP);

	/* disable dma channel */
	DMA1_CCR1 &= ~DMA_CCR_EN;

	/* Decrease the number of remaining samples */
	unsigned count = ain0.count - DMA1_CNDTR1;

	/* mark as stopped */
	ain0.state = B0_STATE_STOPPED;

	unsigned bytes_per_sample = BIT2BYTE(ain0.bitsize);

	usbd_transfer_flags flags = USBD_FLAG_NONE;
	if (count < ain0.count) {
		/* Always End with a short packet to tell host that no more data */
		flags = USBD_FLAG_SHORT_PACKET;
	}

	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = AIN0_EP_ADDR,
		.ep_size = AIN0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = ain0.buffer,
		.length = count * bytes_per_sample,
		.flags =  flags,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = NULL
	};

	usbd_transfer_submit(dev_global, &transfer);
}

/**
 * Interrupt handler for DMA Channel0{Transfer Complete, Transfer Error}
 */
void dma1_channel1_isr(void)
{
	/* Channel 0: Transfer Complete
	 * [snapshot mode only]
	 * the number of samples requested has been capture.
	 */
	if (DMA1_ISR & DMA_ISR_TCIF1) {
		ain0_snapshot_stop();
	}

	/* Channel0: Transfer Error */
	if (DMA1_ISR & DMA_ISR_TEIF1) {
		ain0_deinit();
	}

	/* Channel0: Clear all flags */
	DMA1_IFCR = DMA_IFCR_CGIF1;
}

/**
 * Interrupt: ADC Overrun
 */
void adc_comp_isr(void)
{
	/* ADC Overrun
	 * the DMA could not read the data on time */
	ain0_deinit();
}

/**
 * Deconfigure the AIN0 module.
 */
void ain0_deinit(void)
{
	LOG_LN("AIN0 deinit");

	/* move AIN0 to STOPPED state */
	ain0.state = B0_STATE_STOPPED;

	/* make all pins HiZ PA{0, 1, 2, 3} */
	pins_protect(GPIOA, GPIO0|GPIO1|GPIO2|GPIO3);

	/* stop adc */
	if (ADC1_CR & ADC_CR_ADSTART) {
		ADC1_CR |= ADC_CR_ADSTP;
		while (ADC1_CR & ADC_CR_ADSTP);
	}

	/* disable ADC interrupt */
	nvic_disable_irq(NVIC_ADC_COMP_IRQ);

	/* disable dma channel interrupt and dma channel */
	DMA1_CCR1 &= ~DMA_CCR_EN;
	nvic_disable_irq(NVIC_DMA1_CHANNEL1_IRQ);

	/* disable TIM15 clock since only this interface uses TIM15 */
	rcc_periph_clock_disable(RCC_TIM15);
}

/*
 * As per the document (specification datasheet)
 *  (STM32F072xx) (86/124) (DocID025004 Rev 2)
 * Section: Electrial Characterstics
 *
 * ADC can take trigger upto 823KHz when running from HSI14. (ie internal 14Mhz)
 *  It do not mention trigger rate for non-HSI14 clock (ex: HSE or HSI8)
 *
 * Empirically, it was found that for HSE (+ PLL) that ADC can take
 *  trigger upto approximately 650Khz.
 */
