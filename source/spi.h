/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_SPI_H
#define BOX0V5_SPI_H

#include "standard.h"
#include <stdint.h>
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>

__BEGIN_DECLS

/* Values */
#define B0_SPI_ACTIVE_STATE_LOW 0
#define B0_SPI_ACTIVE_STATE_HIGH 1

#define B0_SPI_VALUE_FROM_WVALUE(wValue) ((wValue) & 0xFF)
#define B0_SPI_PIN_FROM_WVALUE(wValue) ((wValue) >> 8)

/*
 * get active state value of slave select pin
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_SPI_ACTIVE_STATE_GET
 * wValue: HIGH8(<slave-select>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 1
 * data = <value>
 *
 * value = 0:LOW, 1:HIGH
 *
 * STALL if slave-select out of range
 *
 * by default SS is low
 */
#define B0_SPI_ACTIVE_STATE_GET 101

/*
 * set active state value of slave select pin
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_SPI_ACTIVE_STATE_SET
 * wValue: HIGH8(<pin-index>) LOW8(value)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 *
 * value: 0x0 = LOW, 0x1 = HIGH
 *
 * STALL if pin-index out of range
 */
#define B0_SPI_ACTIVE_STATE_SET 102

#define B0_SPI_CONFIG_MODE(i) 			((i & 0x3) << 0)
#define B0_SPI_CONFIG_CPHA 				(0x1 << 0)
#define B0_SPI_CONFIG_CPOL 				(0x1 << 1)
#define B0_SPI_CONFIG_FD		 		(0x0 << 2)
#define B0_SPI_CONFIG_HD_READ 			(0x3 << 2)
#define B0_SPI_CONFIG_HD_WRITE			(0x1 << 2)
#define B0_SPI_CONFIG_MSB_FIRST			(0x0 << 4)
#define B0_SPI_CONFIG_LSB_FIRST			(0x1 << 4)

#define B0_SPI_CONFIG_DUPLEX_HD_MASK	(0x01 << 2)
#define B0_SPI_CONFIG_DUPLEX_HD_READ_MASK	(0x01 << 3)

#define B0_SPI_COMMAND_SIGNATURE 0x9F7AB274
#define B0_SPI_STATUS_SIGNATURE  0xCAB93829

/* bCommand */
#define B0_SPI_COMMAND_TRANSACTION 0

struct b0_spi_command {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bCommand;
} PACKED;

/* bStatus */
#define B0_SPI_STATUS_SUCCESS 0
/* Non-zero values are error */
#define B0_SPI_STATUS_ERR_INVALID 1 /* Invalid Transaction */
#define B0_SPI_STATUS_ERR_UNDEFINED 2 /* Unknown error */
#define B0_SPI_STATUS_ERR_PREMATURE 3 /* Premature transfer */
#define B0_SPI_STATUS_ERR_ARBITRATION 4 /* Arbitration lost */
#define B0_SPI_STATUS_ERR_ELECTRICAL 5 /* Bus/Electrical problem */
#define B0_SPI_STATUS_PREMAT_HALT 6 /* Premature halt - STOP command by Host */

struct b0_spi_status {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bStatus;
} PACKED;

/* OUT Full duplex */
struct b0_spi_out_fd {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bCount;
	uint32_t dSpeed;
	uint8_t bData[0];
} PACKED;

/* OUT Half Duplex Read */
struct b0_spi_out_hd_read {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bRead;
	uint32_t dSpeed;
} PACKED;

/* OUT Half Duplex Write */
struct b0_spi_out_hd_write {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bWrite;
	uint32_t dSpeed;
	uint8_t bData[0];
} PACKED;

struct b0_spi_out_header {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bCount; /* bCount, bRead, bWrite */
	uint32_t dSpeed;
} PACKED;

union b0_spi_out {
	struct {
		uint8_t bSS;
		uint8_t bmConfig;
		uint8_t bBitsize;
		uint8_t bCount; /* bCount, bRead, bWrite */
		uint32_t dSpeed;
	} PACKED;

	union {
		struct b0_spi_out_hd_write write;
		struct b0_spi_out_hd_read read;
	} hd;
	struct b0_spi_out_fd fd;
} PACKED;

/* IN Full Duplex */
struct b0_spi_in_fd {
	uint8_t bData[0];
} PACKED;

/* IN Half Duplex Read */
struct b0_spi_in_hd_read {
	uint8_t bData[0];
} PACKED;

/* IN Half Duplex */
struct b0_spi_in_hd_write {
	uint8_t bWrite;
} PACKED;

union b0_spi_in {
	union {
		struct b0_spi_in_hd_write write;
		struct b0_spi_in_hd_read read;
	} hd;
	struct b0_spi_in_fd fd;
} PACKED;

/**
 * @param count number of slave select pins
 */
struct b0_spi0 {
	struct b0_module header;
	/* iLabel[0] = SCLK
	 * iLabel[1] = MOSI
	 * iLabel[2] = MISO
	 * iLabel[3 + i] = SS<i>
	 */
	struct b0_label7 label7;
	struct b0_buffer buffer;
	struct b0_ref ref;
	struct b0_speed_raw8 speed_raw8;
	struct b0_count count;
	struct b0_bitsize13 bitsize13;
} PACKED;

void spi0_init(void);
void spi0_deinit(void);
bool spi0_setup_callback(usbd_device *dev,
				const struct usb_setup_data *setup_data);

__END_DECLS

#endif
