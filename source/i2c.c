/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/syscfg.h>
#include <unicore-mx/stm32/i2c.h>
#include <unicore-mx/cm3/assert.h>

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "standard.h"
#include "usb.h"
#include "i2c.h"
#include "common/handy.h"
#include "common/helper.h"
#include "common/debug.h"
#include "config.h"
#include "power.h"

static int i2c_validate_transaction(uint8_t *buffer, unsigned len);
static void i2c0_status_submit(uint8_t status);
static void i2c0_exec_next(void);
static void i2c0_exec_stop(void);
static void i2c0_exec_done(uint8_t status);
static void i2c0_command_submit(void);

static struct {
	struct b0_i2c_command command;
	struct b0_i2c_status status;

	uint8_t state;
	uint8_t version;

	/* I2C buffer OUT */
	struct {
		/* keep track of total length of transaction */
		uint8_t len;

		/* keep track of the ongoing transaction (offset) */
		uint8_t processed;

		/* memory to keep transaction */
		uint8_t buffer[I2C0_BUFFER_SIZE] ALIGNED_2BYTE;
	} out;

	/* I2C buffer IN */
	struct {
		/* keep track of onging transaction result and valid length of
		 *  result of transaction (offset as well as length) */
		uint8_t len;

		/* buffer to result transaction result */
		uint8_t buffer[I2C0_BUFFER_SIZE] ALIGNED_2BYTE;
	} in;

} i2c0;

#define _I2C_TIMINGR_VALUE(PREC, SCLL, SCLH, SDADEL, SCLDEL) \
	((PREC << 28) | (SCLDEL << 20) | (SDADEL << 16) | (SCLH << 8) | (SCLL << 0))

static const uint32_t i2c0_version_timingr[] = {
	/* clock: 48MHz, generate: 100khz
	 * PREC = 0xB
	 * SCLL = 0x13
	 * SCLH = 0xF
	 * SDADEL = 0x2
	 * SCLDEL = 0x4 */
	[B0_I2C_VERSION_SM] = _I2C_TIMINGR_VALUE(0xB, 0x13, 0xF, 0x2, 0x4),

	/* clock: 48MHz, generate: 400khz
	 * PREC = 0x5
	 * SCLL = 0x9
	 * SCLH = 0x3
	 * SDADEL = 0x3
	 * SCLDEL = 0x3 */
	[B0_I2C_VERSION_FM] = _I2C_TIMINGR_VALUE(0x5, 0x9, 0x3, 0x3, 0x3),

	/* clock: 48MHz, generate: 1000khz
	 * PREC = 0x5
	 * SCLL = 0x3
	 * SCLH = 0x1
	 * SDADEL = 0x0
	 * SCLDEL = 0x1 */
	[B0_I2C_VERSION_FMPLUS] = _I2C_TIMINGR_VALUE(0x5, 0x3, 0x1, 0x0, 0x1)
};

static usbd_control_transfer_feedback i2c0_mode_set_master_callback(
		usbd_device *dev, const usbd_control_transfer_callback_arg *arg)
{
	if (i2c0.state == B0_STATE_RUNNING) {
		i2c0_exec_stop();
	}

	/* Cancel any pending transfer */
	usbd_transfer_cancel_ep(dev, I2C0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev, I2C0_EP_ADDR_OUT);

	/* Clear any previous stalled endpoint */
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_IN, false);
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_OUT, false);

	/* Accept command */
	i2c0_command_submit();

	return USBD_CONTROL_TRANSFER_OK;
}

bool i2c0_setup_callback(usbd_device *dev,
			const struct usb_setup_data *setup_data)
{
	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		uint8_t *buf = usbd_control_buffer;
		switch (setup_data->bRequest) {
		case B0_MODE_GET:
			buf[0] = B0_MODE_MASTER;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		break;
		case B0_STATE_GET:
			usbd_ep0_transfer(dev, setup_data, &i2c0.state, 1, NULL);
		break;
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_MODE_SET:
			ASSERT_STALL(B0_MODE_FROM_WVALUE(setup_data->wValue) == B0_MODE_MASTER);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, i2c0_mode_set_master_callback);
		break;
		case B0_STATE_SET:
			/* Host can only issue STOP command and should be in running state */
			ASSERT_STALL(B0_STATE_FROM_WVALUE(setup_data->wValue) == B0_STATE_STOP);
			ASSERT_STALL(i2c0.state == B0_STATE_RUNNING);
			i2c0_exec_done(B0_I2C_STATUS_PREMAT_HALT);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

void i2c0_init(void)
{
	/* enable power supply */
	power_state_set(POWER_DIGITAL, POWER_ON);

	/* enable all peripherial clock required */
	rcc_periph_clock_enable(RCC_I2C1);

	/* move to STOPPED state */
	i2c0.state = B0_STATE_STOPPED;

	/* ============== configure pins ================ */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO8 | GPIO9);
	gpio_set_af(GPIOB, GPIO_AF1, GPIO8 | GPIO9);

	/* ================= configure syscfg ================ */
	/* map DMA channel6, channel7 to I2C1 */
	SYSCFG_CFGR1 |= SYSCFG_CFGR1_I2C1_DMA_RMP;

	/* ===== configure DMA Channel6 (used for transmission) ======= */
	/* - Disable channel
	 * - lower priority
	 * - all transmission is 1byte oriented
	 * - read from memory (in increment), copy to I2C TX data register */
	DMA1_CCR6 = DMA_CCR_MINC | DMA_CCR_DIR;
	DMA1_CPAR6 = (uint32_t) &I2C1_TXDR;

	/* ===== configure DMA Channel7 (used for reception) ======= */
	/* - Disable channel
	 * - lower priority
	 * - all reception is 1byte oriented
	 * - copy from I2C reception to memory (incement) */
	DMA1_CCR7 = DMA_CCR_MINC;
	DMA1_CPAR7 = (uint32_t) &I2C1_RXDR;

	/* ============================ configure I2C =================== */
	/* feed 48Mhz to I2C1 (SYSCLK = 48Mhz) */
	RCC_CFGR3 |= RCC_CFGR3_I2C1SW;

	/* disable I2C peripherial (if already running) and wait till it not disable */
	I2C1_CR1 &= ~I2C_CR1_PE;
	while (I2C1_CR1 & I2C_CR1_PE);

	/* enable DMA TX, DMA RX, and interrupts (ERR, STOP, NACK, TC)  */
	I2C1_CR1 = I2C_CR1_RXDMAEN | I2C_CR1_TXDMAEN | I2C_CR1_ERRIE |
				I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_TCIE;

	/* for safty, make them 0 */
	I2C1_TIMEOUTR = 0;
	I2C1_OAR1 = 0;
	I2C1_OAR2 = 0;

	/* set I2C in standard mode */
	i2c0.version = B0_I2C_VERSION_SM;
	I2C1_TIMINGR = i2c0_version_timingr[i2c0.version];

	/* enable I2C peripherial */
	I2C1_CR1 |= I2C_CR1_PE;

	/* enable I2C interript with high priority */
	nvic_set_priority(NVIC_I2C1_IRQ, NVIC_PRIO_HIGH);
	nvic_enable_irq(NVIC_I2C1_IRQ);

	/*============== endpoint =========== */
	/*if dtog bits are not cleared, device will have
	 * problem receiving data on dtog mismatch */
	usbd_set_ep_dtog(dev_global, I2C0_EP_ADDR_IN, false);
	usbd_set_ep_dtog(dev_global, I2C0_EP_ADDR_OUT, false);

	/* Clear any previous stalled endpoint */
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_IN, false);
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_OUT, false);

	/* Cancel any pending transfer */
	usbd_transfer_cancel_ep(dev_global, I2C0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev_global, I2C0_EP_ADDR_OUT);
}

void i2c0_deinit(void)
{
	usbd_transfer_cancel_ep(dev_global, I2C0_EP_ADDR_IN);
	usbd_transfer_cancel_ep(dev_global, I2C0_EP_ADDR_OUT);

	/* enable common peripherial clocks */
	rcc_periph_clock_enable(RCC_I2C1);

	/* ============ deconfigure pins ============= */
	/* make pins HiZ */
	pins_protect(GPIOB, GPIO8 | GPIO9);

	/* ============ deconfigure dma channels ============= */
	/* disable DMA channels */
	DMA1_CCR6 &= ~DMA_CCR_EN;
	DMA1_CCR7 &= ~DMA_CCR_EN;

	/* ============ deconfigure i2c ============= */
	i2c0.state = B0_STATE_STOPPED;

	/* disable peripherial (this will stop any ongoing transaction) */
	I2C1_CR1 &= ~I2C_CR1_PE;
	while (I2C1_CR1 & I2C_CR1_PE);

	/* disable I2C clock */
	rcc_periph_clock_disable(RCC_I2C1);

	/* disable IRQ */
	nvic_disable_irq(NVIC_I2C1_IRQ);
}

void i2c1_isr(void)
{
	/* read flags */
	uint32_t isr = I2C1_ISR;

	/* clear flags */
	I2C1_ICR = isr & (I2C_ICR_TIMEOUTCF | I2C_ICR_NACKCF | I2C_ICR_STOPCF |
						I2C_ICR_ARLOCF | I2C_ICR_OVRCF | I2C_ICR_BERRCF);

	/* get the transaction that was performed */
	struct b0_i2c_out *out = (struct b0_i2c_out *) &i2c0.out.buffer[i2c0.out.processed];

	/* result to be stored here */
	struct b0_i2c_in *in = (struct b0_i2c_in *) &i2c0.in.buffer[i2c0.in.len];

	/* is their any residue data left (as per DMA) */
	uint8_t residue_len = (out->bAddress & 0x01) ? DMA1_CNDTR7 : DMA1_CNDTR6;

	/* which type of transaction was peformed? */
	if (out->bAddress & 0x01) { /* read was peformed */
		/* update the number of ACK received */
		in->bACK = out->bCount - residue_len;

		/* +1 is for including bACK,
		 * (out->bCount - residue_len) = actual data received */
		i2c0.in.len += 1 + out->bCount - residue_len;
	} else { /* write was performed */

		in->bACK = out->bCount - residue_len;

		/* Also count the address write ACK */
		if (!(isr & I2C_ISR_NACKF)) {
			in->bACK++;
		}

		/* +1 is for bACK offset */
		i2c0.in.len += 1;
	}

	if (residue_len) {
		i2c0_exec_done(B0_I2C_STATUS_ERR_PREMATURE);
		return;
	} else if (isr & I2C_ISR_ARLO) {
		i2c0_exec_done(B0_I2C_STATUS_ERR_ARBITRATION);
		return;
	} else if (isr & I2C_ISR_BERR) {
		i2c0_exec_done(B0_I2C_STATUS_ERR_ELECTRICAL);
		return;
	} else if (isr & (I2C_ISR_OVR | I2C_ISR_TIMEOUT)) {
		i2c0_exec_done(B0_I2C_STATUS_ERR_UNDEFINED);
		return;
	} else if (isr & (I2C_ISR_NACKF | I2C_ISR_STOPF)) {
		i2c0_exec_done(B0_I2C_STATUS_SUCCESS);
		return;
	}

	/* increment the pointer to next sub-transaction */
	i2c0.out.processed += sizeof(struct b0_i2c_out_header);
	if ((out->bAddress & 0x01) == 0) {
		i2c0.out.processed += out->bCount;
	}

	/* till now everything goes fine, perform the
	 * next sub-transaction (read or write) */
	if (isr & I2C_ISR_TC) {
		i2c0_exec_next();
		return;
	}

	/* we should not reach here */
	cm3_assert_not_reached();
}

static void i2c0_exec_stop(void)
{
	/* disable DMA channels */
	DMA1_CCR6 &= ~DMA_CCR_EN;
	DMA1_CCR7 &= ~DMA_CCR_EN;

	/* disable peripherial (this will stop any ongoing transaction) */
	I2C1_CR1 &= ~I2C_CR1_PE;
	while (I2C1_CR1 & I2C_CR1_PE);

	/* mark transaction as complete */
	i2c0.state = B0_STATE_STOPPED;
}

static void i2c0_exec_done(uint8_t status)
{
	i2c0_exec_stop();
	i2c0_status_submit(status);
}

//Note: not using 10bit address support by peripherial
//Note: not using RELOAD facility by peripherial

//TODO: remove buffer overflow testing from code, as it is no more required
// checking is now done by i2c0_calculate_transaction_result_size()
static void i2c0_exec_next(void)
{
	uint8_t len_test;
	uint32_t cr2 = 0;

	/* clear DMA flags and disable them */
	DMA1_IFCR = DMA_IFCR_CGIF7 | DMA_IFCR_CGIF6;
	DMA1_CCR6 &= ~DMA_CCR_EN;
	DMA1_CCR7 &= ~DMA_CCR_EN;

	struct b0_i2c_out *out = (struct b0_i2c_out *) &i2c0.out.buffer[i2c0.out.processed];
	struct b0_i2c_in *in = (struct b0_i2c_in *) &i2c0.in.buffer[i2c0.in.len];

	/* Slave address */
	cr2 = out->bAddress;

	/* number of byte to be readed/writen on bus */
	cr2 |= I2C_CR2_NBYTES_VAL(out->bCount);

	if (out->bAddress & 0x01) {
		/* we need to Read */
		cr2 |= I2C_CR2_RD_WRN;
		/* use IN buffer as buffer to store data from bus */
		DMA1_CNDTR7 = out->bCount;
		DMA1_CMAR7 = (uint32_t) in->bData;
		DMA1_CCR7 |= DMA_CCR_EN;
	} else {
		/* we need to write */
		DMA1_CNDTR6 = out->bCount;
		DMA1_CMAR6 = (uint32_t) out->bData;
		DMA1_CCR6 |= DMA_CCR_EN;
	}

	/* test if this is the last packet, then send a STOP automatically { */
	len_test = i2c0.out.processed + sizeof(struct b0_i2c_out_header);
	if ((out->bAddress & 0x01) == 0) {
		len_test += out->bCount;
	}

	/* next sub-transaction is out of buffer? (dry test) */
	if (len_test >= i2c0.out.len) {
		cr2 |= I2C_CR2_AUTOEND;
	}
	/* } */

	/* send a start (or restart) on bus */
	cr2 |= I2C_CR2_START;
	I2C1_CR2 = cr2;
}

#if (I2C0_TRANSACTION_VALIDATION_ENABLE == 1)
static int i2c_validate_transaction(uint8_t *buffer, unsigned len)
{
	unsigned out_processed = 0;
	unsigned in_consumed = 0;
	struct b0_i2c_out *out;

	while (out_processed < len) {
		out = (struct b0_i2c_out *) &buffer[out_processed];

		if (out->bVersion != B0_I2C_VERSION_SM &&
			out->bVersion != B0_I2C_VERSION_FM &&
			out->bVersion != B0_I2C_VERSION_FMPLUS) {
			/* I2C Version change is not supported by periph in realtime
			 *  but the specification support it! */
			return -1;
		}

		out_processed += sizeof(struct b0_i2c_out_header);
		in_consumed += sizeof(struct b0_i2c_in_header);

		if (out->bAddress & 0x01) {
			/* read nbytes from slaves require memory */
			in_consumed += out->bCount;
		} else {
			/* write has data embedded in it */
			out_processed += out->bCount;
		}
	}

	if (out_processed > len) {
		/* corrupt transaction */
		return -1;
	} else {
		return in_consumed;
	}
}
#endif

static void i2c0_problem_detected(void)
{
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_IN, true);
	usbd_set_ep_stall(dev_global, I2C0_EP_ADDR_OUT, true);
}

static void i2c0_in_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = I2C0_EP_ADDR_IN,
		.ep_size = I2C0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = i2c0.in.buffer,
		.length = i2c0.status.dLength,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = NULL
	};

	usbd_transfer_submit(dev_global, &transfer);
}

static void i2c0_exec_start(void)
{
	i2c0.out.len = i2c0.command.dLength;
	i2c0.out.processed = 0;
	i2c0.state = B0_STATE_RUNNING;
	i2c0.in.len = 0;

#if (I2C0_TRANSACTION_VALIDATION_ENABLE == 1)
	int r = i2c_validate_transaction(i2c0.out.buffer, i2c0.out.len);
	if (r < 0 || r > I2C0_BUFFER_SIZE) {
		/* Transaction will overflow */
		LOG_LN("I2C0 Transaction invalid");
		i2c0_exec_done(B0_I2C_STATUS_ERR_INVALID);
		return;
	}
#endif

	/* ***WARNING***: Specification isn't properly implemented.
	 * Actually the version should be changed on fly but the hardware
	 * do not support the feature.
	 * The first sub-transaction version is used for the full transaction. */
	struct b0_i2c_out *out = (void *) i2c0.out.buffer;
	if (out->bVersion != i2c0.version) {
		i2c0.version = out->bVersion;
		I2C1_TIMINGR = i2c0_version_timingr[i2c0.version];
	}

	/* try to reset peripherial (this will stop any ongoing transaction) */
	I2C1_CR1 &= ~I2C_CR1_PE;
	while (I2C1_CR1 & I2C_CR1_PE);
	I2C1_CR1 |= I2C_CR1_PE;

	/* start the first sub-transaction */
	i2c0_exec_next();
}

static void i2c0_status_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	UNUSED(transfer);
	UNUSED(urb_id);

	if (status != USBD_SUCCESS) {
		return;
	}

	if (i2c0.status.dLength) {
		i2c0_in_submit();
	}

	i2c0_command_submit();
}

static void i2c0_status_submit(uint8_t status)
{
	i2c0.status.dSignature = B0_I2C_STATUS_SIGNATURE;
	i2c0.status.dTag = i2c0.command.dTag;
	i2c0.status.dLength = i2c0.in.len;
	i2c0.status.bStatus = status;

	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = I2C0_EP_ADDR_IN,
		.ep_size = I2C0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = &i2c0.status,
		.length = sizeof(i2c0.status),
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = i2c0_status_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}

static void i2c0_out_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	UNUSED(urb_id);

	if (status != USBD_SUCCESS) {
		LOG_LN("I2C0 OUT callback reported failure");
		return;
	}

	i2c0_exec_start();
}

void i2c0_out_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = I2C0_EP_ADDR_OUT,
		.ep_size = I2C0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = i2c0.out.buffer,
		.length = i2c0.command.dLength,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = i2c0_out_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}

/**
 * COMMAND callback
 */
void i2c0_command_callback(usbd_device *dev, const usbd_transfer *transfer,
		usbd_transfer_status status, usbd_urb_id urb_id)
{
	UNUSED(transfer);
	UNUSED(urb_id);

	usbd_transfer_cancel_ep(dev, I2C0_EP_ADDR_IN);

	if (status != USBD_SUCCESS) {
		/* Problem receving command packet */
		return;
	}

	/* Verify the signature */
	if (i2c0.command.dSignature != B0_I2C_COMMAND_SIGNATURE) {
		LOG_LN("I2C0 COMMAND signature match failed");
		goto problem;
	}

	/* Verify the reserved field */
	if (i2c0.command.bCommand != B0_I2C_COMMAND_TRANSACTION) {
		LOG_LN("I2C0 COMMAND reserved field not zero");
		goto problem;
	}

	/* Verify the OUT length */
	if (!i2c0.command.dLength || i2c0.command.dLength > I2C0_BUFFER_SIZE) {
		LOG_LN("I2C0 COMMAND received dLength will cause overflow");
		goto problem;
	}

	i2c0_out_submit();
	return;

	problem:
	i2c0_problem_detected();
}

/**
 * Submit command transfer
 */
void i2c0_command_submit(void)
{
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_BULK,
		.ep_addr = I2C0_EP_ADDR_OUT,
		.ep_size = I2C0_BULK_EP_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = &i2c0.command,
		.length = sizeof(i2c0.command),
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = i2c0_command_callback
	};

	usbd_transfer_submit(dev_global, &transfer);
}
