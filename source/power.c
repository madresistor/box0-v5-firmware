/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power.h"
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/exti.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/cm3/nvic.h>
#include "common/handy.h"
#include "config.h"

/**
 * This is used to keep dynamic data for RT9728.
 */
struct rt9728_data {
	/**
	 * Store the variant.
	 *  true: Active low type
	 *  false: Active high type
	 */
	bool active_low;

	/**
	 * Power state
	 */
	enum power_state state;
} rt9278_analog_data, rt9728_digital_data;

/**
 * RT9728 LED and timer information.
 *   This has the information of the GPIO and TIMER that is used to control LED
 * This is indented for read only.
 */
struct rt9728_led {
	uint32_t gpio;
	uint16_t pin;
	uint8_t pin_af;
	uint32_t tim;
	enum tim_oc_id tim_oc;
	enum rcc_periph_clken rcc_tim;
};

/**
 * RT9728 board information.
 * This will contain the necessary detail to control the RT9728 on PCB.
 */
struct rt9728 {
	struct {
		uint32_t gpio;
		uint16_t pin;
	} en, fault;

	struct {
		uint32_t ch;
		uint8_t nvic;
	} exti;

	struct rt9728_led led;

	struct rt9728_data *data;
};

/**
 * Analog RT9728
 */
const struct rt9728 power_analog = {
	.en = {GPIOC, GPIO5},
	.fault = {GPIOC, GPIO4},
	.exti = {EXTI4, NVIC_EXTI4_15_IRQ},

	.led = {
		.gpio = GPIOA,
		.pin = GPIO6,
		.pin_af = GPIO_AF5,
		.tim = TIM16,
		.tim_oc = TIM_OC1,
		.rcc_tim = RCC_TIM16
	},

	.data = &rt9278_analog_data
};

/**
 * Digital RT9728
 */
const struct rt9728 power_digital = {
	.en = {GPIOC, GPIO14},
	.fault = {GPIOC, GPIO15},
	.exti = {EXTI15, NVIC_EXTI4_15_IRQ},

	.led = {
		.gpio = GPIOC,
		.pin = GPIO9,
		.pin_af = GPIO_AF0,
		.tim = TIM3,
		.tim_oc = TIM_OC4,
		.rcc_tim = RCC_TIM3
	},

	.data = &rt9728_digital_data
};

static void _init_led(const struct rt9728_led *led)
{
	rcc_periph_clock_enable(led->rcc_tim);

	gpio_mode_setup(led->gpio, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, led->pin);
	gpio_set_output_options(led->gpio, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, led->pin);
	gpio_set_af(led->gpio, led->pin_af, led->pin);

	/* initalize prescaler to divide 48Mhz by 48000.
	 * resullt: precaler output 1Khz */
	timer_set_prescaler(led->tim, 48000 - 1);

	timer_continuous_mode(led->tim);
	timer_direction_up(led->tim);

	/* TIM16 only.
	 * Why? TIM16 not an advance timer but require MOE bit set to output pwm */
	TIM_BDTR(led->tim) = TIM_BDTR_MOE;

	/* turned off (on reset) */
	timer_set_period(led->tim, 0);
	timer_set_oc_mode(led->tim, led->tim_oc, TIM_OCM_PWM1);
	timer_set_oc_value(led->tim, led->tim_oc, 0);
	timer_enable_oc_output(led->tim, led->tim_oc);

	timer_enable_counter(led->tim);
}

/**
 * @brief initalize the analog power supply of the board
 * @details
 *  initalization is done in the following sequence.
 *  - enable the required peripherial clock
 *  - make the EN pin into input mode and identify the variant of RT9728A
 *  - configure the EN pin into output and control the RT9728A.
 *  - initalize the FAULT output of RT9728A.
 *     configure uC EXTI to generate interrupt when FAULT goes high->low
 *  - Enable the EXTI IRQ
 *
 * @note the board have a pull-up or pull-down resistance that
 *    by default make the RT9728A in inactive state while
 *     the uC is in reset state.
 *    the pull-* resistance can also be used to know the variant of RT9728A
 *
 * @note their is no external pull on FAULT (FAULT is active low on both variant)
 *     and internal pull of the uC is used
 */
void power_init(const struct rt9728 *ic)
{
	/* determine the type of IC (active high or active low) */
	gpio_mode_setup(ic->en.gpio, GPIO_MODE_INPUT, GPIO_PUPD_NONE, ic->en.pin);
	ic->data->active_low = (gpio_get(ic->en.gpio, ic->en.pin) != 0x00);

	/* enable pin */
	gpio_mode_setup(ic->en.gpio, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ic->en.pin);
	gpio_set_output_options(ic->en.gpio, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, ic->en.pin);

	/* Disable the IC by default */
	if (ic->data->active_low) {
		gpio_set(ic->en.gpio, ic->en.pin);
	} else {
		gpio_clear(ic->en.gpio, ic->en.pin);
	}

	/* fault interrupt */
	gpio_mode_setup(ic->fault.gpio, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, ic->fault.pin);
	exti_select_source(ic->exti.ch, ic->fault.gpio);
	exti_set_trigger(ic->exti.ch, EXTI_TRIGGER_FALLING);
	exti_enable_request(ic->exti.ch);

	/* Enable EXTI interrupt */
	nvic_set_priority(ic->exti.nvic, NVIC_PRIO_LOW);
	nvic_enable_irq(ic->exti.nvic);

	/* LED */
	_init_led(&ic->led);
}

/**
 * Set the power LED State
 * @param[in] led LED data
 * @param[in] state LED State {POWER_OFF, POWER_ON, POWER_OC}
 * @see led_power_init()
 */
static void set_led_state(const struct rt9728_led *led, enum power_state state)
{
	struct {
		uint16_t arr;
		uint16_t oc;
	} val[] =  {
		[POWER_OFF] = {0, 0}, /* OFF */
		[POWER_ON] = {3, 1}, /* ON with 1Khz, 25% duty cycle */
		[POWER_OC] = {250, 125} /* TOGGLE with 4Hz, 50% duty cycle */
	};

	timer_set_period(led->tim, val[state].arr);
	timer_set_oc_value(led->tim, led->tim_oc, val[state].oc);
	TIM_CNT(led->tim) = 0;
}

void fault_check_up(const struct rt9728 *ic)
{
	if (!(EXTI_PR & ic->exti.ch)) {
		return;
	}

	if (!gpio_get(ic->fault.gpio, ic->fault.pin)) {
		/* FIXME: since this interrupt is of low priority and in between a usb
		 *  control transfer (usb interrupt is of higher priority) comes, then
		 *  the behaviour is undefined. */
		power_state_set(ic, POWER_OC);
	}

	exti_reset_request(ic->exti.ch);
}

void exti4_15_isr(void)
{
	fault_check_up(POWER_ANALOG);
	fault_check_up(POWER_DIGITAL);
}

enum power_state power_state_get(const struct rt9728 *ic)
{
	return ic->data->state;
}

void power_state_set(const struct rt9728 *ic, enum power_state state)
{
	bool pin_value;

	ic->data->state = state;

	/* set led state */
	set_led_state(&ic->led, state);

	/* flip to actual pin value */
	pin_value = (state == POWER_ON);
	if (ic->data->active_low) {
		pin_value = !pin_value;
	}

	/* set pin value */
	if (pin_value) {
		gpio_set(ic->en.gpio, ic->en.pin);
	} else {
		gpio_clear(ic->en.gpio, ic->en.pin);
	}
}
