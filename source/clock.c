/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/crs.h>
#include <unicore-mx/stm32/flash.h>
#include <unicore-mx/stm32/gpio.h>

#include "clock.h"
#include "config.h"

/*
 * Initalize clock.
 * as per, EXTERNAL_CRYSTAL, clock is initalized
 *
 * Option 1 (EXTERNAL_CRYSTAL == 0):
 *  Use Internal (HSI48) 48Mhz RC
 *
 * Option 2 (EXTERNAL_CRYSTAL == 8):
 *  Use External 8Mhz Crystal Oscillator
 *  Use PLL to generate 48Mhz clock
 *
 * Option 2 (EXTERNAL_CRYSTAL == 16):
 *  Use External 16Mhz Crystal Oscillator
 *  Use PLL to generate 48Mhz clock
 */

#if (EXTERNAL_CRYSTAL > 0)
static void hse_out_48mhz(uint32_t pll_mult_factor)
{
	rcc_osc_on(RCC_HSE);
	rcc_wait_for_osc_ready(RCC_HSE);

	rcc_set_hpre(RCC_CFGR_HPRE_NODIV);
	rcc_set_ppre(RCC_CFGR_PPRE_NODIV);

	flash_set_ws(FLASH_ACR_LATENCY_024_048MHZ);

	/* external-crystal * pll_mult_factor / 1 = 48MHz (expected) */
	rcc_set_prediv(RCC_CFGR2_PREDIV_NODIV);
	rcc_set_pll_multiplication_factor(pll_mult_factor);

	/* PLLSRC[1] = 1, PLLSRC0[0] = 0 (ie HSE/PREDIV selected as PLL clock */
	RCC_CFGR = (RCC_CFGR & ~RCC_CFGR_PLLSRC0) | RCC_CFGR_PLLSRC;

	rcc_osc_on(RCC_PLL);
	rcc_wait_for_osc_ready(RCC_PLL);
	rcc_set_sysclk_source(RCC_PLL);
	rcc_set_usbclk_source(RCC_PLL);
}

static void clock_init_hse(uint32_t pll_mult_factor)
{
	gpio_mode_setup(GPIOF, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO0|GPIO1);

	hse_out_48mhz(pll_mult_factor);
}
#endif

#if (EXTERNAL_CRYSTAL == 0)
void clock_init(void)
{
	/* start HSI48 */
	rcc_clock_setup_in_hsi48_out_48mhz();

	/* use usb SOF as correction source for HSI48 */
	crs_autotrim_usb_enable();

	/* use HSI48 for USB */
	rcc_set_usbclk_source(RCC_HSI48);

	/* usb HSI48 for system clock */
	rcc_set_sysclk_source(RCC_HSI48);
}
#elif (EXTERNAL_CRYSTAL == 16)
void clock_init(void)
{
	/* 16Mhz * 3 = 48Mhz */
	clock_init_hse(RCC_CFGR_PLLMUL_MUL3);
}
#elif (EXTERNAL_CRYSTAL == 8)
void clock_init(void)
{
	/* 8Mhz * 6 = 48Mhz */
	clock_init_hse(RCC_CFGR_PLLMUL_MUL6);
}
#else
# error "Crystal value invalid"
#endif
