/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include <unicore-mx/usb/class/cdc.h>
#include <unicore-mx/stm32/crs.h>
#include <unicore-mx/stm32/flash.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/st_usbfs.h>
#include <unicore-mx/stm32/memorymap.h>

#include <stdint.h>
#include "standard.h"
#include "dio.h"
#include "pwm.h"
#include "ain.h"
#include "aout.h"
#include "i2c.h"
#include "spi.h"
#include "common/handy.h"
#include "common/helper.h"
#include "config.h"
#include "usb.h"
#include "power.h"
#include "device.h"

void usb_reset(usbd_device *dev);

static void usb_set_interface(usbd_device *dev,
		const struct usb_interface *iface,
		const struct usb_interface_descriptor *altsetting);

static void usb_set_config(usbd_device *dev,
		const struct usb_config_descriptor *cfg);

bool interface_setup_callback(usbd_device *dev,
		const struct usb_setup_data *setup_data);

extern const struct usb_device_descriptor device_descriptor;
extern const struct b0_device box0_device;

/**
 * Buffer to be used for control requests.
 * This need to be 2byte aligned because
 *    it is effecient to copy from PMA at 16bit.
 * Also, older version of libopencm3 did not support unaligned copy.
 */
uint8_t usbd_control_buffer[USB_CONTROL_BUFFER] ALIGNED_2BYTE;

usbd_device *dev_global;

/**
 * Modules information and callback
 */
struct callback_map {
	uint8_t bModule; /**< module type */
	uint8_t bIndex; /**< module index */
	void (* init)(void); /**< called to initalize module */
	void (* deinit)(void); /**< called to de-initalize module */
	/** called when host send a control request.
	 *  @brief
	 *   Disable any peripherial clock specific to the module
	 *    ex: ADC is only require by AIN.
	 *   Make pins in hiz mode.
	 *   Disable share resources that are specific to module.
	 *    ex: DMA channel used by AIN */
	bool (*setup_callback)(usbd_device *dev,
				const struct usb_setup_data *setup_data);
};

/**
 * Store modules callback
 */
static const struct callback_map cb_map[/* bInterface */] = {
	{B0_DIO, 0, dio0_init, dio0_deinit, dio0_setup_callback},
	{B0_PWM, 0, pwm0_init, pwm0_deinit, pwm0_setup_callback},
	{B0_PWM, 1, pwm1_init, pwm1_deinit, pwm1_setup_callback},
	{B0_AIN, 0, ain0_init, ain0_deinit, ain0_setup_callback},
	{B0_AOUT, 0, aout0_init, aout0_deinit, aout0_setup_callback},
	{B0_I2C, 0, i2c0_init, i2c0_deinit, i2c0_setup_callback},
	{B0_SPI, 0, spi0_init, spi0_deinit, spi0_setup_callback}
};

/**
 * search and forware the control request vendor to appropriate module
 * @param[in] dev USB Device
 * @param[in] req USB Control transfer data
 * @param[out] buf Buffer
 * @param[in,out] len
 */
bool interface_setup_callback(usbd_device *dev,
					const struct usb_setup_data *setup_data)
{
	uint8_t bModule, bIndex;
	size_t i;

	bModule = B0_MODULE_TYPE_FROM_WINDEX(setup_data->wIndex);
	bIndex = B0_MODULE_INDEX_FROM_WINDEX(setup_data->wIndex);

	for (i = 0; i < ARRAY_LEN(cb_map); i++) {
		const struct callback_map *cb = &cb_map[i];
		if (cb->bModule == bModule && cb->bIndex == bIndex) {
			return cb->setup_callback(dev, setup_data);
		}
	}

	return false;
}

/**
 * Handle sending of box0 descriptors.
 * Box0 descriptors are like USB device and configuration descriptor.
 * Ref: http://stackoverflow.com/a/19470527/1500988
 */
bool box0_desc_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data)
{
	uint8_t type, index;

	if (setup_data->bRequest != USB_REQ_GET_DESCRIPTOR) {
		return false; /* Not of our interest */
	}

	/* extract descriptor type and index */
	type = setup_data->wValue >> 8;
	index = setup_data->wValue & 0xFF;

	/* requesting vendor descriptor? */
	if ((type & (0x3 << 5)) != (0x2 << 5)) {
		return false; /* Not supported */
	}

	switch (type) {
	case B0_DESC_DEVICE:
		ASSERT_STALL(index == 0);
		usbd_ep0_transfer(dev, setup_data, (void *) &box0_device,
			box0_device.bLength, NULL);
	break;
	case B0_DESC_MODULE:
		ASSERT_STALL(index < box0_device.bNumModules);
		usbd_ep0_transfer(dev, setup_data,
			(void *) box0_device.modules[index],
			box0_device.modules[index]->wTotalLength, NULL);
	break;
	default:
	goto stall;
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}

/**
 * Control handler routing table.
 * The request are filtered based on (bmReqType & type_mask) == type_value
 * If the filter matches, the callback is invoked.
 * After invoking, the callback return weather it has
 *    {handled, un-handled, or stalled} the request.
 * If the request is unhandled, the process continues till reach end of table.
 */
static const struct usbd_setup_callback_route {
	bool (*callback)(usbd_device *dev, const struct usb_setup_data *setup_data);
	uint8_t type_mask, type_value;
} setup_callbacks[] = {{
	.callback = interface_setup_callback,
	.type_mask = USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
	.type_value = USB_REQ_TYPE_VENDOR | USB_REQ_TYPE_INTERFACE
}, {
	.callback = device_setup_callback,
	.type_mask = USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
	.type_value = USB_REQ_TYPE_VENDOR | USB_REQ_TYPE_DEVICE
}, {
	.callback = box0_desc_setup_callback,
	.type_mask = USB_REQ_TYPE_DIRECTION | USB_REQ_TYPE_TYPE |
					USB_REQ_TYPE_RECIPIENT,
	.type_value = USB_REQ_TYPE_IN | USB_REQ_TYPE_STANDARD |
					USB_REQ_TYPE_DEVICE
}, {
	.callback = NULL /* END OF LIST */
}};

/**
 * Handle the control transfer.
 * This function is called before libopencm3 usb stack handle the request.
 *  if this function return un-handled, libopencm3 usb stack will try to handle the request.
 * If the request was successfully handled, "act" LED to blinked.
 */
void usb_setup_callback(usbd_device *dev, uint8_t addr,
					const struct usb_setup_data *setup_data)
{
	UNUSED(addr); /* assuming (addr == 0) */

	bool consumed = false;
	size_t i;

	for (i = 0; !consumed && setup_callbacks[i].callback; i++) {
		const struct usbd_setup_callback_route *route = &setup_callbacks[i];
		if ((route->type_mask & setup_data->bmRequestType) == route->type_value) {
			consumed = route->callback(dev, setup_data);
		}
	}

	if (!consumed) {
		/* No one consumed it, pass on to library */
		usbd_ep0_setup(dev, setup_data);
	}

	if ((setup_data->bmRequestType & USB_REQ_TYPE_TYPE) == USB_REQ_TYPE_VENDOR) {
		led_act_toggle();
	}
}

/**
 * @note care need to be taken, for alignment
 * since adc/dac could be in 16bit and
 * allocation is therefore to be  memory that is 16bit align'd
 *
 * @note check if wValue is not bogus as libopencm3 isnt doing any check
 *  there is a PR for this pending in libopencm3
 */
static void usb_set_config(usbd_device *dev,
				const struct usb_config_descriptor *cfg)
{
	UNUSED(cfg);

	/* AIN0 endpoints */
	usbd_ep_prepare(dev, AIN0_EP_ADDR, USBD_EP_BULK, AIN0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	/* AOUT0 endpoints */
	usbd_ep_prepare(dev, AOUT0_EP_ADDR, USBD_EP_BULK, AOUT0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	/* I2C0 endpoints */
	usbd_ep_prepare(dev, I2C0_EP_ADDR_IN, USBD_EP_BULK, I2C0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	usbd_ep_prepare(dev, I2C0_EP_ADDR_OUT, USBD_EP_BULK, I2C0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	/* SPI0 endpoints */
	usbd_ep_prepare(dev, SPI0_EP_ADDR_IN, USBD_EP_BULK, SPI0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	usbd_ep_prepare(dev, SPI0_EP_ADDR_OUT, USBD_EP_BULK, SPI0_BULK_EP_SIZE,
			USBD_INTERVAL_NA, USBD_EP_NONE);

	/* power supply */
	power_state_set(POWER_DIGITAL, POWER_OFF);
	power_state_set(POWER_ANALOG, POWER_OFF);

	/* This loop should not be here, but on Microsoft Windows,
	 * SET_INTERFACE not being set for interface
	 * which have single alternate setting. */
	for (size_t i = 0; i < ARRAY_LEN(cb_map); i++) {
		cb_map[i].init();
	}

	/* led's */
	led_act_en(true);
}

/**
 * Called for USB set interface
 * @param[in] dev USB Device
 * @param[in] wIndex request wIndex value
 * @param[in] wValue request wValue value
 */
static void usb_set_interface(usbd_device *dev,
		const struct usb_interface *iface,
		const struct usb_interface_descriptor *altsetting)
{
	UNUSED(dev);

	if (altsetting->bInterfaceNumber < ARRAY_LEN(cb_map)) {
		cb_map[altsetting->bInterfaceNumber].init();
	}
}

/**
 * Initalize USB
 */
void usb_init(void)
{
	usbd_device *dev;

	/* prepare the deviceID string */
	prepare_uid();

	/* initalize USB_DP USB_DM pins */
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO11 | GPIO12);

	dev = usbd_init(USBD_STM32_FSDEV_V2, NULL, &device_descriptor,
		usbd_control_buffer, sizeof(usbd_control_buffer));

	/* reset callback */
	usbd_register_reset_callback(dev, usb_reset);

	/* SET_CONFIGURATION callback */
	usbd_register_set_config_callback(dev, usb_set_config);

	/* SET_INTERFACE callback */
	usbd_register_set_interface_callback(dev, usb_set_interface);

	/* vendor specific (box0) command handling */
	usbd_register_setup_callback(dev, usb_setup_callback);

	dev_global = dev;

#if (USB_INTERRUPT_DESIGN == 1)
	/* enable usb interrupt and priority to high */
	nvic_set_priority(NVIC_USB_IRQ, NVIC_PRIO_HIGH);
	nvic_enable_irq(NVIC_USB_IRQ);
#endif
}

/**
 * Function to be called when USB reset occur
 */
void usb_reset(usbd_device *dev)
{
	UNUSED(dev);

	power_state_set(POWER_DIGITAL, POWER_OFF);
	power_state_set(POWER_ANALOG, POWER_OFF);

	led_act_en(false);

	/* iterate over all interface and execute deinit
	 * callback to set all interface */
	for (size_t i = 0; i < ARRAY_LEN(cb_map); i++) {
		cb_map[i].deinit();
	}
}

void usb_isr(void)
{
	usbd_poll(dev_global, 0);
}
