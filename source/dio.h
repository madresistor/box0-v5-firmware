/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_DIO_H
#define BOX0V5_DIO_H

#include "standard.h"
#include <stdint.h>
#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>

__BEGIN_DECLS

/*
 * on set interface all pin should be made HiZ to prevent damage
 */

/* output/input mode */
#define B0_DIO_CAPAB_OUTPUT (0x1 << 0)
#define B0_DIO_CAPAB_INPUT (0x1 << 1)
#define B0_DIO_CAPAB_HIZ (0x1 << 2)
#define B0_DIO_CAPAB_PD (0x1 << 3)
#define B0_DIO_CAPAB_PU (0x1 << 4)

/* interrupt capability (using interrupt endpoint)
 * interrupt direction
 * implemented using interrupt endpoint
 * TODO: explore usecase and design interrupt specs
 */
//#define B0_DIO_INTERRUPT_LOW_TO_HIGH (0x1 << 4)
//#define B0_DIO_INTERRUPT_HIGH_TO_LOW (0x1 << 5)
//#define B0_DIO_INTERRUPT_LOW_TO_LOW (0x1 << 6)
//#define B0_DIO_INTERRUPT_HIGH_TO_HIGH (0x1 << 7)

struct b0_dio0 {
	struct b0_module header;
	struct b0_label8 label8;
	struct b0_capab capab;
	struct b0_count count;
	struct b0_ref ref;
}PACKED;

/*
 * set pin value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_VALUE_SET
 * wValue: HIGH8(<pin-index>) LOW8(v)
 * 		v = 0x1 = high
 * 		v = 0x0 = low
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 * STALL if pin-index out of range
 * STALL if pin in input mode
 * STALL if pin is not configured
 */
#define B0_DIO_VALUE_SET 101

/*
 * get pin value
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_VALUE_GET
 * wValue: HIGH8(<pin-index>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 1
 *
 * STALL if pin-index out of range
 * STALL if pin is not configured
 */
#define B0_DIO_VALUE_GET 102

/*
 * toggle pin value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_VALUE_TOGGLE
 * wValue: HIGH8(<pin-index>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 * STALL if pin-index out of range
 * STALL if pin in input mode
 * STALL if pin is not configured
 */
#define B0_DIO_VALUE_TOGGLE 103

/*
 * set pin direction
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_DIR_SET
 * wValue: HIGH8(<pin-index>) LOW8(c)
 * 		c = 0x01 = output
 * 		c = 0x00 = input
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 * STALL if pin-index out of range
 * STALL if pin in input mode
 */
#define B0_DIO_DIR_SET 104

/*
 * get pin direction
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_DIR_GET
 * wValue: HIGH8(<pin-index>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 1
 *
 * data: c
 * 	c = 0x00 = input
 * 	c = 0x01 = output
 *
 * STALL if pin-index out of range
 * STALL if pin not configured
 */
#define B0_DIO_DIR_GET 105

/*
 * set pin into hiz mode
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_VALUE_SET
 * wValue: HIGH8(<pin-index>) LOW8(v)
 * 		v = 0x1 = yes
 * 		v = 0x0 = no
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 * STALL if pin-index out of range
 */
#define B0_DIO_HIZ_SET 106

/*
 * get pin value
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_HIZ_GET
 * wValue: HIGH8(<pin-index>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 1
 *
 * data: 0x01 yes, 0x00 no
 *
 * STALL if pin-index out of range
 */
#define B0_DIO_HIZ_GET 107

/*
 * set multiple pins value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_VALUE_SET
 * wValue: HIGH8(<start-pin>) LOW8(<v>)
 * 		start-pin: pin to start pins calculation
 * 		v = 0x01 = high
 * 		v = 0x00 = low
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 * data = pinmask
 * 			HIGH = pin to be executed
 * 			LOW = pin not to be executed
 *
 *  for (i = <start-pin>; i < MIN(<wLength> * 8, <total-pins>); i++)
 * 		pin[i] = <v>
 *
 * STALL if pin any in input mode
 * STALL if pin any is not configured
 */
#define B0_DIO_MULTIPLE_VALUE_SET 121

/*
 * get multiple pins value
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_VALUE_GET
 * wValue: HIGH8(start-pin) LOW8(v)
 * 		start-pin: pin to start pins calculation
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: bitmap of dio data
 * STALL if any pin is not configured
 * STALL if wLength > CEIL(pin-count/8) bytes
 * STALL if wLength == 0
 */
#define B0_DIO_MULTIPLE_VALUE_GET 122

/*
 * toggle multiple pin value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_VALUE_TOGGLE
 * wValue: HIGH8(start-pin) LOW8(0)
 * 		start-pin: pin to start pins calculation
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 * STALL if pin-index out of range
 * STALL if pin in input mode
 * STALL if pin is not configured
 */
#define B0_DIO_MULTIPLE_VALUE_TOGGLE 123

/*
 * set multiple pins direction
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_DIR_SET
 * wValue: HIGH8(<start-pin>) LOW8(c)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * set pin direction to c when ever bitmap contain mask high
 *
 * STALL if pin-index out of range
 * STALL if pin in input mode
 */
#define B0_DIO_MULTIPLE_DIR_SET 124

/*
 * get multiple pins direction
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_DIR_GET
 * wValue: HIGH8(<pin-index>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * return a bitmap that contain HIGH when ever direction is equal to c
 *
 * STALL if pin-index out of range
 * STALL if pin not configured
 */
#define B0_DIO_MULTIPLE_DIR_GET 125

/*
 * set multiple pins hiz
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_VALUE_SET
 * wValue: HIGH8(<start-pin>) LOW8(<v>)
 * 		start-pin: pin to start pins calculation
 * 		v = 0x01 = yes
 * 		v = 0x00 = no
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 * data = pinmask
 * 			HIGH = pin to be executed
 * 			LOW = pin not to be executed
 *
 *  for (i = <start-pin>; i < MIN(<wLength> * 8, <total-pins>); i++)
 * 		hiz[i] = <v>
 */
#define B0_DIO_MULTIPLE_HIZ_SET 126

/*
 * get multiple pins hiz
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_MULTIPLE_HIZ_GET
 * wValue: HIGH8(start-pin) LOW8(v)
 * 		start-pin: pin to start pins calculation
 * 		v = 0x01 = yes
 * 		v = 0x00 = no
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: bitmap of dio data, HIGH = value-equal, LOW = value-not-equal
 * STALL if wLength > CEIL(pin-count/8) bytes
 * STALL if wLength == 0
 */
#define B0_DIO_MULTIPLE_HIZ_GET 127

/*
 * set all pins value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_ALL_VALUE_SET
 * wValue: HIGH8(0) LOW8(<v>)
 * 		v = 0x01 = high
 * 		v = 0x00 = low
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 *
 * data: None
 *
 * NOTE  Only those which are output are affected
 */
#define B0_DIO_ALL_VALUE_SET 141

/*
 * get all pins value
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_ALL_DIR_GET
 * wValue: HIGH8(0) LOW8(v)
 * 		v = 0x01 = output
 * 		v = 0x00 = input
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: bitmap of dio data
 * STALL if wLength > CEIL(pin-count/8) bytes
 * STALL if wLength == 0
 * NOTE bitmap has invalid value for unconfigure pins
 */
#define B0_DIO_ALL_VALUE_GET 142

/*
 * toggle all pin value
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_ALL_VALUE_TOGGLE
 * wValue: HIGH8(0) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: bitmap of dio data
 *
 * NOTE  Only those which are output are affected
 */
#define B0_DIO_ALL_VALUE_TOGGLE 143

/*
 * set all pins direction
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_ALL_DIR_SET
 * wValue: HIGH8(0) LOW8(c)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 */
#define B0_DIO_ALL_DIR_SET 144

/*
 * get all pins direction
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_ALL_DIR_GET
 * wValue: HIGH8(0) LOW8(c)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: a bitmap that contain HIGH when ever direction is equal to c for all pins
 */
#define B0_DIO_ALL_DIR_GET 145

/*
 * set all pins hiz
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_DIO_ALL_VALUE_SET
 * wValue: HIGH8(0) LOW8(<v>)
 * 		v = 0x01 = yes
 * 		v = 0x00 = no
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 *
 *  for (i = 0; i < <total-pins>; i++)
 * 		hiz[i] = <v>
 */
#define B0_DIO_ALL_HIZ_SET 146

/*
 * get all pins hiz
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_DIO_ALL_HIZ_GET
 * wValue: HIGH8(0) LOW8(v)
 * 		start-pin: pin to start pins calculation
 * 		v = 0x01 = yes
 * 		v = 0x00 = no
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: > 0
 *
 * data: bitmap of dio data, HIGH = value-equal, LOW = value-not-equal
 * STALL if wLength > CEIL(pin-count/8) bytes
 * STALL if wLength == 0
 */
#define B0_DIO_ALL_HIZ_GET 147

/* their are three kind of command
 *
 * single pin: for single pin
 * multiple pins: for multiple pins, pinmask is provided to select a range of pins
 * all pins: applies to all pins
 */

/* extract pin number from wvalue */
#define B0_DIO_PIN_FROM_WVALUE(wValue) (wValue >> 8)

/* Pin direction */
#define B0_DIO_OUTPUT 0x01
#define B0_DIO_INPUT 0x00

/* Pin value */
#define B0_DIO_LOW 0x0
#define B0_DIO_HIGH 0x1

/* Pin HiZ */
#define B0_DIO_DISABLE 0x0
#define B0_DIO_ENABLE 0x1

void dio0_deinit(void);
void dio0_init(void);
bool dio0_setup_callback(usbd_device *dev,
					const struct usb_setup_data *setup_data);

__END_DECLS

#endif
