/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_CONFIG_H
#define BOX0V5_CONFIG_H

#include "common/handy.h"

__BEGIN_DECLS

/* USB */
#define USB_EP0_SIZE 64
#define USB_CONTROL_BUFFER (128)

/* AIN0 */
#define AIN0_EP_ADDR 0x81
#define AIN0_BUFFER_SIZE (2048) /* Should be multiple of 2 */
#define AIN0_BULK_EP_SIZE 64

/* AOUT0 */
#define AOUT0_EP_ADDR 0x02
#define AOUT0_BUFFER_SIZE (10240) /* Should be multiple of 2 */
#define AOUT0_BULK_EP_SIZE 64

/* I2C0 */
#define I2C0_EP_ADDR_IN 0x83
#define I2C0_EP_ADDR_OUT 0x03
#define I2C0_BUFFER_SIZE 64
#define I2C0_BULK_EP_SIZE 64

/* SPI0 */
#define SPI0_EP_ADDR_IN 0x84
#define SPI0_EP_ADDR_OUT 0x04
#define SPI0_BUFFER_SIZE 64
#define SPI0_BULK_EP_SIZE 64

/* ------------------------------------------------------------------ */

/*
 * use external crystal
 * 16	16Mhz crystal
 * 8	8Mhz crystal
 * 0	use internal clock
 */
#if !defined(EXTERNAL_CRYSTAL)
# define EXTERNAL_CRYSTAL 16
#endif

/*
 * device voltage
 * NOTE: used in device descriptor for DIO0, PWM0, PWM1, SPI0, I2C0 Vref+
 * TYPE: floating number
 * Unit: volts
 */
#if !defined(DEVICE_VOLTAGE)
# define DEVICE_VOLTAGE 3.3
#endif

/*
 * validate i2c0 transaction before executing
 * NOTE: this validation can be instead performed on host library
 * TYPE: boolean
 */
#if !defined(I2C0_TRANSACTION_VALIDATION_ENABLE)
# define I2C0_TRANSACTION_VALIDATION_ENABLE 1
#endif

/*
 * validate spi0 transaction before executing
 * NOTE: this validation can be instead performed on host library
 * TYPE: boolean
 */
#if !defined(SPI0_TRANSACTION_VALIDATION_ENABLE)
# define SPI0_TRANSACTION_VALIDATION_ENABLE 1
#endif

/*
 * Vref+ for AIN0
 * TYPE: floating number
 * Unit: volts
 */
#if !defined(AIN_REF_HIGH)
# define AIN_REF_HIGH (5.0259)
#endif


/*
 * Vref- for AIN0
 * TYPE: floating number
 * Unit: volts
 */
#if !defined(AIN_REF_LOW)
# define AIN_REF_LOW (-5.0259)
#endif

/*
 * Vref+ for AOUT0
 * TYPE: floating number
 * Unit: volts
 */
#if !defined(AOUT_REF_HIGH)
# define AOUT_REF_HIGH (3.3)
#endif

/*
 * Vref- for AOUT0
 * TYPE: floating number
 * Unit: volts
 */
#if !defined(AOUT_REF_LOW)
# define AOUT_REF_LOW (-3.3)
#endif

/*
 * AIN0 can run at full 1MS in snapshot mode.
 *
 * Hack: turn on full speed, but do note that timer + ADC wont work at full 1MS
 * (as per calculation only upto ~850KS)
 *
 * so, instead of triggering using Timer, ADC is run at full pace.
 */
#if !defined(AIN0_1MS_HACK)
# define AIN0_1MS_HACK 0
#endif

/* use ISR instead of loop check for new interrupt */
#if !defined(USB_INTERRUPT_DESIGN)
# define USB_INTERRUPT_DESIGN 1
#endif

/* The duration between two poll when "USB_INTERRUPT_DESIGN" is used.
 * The value can vary from 1 to 1000  (unit: Micro second)
 */
#if !defined(USBD_INTERRUPT_POLL_DELAY_US)
# define USBD_INTERRUPT_POLL_DELAY_US 1000 /* 1 ms */
#endif

/*
 * maximum speed for snapshot AIN0 (without HACK).
 * also, note their is a special case of running at 1MS/S (see AIN0_1MS_HACK)
 */
#if !defined(AIN0_SNAPSHOT_MAX_SPEED)
# define AIN0_SNAPSHOT_MAX_SPEED B0_SPEED_KHZ(600)
#endif

/*
 * include hardware details in string descriptor (if possible)
 * for ex: AIN0 "IN0" is actually "PA3".
 *  so, include the "PA0" information too
 * or AIN0 use "ADC0, TIM15" and "DMA_CH1", so include this in name
 * note: this could (if any) useful while debugging
 */
#if !defined(VERBOSE_STRING_DESC)
# define VERBOSE_STRING_DESC 0
#endif

#define AIN0_CHAN_COUNT 4
#define AOUT0_CHAN_COUNT 2
#define SPI0_SS_COUNT 4
#define DIO0_PIN_COUNT 8

__END_DECLS

#endif
