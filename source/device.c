/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/flash.h>
#include "device.h"
#include "power.h"
#include "config.h"
#include "standard.h"
#include "usb.h"
#include "usb.h"

/**
 * Activity LED Behaviour:
 *  this should give a sense to the user that the device is working
 *
 *  off: the device is not connected to any host (or configuration not set)
 *  on: no activity, but connected to host (usb configuration set)
 *  toggle: activity (only when box0 vendor command are successfully executed)
 *
 * Power LED Behaviour: (Analog And Digital Power)
 *  this should give a sense to the user that power is OK.
 *  if thing are not ok, if their is something wrong,
 *   rapid flashing shall get h(er|is) attraction.
 *
 *  off: neither power supply has been enabled
 *  on: atlease one of the power supply has been enabled
 *  toggle: short circuit detected (and disable)
 */

/**
 * initalize acitivity led.
 *  - enable the required peripherial clock
 *  - configure the timer that will generate the PWM
 *  - turn off the LED by default
 *
 * @note
 * LED is toggled by Timer using PWM.
 *  timer is configured in one shot mode.
 *  when counter is enabled, it output PWM.
 *  if counter enable is called again, (while the timer is running) it is ignored.
 *
 *  so effectively a toggle with precise timing and
 *    non blocking operation with the help of timer.
 *
 * @note with the help of alternate function, timer is connected (and disconnected) from led
 *
 * @see led_act_en()
 * @see led_act_toggle()
 */
void led_act_init(void)
{
	rcc_periph_clock_enable(RCC_TIM14);

	/*
	 * Off:
	 *  5ms duty cycled LED (this will give 5% duty cycle since the period is 100ms)
	 *  It is done so to let user know that the device is in unconfigured state.
	 *
	 * On:
	 *  50ms off.
	 *  It goes to off for 50ms and then goes back to on state.
	 *
	 * - sequence: [50ms] [HIGH->LOW] [50ms] [LOW->HIGH]
	 *         active ->|  |<-  inactive ->|  |<- active
	 * - in-active HIGH
	 */
	timer_set_oc_mode(TIM14, TIM_OC1, TIM_OCM_PWM1);
	led_act_en(false);
	timer_enable_oc_output(TIM14, TIM_OC1);

	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO7);
	gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, GPIO7);
	gpio_set_af(GPIOA, GPIO_AF4, GPIO7);
}

/**
 * turn activity led states.
 * @param on turn LED on or off.
 * @note only led_act_toggle() can be called when LED is on
 *
 * @see led_act_init()
 * @see led_act_toggle()
 */
void led_act_en(bool on)
{
	TIM14_CNT = 0;

	if (on) {
		/* initalize prescaler to divide 48Mhz by 48000.
		 * resullt: precaler output 1Khz */
		timer_set_prescaler(TIM14, 48000 - 1);
		timer_disable_counter(TIM14);
		timer_one_shot_mode(TIM14);
		timer_set_period(TIM14, 100);
		timer_set_oc_value(TIM14, TIM_OC1, 50);
	} else {
		timer_set_prescaler(TIM14, 48 - 1);
		timer_continuous_mode(TIM14);
		timer_set_period(TIM14, 100);
		timer_set_oc_value(TIM14, TIM_OC1, 3);
		timer_enable_counter(TIM14);
	}
}

/**
 * toggle the LED.
 *  instead of actually toggling, the timer will actually generate a toggle.
 *
 * @note
 *  if the timer is running and another toggled is request, the request is ingored.
 *  this is done to ensure that the toggle is information to Human
 *
 * @note this function can only be called after led_act_en(true)
 *
 * @see led_act_init()
 * @see led_act_en()
 */
void led_act_toggle(void)
{
	timer_enable_counter(TIM14);
}

#define ANALOG_MASK (1 << 0)
#define DIGITAL_MASK (1 << 1)

#define POWER_OC_ACK(ic, mask) 									\
	if (setup_data->wValue & mask) {							\
		ASSERT_STALL(power_state_get(ic) == POWER_OC);			\
		power_state_set(ic, POWER_OFF);							\
	}

#define POWER_EN_SET(ic, mask)									\
	if (setup_data->wValue & (mask << 8)) {						\
		bool marked = !!(setup_data->wValue & mask);			\
		power_state_set(ic, marked ? POWER_ON : POWER_OFF);		\
	}

#define POWER_EN_GET(ic, mask) ((power_state_get(ic) == POWER_ON) ? mask : 0x00)

#define POWER_OC_GET(ic, mask) ((power_state_get(ic) == POWER_OC) ? mask : 0x00)

extern uint8_t _nvm;

void flash_multi_program_half_word(volatile uint16_t *dest,
		const uint16_t *src, uint32_t count)
{
	uint32_t i;

	flash_wait_for_last_operation();

	/* Start programming */
	FLASH_CR |= FLASH_CR_PG;

	for (i = 0; i < count; i++) {
		flash_wait_for_last_operation();
		dest[i] = src[i];
	}

	/* End programming */
	FLASH_CR &= ~FLASH_CR_PG;
}

void nvm_set_status_stage(usbd_device *dev)
{
	/* status stage */
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_CONTROL,
		.ep_addr = 0x80,
		.ep_size = USB_EP0_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = NULL,
		.length = 0,
		.flags = USBD_FLAG_NONE,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = NULL
	};

	usbd_transfer_submit(dev, &transfer);
}

void nvm_set_data_stage_callback(usbd_device *dev,
		const usbd_transfer *transfer, usbd_transfer_status status,
		usbd_urb_id urb_id)
{
	if (status == USBD_ONE_PACKET_DATA) {
		uint32_t pkt_index = (transfer->transferred - 1) / transfer->ep_size;
		uint32_t old_bytes = pkt_index * transfer->ep_size;
		uint32_t pkt_bytes = transfer->transferred - old_bytes;
		volatile uint16_t *dest = transfer->user_data + old_bytes;

		flash_unlock();

		if (!pkt_index) {
			/* First packet, perform erase */
			flash_erase_page((uint32_t) &_nvm);
		}

		flash_multi_program_half_word(dest, transfer->buffer, pkt_bytes / 2);

		flash_lock();

		return;
	}

	if (status == USBD_SUCCESS) {
		nvm_set_status_stage(dev);
	}
}

void nvm_set_data_stage(usbd_device *dev,
		const struct usb_setup_data *setup_data)
{
	/* data stage */
	const usbd_transfer transfer = {
		.ep_type = USBD_EP_CONTROL,
		.ep_addr = 0x00,
		.ep_size = USB_EP0_SIZE,
		.ep_interval = USBD_INTERVAL_NA,
		.buffer = usbd_control_buffer,
		.length = setup_data->wLength,
		.flags = USBD_FLAG_PER_PACKET_CALLBACK,
		.timeout = USBD_TIMEOUT_NEVER,
		.callback = nvm_set_data_stage_callback,
		.user_data = &_nvm + setup_data->wIndex
	};

	usbd_transfer_submit(dev, &transfer);
}

/**
 * handle vendor+device control request
 * @param[in] dev USB Device
 * @param[in] req USB Control transfer data
 * @param[out] buf Buffer
 * @param[in,out] len
 */
bool device_setup_callback(usbd_device *dev, const struct usb_setup_data *setup_data)
{
	if ((setup_data->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN) {
		uint8_t *buf =  usbd_control_buffer;

		switch (setup_data->bRequest) {
		case BOX0V5_PS_EN_GET: {
			uint8_t value = 0;
			value |= POWER_EN_GET(POWER_ANALOG, ANALOG_MASK);
			value |= POWER_EN_GET(POWER_DIGITAL, DIGITAL_MASK);
			buf[0] = value;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		} break;
		case BOX0V5_PS_OC_GET: {
			uint8_t value = 0;
			value |= POWER_OC_GET(POWER_ANALOG, ANALOG_MASK);
			value |= POWER_OC_GET(POWER_DIGITAL, DIGITAL_MASK);
			buf[0] = value;
			usbd_ep0_transfer(dev, setup_data, buf, 1, NULL);
		} break;
		case BOX0V5_NVM_GET:
			ASSERT_STALL(setup_data->wLength <= 2048);
			ASSERT_STALL((setup_data->wIndex + setup_data->wLength) <= 2048);
			usbd_ep0_transfer(dev, setup_data, &_nvm + setup_data->wIndex,
								setup_data->wLength, NULL);
		break;
		/* unknown command */
		default:
		goto stall;
		}
	} else {
		switch (setup_data->bRequest) {
		case B0_NOP:
			/* No Operation */
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		break;
		case BOX0V5_PS_EN_SET: {
			POWER_EN_SET(POWER_ANALOG, ANALOG_MASK);
			POWER_EN_SET(POWER_DIGITAL, DIGITAL_MASK);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		} break;
		case BOX0V5_PS_OC_ACK: {
			POWER_OC_ACK(POWER_ANALOG, ANALOG_MASK);
			POWER_OC_ACK(POWER_DIGITAL, DIGITAL_MASK);
			usbd_ep0_transfer(dev, setup_data, NULL, 0, NULL);
		} break;
		case BOX0V5_NVM_SET:
			ASSERT_STALL(setup_data->wLength <= 2048);
			ASSERT_STALL((setup_data->wIndex + setup_data->wLength) <= 2048);
			nvm_set_data_stage(dev, setup_data);
		break;
		default:
		goto stall;
		}
	}

	return true;

	stall:
	usbd_ep0_stall(dev);
	return true;
}
