/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_STANDARD_H
#define BOX0V5_STANDARD_H

#include <stdint.h>
#include "common/handy.h"
#include <unicore-mx/usb/usbstd.h>

__BEGIN_DECLS

#define B0_DESC_TYPE(id) ((0x2 << 5) | ((id) & 0x1F))
#define B0_DESC_DEVICE  B0_DESC_TYPE(1)
#define B0_DESC_MODULE  B0_DESC_TYPE(2)
#define B0_DESC_PROPERTY  B0_DESC_TYPE(3)

#define B0_DIO 1
#define B0_AOUT 2
#define B0_AIN 3
#define B0_SPI 4
#define B0_I2C 5
#define B0_PWM 6

#define B0_REF 1
#define B0_SPEED 2
#define B0_BITSIZE 3
#define B0_CAPAB 4
#define B0_COUNT 5
#define B0_LABEL 6
#define B0_BUFFER 7
#define B0_BITSIZE_SPEED 10

/* device type command */

/* No Operation. All device need to accept (used for ping) */
#define B0_NOP 1

/* states of module */
#define B0_STATE_GET 3
#define B0_STATE_SET 4

#define B0_STATE_STOP  0x00
#define B0_STATE_START 0x01
#define B0_STATE_STOPPED 0x00
#define B0_STATE_RUNNING 0x01

#define B0_STATE_FROM_WVALUE(wValue) (wValue & 0xFF)

/* channel list */
#define B0_CHAN_SEQ_GET 7
#define B0_CHAN_SEQ_SET 8

/* bitsize */
#define B0_BITSIZE_GET 9
#define B0_BITSIZE_SET 10

#define B0_BITSIZE_FROM_WVALUE(wValue) (wValue & 0xFF)

/* output */
#define B0_REPEAT_GET 11
#define B0_REPEAT_SET 12

/* bitsize and speed -- for AIN and AOUT */
#define B0_BITSIZE_SPEED_SET 13
#define B0_BITSIZE_SPEED_GET 14

/* Control request argument (data stage) */
struct b0_bitsize_speed_arg {
	uint32_t dSpeed;
	uint8_t bBitsize;
} PACKED;

#define B0_MODE_SET 15
#define B0_MODE_GET 16

#define B0_MODE_SNAPSHOT 0x0 /* AIN, AOUT */
#define B0_MODE_BASIC 0x0 /* DIO */
#define B0_MODE_OUTPUT 0x0 /* PWM */
#define B0_MODE_MASTER 0x0 /* SPI, I2C */
#define B0_MODE_STREAM 0x1 /* AIN, AOUT */

#define B0_MODE_FROM_WVALUE(wValue) (wValue & 0xFF)

/**
 * Box0 USB Versioning:
 * TODO: place the versioning scheme here
 */

/**
 * @brief used as header marker of start of a module
 * @param bLength size of the descriptor
 * @param bDescriptorType should be VENDOR_SPECIFIC (0xFF)
 * @param bcdBOX0 box0 ver of the module confirm to. (in BCD format)
 * @param wTotalLength total length the module including properties
 * @param bModule type of module
 * @param bIndex value that will be used to reference the module
 * @param bInterface module attached to which interface
 *
 * when control reguest is performed:
 * wIndex paramter of control transfer is:
 * 		B0_WINDEX_FROM_MODULE(module) = HIGH8(module.bModule) | LOW8(module.bIndex)
 *
 * HIGH8(x) = (x << 8)
 * LOW8(x) = (x << 0)
 */
struct b0_module {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint16_t wTotalLength;
	uint8_t bModule;
	uint8_t bIndex;
	uint8_t bInterface;
	uint8_t iName;
} PACKED;

#define FILL_B0_MODULE(type_size, module, index, interface, name) {		\
		.bLength = sizeof(struct b0_module),								\
		.bDescriptorType = B0_DESC_MODULE,									\
		.wTotalLength = type_size,											\
		.bModule = module,													\
		.bIndex = index,													\
		.bInterface = interface,											\
		.iName = name														\
	}

/**
 * @brief used as header for other properties
 * @param bLength size of the descriptor
 * @param bDescriptorType should be VENDOR_SPECIFIC (0xFF)
 * @param bProperty name of property
 */
struct b0_property {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bProperty;
} PACKED;

#define FILL_B0_PROPERTY(type_size, property_type) {						\
		.bLength = type_size,												\
		.bDescriptorType = B0_DESC_PROPERTY,								\
		.bProperty = property_type											\
	}

#define B0_REF_TYPE_VOLTAGE 0
#define B0_REF_TYPE_CURRENT 1

/**
 * @brief reference property
 * @param header header of type struct b0_property
 * @param bType type of ref
 * @param spLow Single Precision Low value
 * @param spHigh Single Precision Low value
 */
struct b0_ref {
	struct b0_property header;
	uint8_t bType;
	float spLow;
	float spHigh;
} PACKED;

#define FILL_B0_REF(type, low, high) {										\
		.header = FILL_B0_PROPERTY(											\
			sizeof(struct b0_ref), B0_REF),								\
		.bType = type,														\
		.spLow = low,														\
		.spHigh = high,														\
	}

#define B0_SPEED_OPER_RAW 0
#define B0_SPEED_OPER_INC 1
#define B0_SPEED_OPER_DIV 2
#define B0_SPEED_OPER_DEC 3

#define B0_SPEED_KHZ(val) (val * 1000)
#define B0_SPEED_MHZ(val) (val * 1000000)

/*  speed */
/*
 * set new speed
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_SPEED_SET
 * wValue: 0
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 4
 *
 * data: <speed register value>
 *
 * STALL if module dont support speed
 */
#define B0_SPEED_SET 1

/*
 * get the current speed
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_SPEED_GET
 * wValue: 0
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 4
 *
 * data: <speed value>
 */
#define B0_SPEED_GET 2

/**
 * @brief speed header
 * @param header header of type struct b0_property
 * @param bOperator operator type
 */
struct b0_speed {
	struct b0_property header;
	uint8_t bOperator;
} PACKED;

#define FILL_B0_SPEED(type_size, operator) {							\
		.header = FILL_B0_PROPERTY(type_size, B0_SPEED),				\
		.bOperator = operator,											\
	}

/**
 * @brief speed (incremental)
 * @param header header of type struct b0_speed
 * @param lCount number of values to generate
 * @param lStart number to start calculation from
 * @param lIncrement value to increment in each iteration
 *
 * @note lCount = 0 is not a valid value
 * @note lStart = 0 is a valid value
 * @note lIncrement = 0 is not a valid value
 */
struct b0_speed_inc {
	struct b0_speed header;
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dIncrement;
} PACKED;

/* generate values using lStart + (0 .... (lCount - 1))*lIncrement */

#define FILL_B0_SPEED_INC(start, count, inc) {							\
		.header = FILL_B0_SPEED(										\
			sizeof(struct b0_speed_inc), B0_SPEED_OPER_INC),			\
		.dStart = start,												\
		.dCount = count,												\
		.dIncrement = inc												\
	}

/**
 * @brief speed (decement)
 * @param header header of type struct b0_speed
 * @param lCount number of values to generate
 * @param lStart number to start calculation from
 * @param lDecrement value to decrement in each iteration
 *
 * @note lCount = 0 is not a valid value
 * @note lStart = 0 is a valid value
 * @note lDecrement = 0 is not a valid value
 */
struct b0_speed_dec {
	struct b0_speed header;
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dDecrement;
} PACKED;

/* generate values using lStart - (0 .... (lCount - 1))*lDecrement */

#define FILL_B0_SPEED_DEC(start, count, dec) {							\
		.header = FILL_B0_SPEED(										\
			sizeof(struct b0_speed_dec), B0_SPEED_OPER_DEC),			\
		.dStart = start,												\
		.dCount = count,												\
		.dDecrement = dec												\
	}


/**
 * @brief generate speed using division
 * @param header header of type struct b0_speed
 * @param lCount number of values to generate
 * @param lSpeed value to start division from
 * @note lCount = 0 is not a valid value
 * @note lCount = 1 is not a valid value
 * @note lSpeed = 0 is not a valid value
 */
struct b0_speed_div {
	struct b0_speed header;
	uint32_t dCount;
	uint32_t dSpeed;
} PACKED;

#define FILL_B0_SPEED_DIV(count, speed) {								\
		.header = FILL_B0_SPEED(										\
			sizeof(struct b0_speed_div), B0_SPEED_OPER_DIV),			\
		.dCount = count,												\
		.dSpeed = speed													\
	}

/**
 * @brief generate speed from raw values
 * @param lSpeed[] speed values
 */
#define DECLARE_B0_SPEED_RAW(length_of_array)							\
struct b0_speed_raw ##length_of_array {								\
	struct b0_speed header;											\
	uint32_t dValues[length_of_array];									\
} PACKED

DECLARE_B0_SPEED_RAW(1);
DECLARE_B0_SPEED_RAW(8);

#define FILL_B0_SPEED_RAW(type_size, ...) {								\
		.header = FILL_B0_SPEED(type_size, B0_SPEED_OPER_RAW),			\
		.dValues = {__VA_ARGS__}										\
	}

/**
 * @brief bitsize values
 * @param header header of type b0_property
 * @param bValues[] array of bitsizes
 */
#define DECLARE_B0_BITSIZE(length_of_array)								\
struct b0_bitsize ##length_of_array {										\
	struct b0_property header;											\
	uint8_t bValues[length_of_array];										\
} PACKED

DECLARE_B0_BITSIZE(2);
DECLARE_B0_BITSIZE(1);
DECLARE_B0_BITSIZE(13);

#define FILL_B0_BITSIZE(type_size, ...) {									\
		.header = FILL_B0_PROPERTY(type_size, B0_BITSIZE),					\
		.bValues = {__VA_ARGS__}											\
	}

#define B0_BITSIZE_SPEED_SNAPSHOT_USE 0
#define B0_BITSIZE_SPEED_STREAM_USE 1

struct b0_bitsize_speed {
	struct b0_property header;

	uint8_t bUse;

	/* Rule:
	 *  - unused are 0
	 *  - atleast value is provided
	 *  - valid at top
	 */
	uint8_t bBitsize[3];

	uint8_t bSpeedOperator;
} PACKED;

#define FILL_B0_BITSIZE_SPEED(type_size, use, bs0, bs1, bs2, oper) {		\
		.header = FILL_B0_PROPERTY(type_size, B0_BITSIZE_SPEED),			\
		.bUse = use,														\
		.bBitsize = {bs0, bs1, bs2},										\
		.bSpeedOperator = oper												\
	}

struct b0_bitsize_speed_inc {
	struct b0_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedStart;
	uint32_t dSpeedIncrement;
} PACKED;

#define FILL_B0_BITSIZE_SPEED_INC(use, bs0, bs1, bs2, speed_oper,			\
								speed_count, speed_start, speed_inc) {		\
		.header = FILL_B0_BITSIZE_SPEED(									\
					sizeof(struct b0_bitsize_speed_div),					\
					bs0, bs1, bs2, B0_SPEED_OPER_INC),						\
		.dSpeedCount = speed_count,											\
		.dSpeedStart = speed_start,											\
		.dSpeedIncrement = speed_inc,										\
	}

struct b0_bitsize_speed_div {
	struct b0_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedSpeed;
} PACKED;

#define FILL_B0_BITSIZE_SPEED_DIV(use, bs0, bs1, bs2, speed_oper,			\
									speed_count, speed_speed) {				\
		.header = FILL_B0_BITSIZE_SPEED(									\
					sizeof(struct b0_bitsize_speed_div),					\
					bs0, bs1, bs2, B0_SPEED_OPER_DIV),						\
		.dSpeedCount = speed_count,											\
		.dSpeedSpeed = speed_speed											\
	}

#define DECLARE_B0_BITSIZE_SPEED_RAW(length_of_array)						\
struct b0_bitsize_speed_raw ##length_of_array {							\
	struct b0_bitsize_speed header;										\
	uint32_t dSpeedValues[length_of_array];								\
} PACKED

DECLARE_B0_BITSIZE_SPEED_RAW(1);
DECLARE_B0_BITSIZE_SPEED_RAW(6);

#define FILL_B0_BITSIZE_SPEED_RAW(type_size, use, bs0, bs1, bs2, ...) {	\
		.header = FILL_B0_BITSIZE_SPEED(type_size, use,					\
						bs0, bs1, bs2, B0_SPEED_OPER_RAW),					\
		.dSpeedValues = {__VA_ARGS__}										\
	}

struct b0_bitsize_speed_dec {
	struct b0_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedStart;
	uint32_t dSpeedDecrement;
} PACKED;

#define FILL_B0_BITSIZE_SPEED_DEC(use, bs0, bs1, bs2, speed_count,			\
												speed_start, speed_dec) {	\
		.header = FILL_B0_BITSIZE_SPEED(									\
						sizeof(struct b0_bitsize_speed_dec), use,		\
						bs0, bs1, bs2, B0_SPEED_OPER_DEC),					\
		.dSpeedCount = speed_count,											\
		.dSpeedStart = speed_start,											\
		.dSpeedDecrement = speed_dec										\
	}

/**
 * @brief capab of a module (module specific)
 * @param header header of type struct b0_property
 * @param bmCapab0 bitmap of capab
 * @note bmCapabX can be added as per module
 */
struct b0_capab {
	struct b0_property header;
	uint8_t bmCapab;
} PACKED;

#define FILL_B0_CAPAB(capab) {												\
		.header = FILL_B0_PROPERTY(sizeof(struct b0_capab), B0_CAPAB),	\
		.bmCapab = (capab)													\
	}

/**
 * @brief channel/pin count
 * @param header header of type b0_property
 * @param bCount number if channels/pins
 *

 */
struct b0_count {
	struct b0_property header;
	uint8_t bCount;
} PACKED;

#define FILL_B0_COUNT(val_count) {											\
		.header = FILL_B0_PROPERTY(sizeof(struct b0_count), B0_COUNT),	\
		.bCount = val_count, 												\
	}

/**
 * @param iLabel[] is a list of string descriptor that will be shown to user
 * @param iLabel[] is a sequence that is interpretter by the module finally
 * Example:
 * 		iLabel[0] is always the module name
 * 		iLabel[1] is
 * 			channel0 name for AIN/AOUT
 * 			pin name for GPIO
 * 			clock pin name for I2C
 * if the list is shorter than the exepcted list 0 is assumed
 * if property missing 0 values is assumed
 * if any value is missing, string is as per host choice
 */
#define DECLARE_B0_LABEL(len_of_array)									\
struct b0_label ## len_of_array {										\
	struct b0_property header;												\
	uint8_t iValues[len_of_array];										\
} PACKED

DECLARE_B0_LABEL(1);
DECLARE_B0_LABEL(2);
DECLARE_B0_LABEL(3);
DECLARE_B0_LABEL(4);
DECLARE_B0_LABEL(7);
DECLARE_B0_LABEL(8);
DECLARE_B0_LABEL(9);

#define FILL_B0_LABEL(type_size, ...) {									\
		.header = FILL_B0_PROPERTY(type_size, B0_LABEL),				\
		.iValues = { __VA_ARGS__ }												\
	}

struct b0_buffer {
	struct b0_property header;
	uint8_t bUnused; /* for alignment only */
	uint32_t dSize;
} PACKED;

#define FILL_B0_BUFFER(size_bytes) {									\
		.header = FILL_B0_PROPERTY(										\
			sizeof(struct b0_buffer), B0_BUFFER),						\
		.dSize = size_bytes													\
	}

/* extract module number from wIndex */
#define B0_MODULE_INDEX_FROM_WINDEX(wIndex) (wIndex & 0xFF)
#define B0_MODULE_TYPE_FROM_WINDEX(wIndex) (wIndex >> 8)

/* Device */
struct b0_device {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint16_t bcdBOX0;
	uint8_t bNumModules;

	const struct b0_module **modules;
} PACKED;

#define B0_DT_DEVICE_SIZE 5

__END_DECLS

#endif
