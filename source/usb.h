/*
 * This file is part of box0-v5.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-v5 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-v5 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-v5.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0V5_USB_H
#define BOX0V5_USB_H

#include "common/handy.h"
#include <unicore-mx/usbd/usbd.h>
#include <unicore-mx/usb/usbstd.h>
#include "config.h"

__BEGIN_DECLS

void usb_init(void);
void prepare_uid(void);

extern usbd_device *dev_global;

extern uint8_t usbd_control_buffer[USB_CONTROL_BUFFER] ALIGNED_2BYTE;

#define ASSERT_STALL(stmt)		\
	if (!(stmt)) {				\
		goto stall;				\
	}

INLINE_FUNC void transfer_cancel_if_valid(usbd_urb_id urb_id)
{
	if (urb_id != USBD_INVALID_URB_ID) {
		usbd_transfer_cancel(dev_global, urb_id);
	}
}

__END_DECLS

#endif
