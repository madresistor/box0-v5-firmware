# Using dfu-util from http://dfu-util.gnumonks.org/
# Ref: http://www.libopencm3.org/wiki/USB_DFU

# Firmware
FIRMWARE="$PWD/box0-v5.bin"
ADDRESS=0x08000000

# Device
VID="0483"
PID="df11"
ALT_SETTING=0

# Command
dfu-util -a $ALT_SETTING -d $VID:$PID -s $ADDRESS:leave -D "$FIRMWARE"
